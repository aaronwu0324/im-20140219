

#import "AFNetworking.h"

typedef enum
{
    APIClientResponseStatusNetworkFail = 0,
    APIClientResponseStatusSuccess = 1,
    APIClientResponseStatusResultTypeFail = 2,
    APIClientResponseStatusAuthFail = 3,
    APIClientResponseStatusSuccessButError = 4

}APIClientResponseStatus;

typedef void(^APIClientResponse)(APIClientResponseStatus status, NSDictionary *JSON);

/**
 API 管理器, 基於Open Source AFNetworking 建構, 有關 AFNetworking 
 請自行查閱 https://github.com/AFNetworking/AFNetworking 
 
 各個 API 的參數及使用請自行查閱文件, 或是聯絡 richard@jamzoo.com.tw
 
 APIClient 利用 block 回傳狀態及資料, 請注意 Block Retain cycle issue
 
 其 block 形態為 _`void(^APIClientResponse)(APIClientResponseStatus status, NSDictionary *JSON)`_
 
    status 分為
    0 APIClientResponseStatusNetworkFail 表示失敗
    1 APIClientResponseStatusSuccess 表示成功
    2 APIClientResponseStatusResultTypeFail 表示回傳格式錯誤, 例如要 Array 卻回傳 Dictionary
    3 APIClientResponseStatusAuthFail 表示會員認證失敗
    4 APIClientResponseStatusSuccessButError 表示後台回傳自定格式的錯誤訊息
 
 */
@interface APIClient : AFHTTPClient

///---------------------------------------------------------------------
/// @name Class Methods
///---------------------------------------------------------------------

/**
 返回一個 singleton instance APIClient 物件
 */
+ (instancetype)sharedClient;

///---------------------------------------------------------------------
/// @name 執行 / 取消 init.asp
///---------------------------------------------------------------------

/**
 https://singlebank-mq.jampush.com.tw/v1/login 會在 _MQTTManager_ 執行
 
 @param callback response 的 callback
 */
- (void)callAPIInitBorker:(APIClientResponse)callback;

/**
 取消 https://singlebank-mq.jampush.com.tw/v1/login, 在 _callAPIInitBorker:_ 之前會先取消一次
 */
- (void)cancelBrokerOperation;
@end
