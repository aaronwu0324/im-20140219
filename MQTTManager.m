

#import "User.h"
#import "NotifyBar.h" //顯示訊息通知
#import "APIClient.h" //連線MQTT Server
#import "MQTTManager.h"
#import "AppDelegate.h"
//#import "MessageManager.h"
#import "DBProcess.h"

@interface MQTTManager(/*Private*/) {
    DBProcess *dbprocess;
}
#pragma mark - Private Properties

//@property (nonatomic, retain) MessageManager *msgManager;

//MQTT IP Address
@property (nonatomic, copy) NSString *borkerIP;

//MQTT Client
@property (nonatomic, strong) SPMQTTClient *client;
//@property (nonatomic, strong) DBProcess *dbprocess;
@end


@implementation MQTTManager

;
#pragma mark - LifeCycle
- (void)dealloc {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
}

+ (instancetype )sharedInstance {
    static dispatch_once_t onceToken;
    static MQTTManager *_manager;
    dispatch_once(&onceToken, ^{
        _manager = [[MQTTManager alloc] init];
    });
    
    return _manager;
}
- (id)init {
    self = [super init];
    if(self) {
        //self.msgManager = [MessageManager sharedManager];
        self.topics = [NSMutableDictionary dictionary];
        dbprocess = [[DBProcess alloc] init];
    }
    return self;
}
#pragma mark - start
- (void) start {
    //[_msgManager startCheckMessageSendFail];
    
    if(!_borkerIP) {
        [[APIClient sharedClient] callAPIInitBorker:^(APIClientResponseStatus status, NSDictionary *JSON) {
            
            switch (status)
            {
                case APIClientResponseStatusSuccess:
                {
                    NSString *broker = JSON[@"broker"];
                    
                    //去掉tcp://
                    self.borkerIP = [broker stringByReplacingOccurrencesOfString:@"tcp://" withString:@""];
                    [self mqttConnect];
                }
                    break;
                    
                default:
                {
                    //失敗再 Call 一次
                    [self start];
                }
                    break;
            }
        }];
    }
    else {
        [self mqttConnect];
    }
}
#pragma mark - unsubscribeAllTopic
- (void)unsubscribeAllTopic {
    for(NSString *topic in [_topics allValues]) {
        [_client unsubscribe:topic];
    }
    
    [_topics removeAllObjects];
}

#pragma mark - stop
- (void)stop {
    //[_msgManager stopCheckMessageSendFail];
    [_client preRelease];
    self.client = nil;
}
#pragma mark - 傳送訊息
- (void)publish:(NSData *)payload onTopic:(NSString *)topic Qos:(NSUInteger)qos retain:(BOOL)retain {
    NSString *sendTopic = [NSString stringWithFormat:@"send/%@", topic];
    NSString *jsonString = [[NSString alloc]initWithData:payload encoding:NSUTF8StringEncoding];
    [_client publish:jsonString onTopic:sendTopic Qos:qos retain:retain];
}
#pragma mark - 連接 MQTT
///---------------------------------------------------------------------
/// @name Private Methods
///---------------------------------------------------------------------
- (void)mqttConnect { //連線至 Broker
    NSLog(@"_borkerIP:%@",_borkerIP);
    NSArray *array = [_borkerIP componentsSeparatedByString:@":"];
    NSString *host = array[0];
    NSUInteger port = [array[1]integerValue];
    
    if(!_client) {
        self.client = [[SPMQTTClient alloc]initWithClientId:[[User sharedUser] CLIENTID]
                                                  keepAlive:20
                                                willMessage:[[User sharedUser] UDID]
                                                  willTopic:@"health/disconnect"
                                                        Qos:2
                                               cleanSession:NO
                                                     retain:NO];
        
        _client.delegate = self;
        [_client connectHost:host port:port];
    }
    else {
        [_client connectHost:host port:port];
    }
}
#pragma mark - SPMQTTClient delegate
- (void)mqttDidConnect:(SPMQTTClient *)mqttClient {
    NSLog(@"MQTT Server 連線成功");
    //完成連線後，會訂閱 device/{token}/data 稱做 private topic，
    //{token} 為 MQTT Client ID (23 bytes)，
    
    [self UpdateDxidToServer];
    
    if(_topics.count > 0) {
        for(NSString *topic in [_topics allValues]){
            [mqttClient subscribe:topic Qos:2];
        }
    }
    else {
        NSString *privateTopic = [NSString stringWithFormat:@"device/%@/data", [[User sharedUser]CLIENTID]];
        [mqttClient subscribe:privateTopic Qos:2];
        _topics[@"privateTopic"] = privateTopic;
    }
    
    [mqttClient publish:[[User sharedUser] UDID] onTopic:@"health/connect" Qos:2 retain:NO];
}
- (void)mqttConnectError:(SPMQTTClient *)mqttClient {
    NSLog(@"MQTT Server 連線失敗");
    
    [self stop];
    
    //重試
    double delayInSeconds = .8f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void){
        [self start];
    });
}
#pragma mark - 收到MQTT訊息
- (void)mqtt:(SPMQTTClient *)mqttClient didReciveMessage:(NSData *)message onTopic:(NSString *)topic; {
    NSError *err = nil;
    id msg = [NSJSONSerialization JSONObjectWithData:message options:NSJSONReadingAllowFragments error:&err];
    
    if(err || ![msg isKindOfClass:[NSDictionary class]]) {
        NSLog(@"收到MQTT訊息發生錯誤:%@",[err localizedDescription]);
        return;
    }
    
    NSLog(@"收到MQTT訊息:%@=%@",topic ,msg);
    
    //我的 UDID
    NSString *myUDID = [[User sharedUser] UDID];
    
    //我的 MEMID
    NSString *myMEMID = [[User sharedUser] ACCOUNT];
    
    //訊息從哪裡來
    NSString *fid = msg[@"fid"];
    
    //訊息從哪裡來的 UDID
    NSString *dxid = msg[@"dxid"];
    
    //訊息來源者的會員代號
    NSString *sid = msg[@"sid"];
   
    //訊息 ID
    //NSString *mid = msg[@"mid"];
    
    //borker 送來有關訊息的資料
    NSString *ctrl = msg[@"ctrl"];
    NSLog(@"ctrl:%@",ctrl);
    
    if([ctrl isEqualToString:@"unsub/channel"]) { //需要取消訂閱此頻道
        NSArray *members = [msg[@"members"]sortedArrayUsingSelector:@selector(compare:)];
        if(_topics[members]) {
            NSLog(@"需要取消訂閱頻道:%@", msg[@"channel"]);
            [_client unsubscribe:msg[@"channel"]];
            [_topics removeObjectForKey:members];
        }
    }
    else if([ctrl isEqualToString:@"sub/channel"]) { //需要訂閱此頻道
        NSArray *members = [msg[@"members"]sortedArrayUsingSelector:@selector(compare:)];
        if(!_topics[members]) {
            NSLog(@"需要訂閱頻道:%@", msg[@"channel"]);
            [_client subscribe:msg[@"channel"] Qos:2];
            _topics[members] = msg[@"channel"];
        }
    }
    else if([ctrl isEqualToString:@"channel/chat"]) {
        //訊息要發給誰
        NSString *tid = msg[@"tid"];
        
        //判斷是不是群組訊息（toId是數字）
        BOOL isGroupMessage = [[NSScanner scannerWithString:tid] scanInt:nil];
        if (isGroupMessage) {
            NSLog(@"是群組訊息");
        }
        
        //我發的訊息，已傳送到 server
        if([fid isEqualToString:myMEMID] && [dxid isEqualToString:myUDID]) {
            [self messageSentHandle:msg onTopic:topic isGroupMessage:isGroupMessage];
        }
        //我可能從另一台裝置發送的訊息
        else if ([fid isEqualToString:myMEMID] && ![dxid isEqualToString:myUDID]) {
            [self newMessageFromOtherDeviceHandle:msg onTopic:topic];
        }
        //單純別人傳給我的訊息
        else if([tid isEqualToString:myMEMID] || isGroupMessage) {
            //接收訊息
            [self newMessageHandle:msg onTopic:topic];
        }
    }
    else if([ctrl isEqualToString:@"channel/read"]) { //訊息讀取
        //判斷是不是群組訊息（toId是數字）
        BOOL isGroupMessage = [[NSScanner scannerWithString:sid] scanInt:nil];
        if (isGroupMessage) {
            NSLog(@"是群組訊息");
        }
        
        //對方傳給我已讀的ack
        if([sid isEqualToString:myMEMID]) {
            [self messageReadHandle:msg onTopic:topic needPostToChannel:YES isGroupMessage:NO];
        }
        //傳給對方或是群聊訊息的已讀
        else if([fid isEqualToString:myMEMID] || isGroupMessage) {
            [self messageReadHandle:msg onTopic:topic needPostToChannel:NO isGroupMessage:isGroupMessage];
        }
    }
    else if([ctrl isEqualToString:@"channel/recv"]) { ////對方已收取的 ack
        BOOL isGroupMessage = [[NSScanner scannerWithString:sid] scanInt:nil];
        if (isGroupMessage) { //群組訊息，存起來
            [dbprocess SaveGroupMessageReceive:msg];
        }
        /*else if([sid isEqualToString:myMEMID]) { //個聊
            [self messageRecivedHandle:msg];
            NSDictionary *message = @{@"payload" : msg, @"channelTopic" : topic};
            [self mh_post:message toChannel:@"kChatroomShouldHandleMessageWasRecived"];
        }*/
    }
}
#pragma mark - MQTT 訊息處理
/**
 處理 MQTT 訊息已傳送到 Server
 
 @param msg 傳送的訊息
 @param topic 頻道
 */
- (void)messageSentHandle:(NSDictionary *)msg onTopic:(NSString *)topic isGroupMessage:(BOOL)isGroupMessage
{
    //NSLog(@"msg:%@",msg);
    NSMutableDictionary *message = [dbprocess GetMessageWithMessageID:[msg objectForKey:@"mid"]];
    
    //設定訊息是群聊
    if (isGroupMessage) {
        [message setObject:@"1" forKey:@"isGroupMessage"];
    }
    
    NSLog(@"message:%@",message);
    
    if(message) {
        [message setObject:[NSNumber numberWithBool:YES] forKey:@"isSend"];
        NSTimeInterval timestemp = [msg[@"t"]doubleValue] / 1000.0f;
        NSDate *sendDate = [NSDate dateWithTimeIntervalSince1970:timestemp];
        
        NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
        _formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSString *stringSendDate = [_formatter stringFromDate:sendDate];
        
        [message setObject:stringSendDate forKey:@"sendDate"];
        
        [dbprocess SaveMessage:message];
        
        NSLog(@"message <%@> was send", [message objectForKey:@"messageId"]);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kChatroomShouldHandleMessageWasSend" object:@{@"message" : message}];
    }
}
/**
 處理 MQTT 訊息已被對方讀取
 
 @param msg 傳送的訊息
 @param topic 頻道
 @param flag 是否要透過 MHChannel 廣播
 */
- (void)messageReadHandle:(NSDictionary *)msg onTopic:(NSString *)topic needPostToChannel:(BOOL)flag isGroupMessage:(BOOL)isGroupMessage {
    //邏輯：判斷是群聊還是個聊的已讀，群聊就直接存起來，個聊存起來並發佈到頻道（如果有需要）
    
    //訊息要發給誰
    //NSString *message_id = [msg objectForKey:@"mid"];
    //NSString *tid = [dbprocess GetToIDWithMessageID:message_id];
    
    //判斷是不是群組訊息（toId是數字）
    //BOOL isGroupMessage = [[NSScanner scannerWithString:tid] scanInt:nil];
    if (isGroupMessage) { //群聊就直接存起來
        [dbprocess SaveGroupMessageRead:msg];
    }
    else { //個聊存起來並發佈到頻道（如果有需要）
        NSMutableDictionary *message = [dbprocess GetMessageWithMessageID:[msg objectForKey:@"mid"]];
        
        if (message) {
            [message setObject:[NSNumber numberWithBool:YES] forKey:@"isRead"];
            NSTimeInterval timestemp = [msg[@"t"]doubleValue] / 1000.0f;
            NSDate *readDate = [NSDate dateWithTimeIntervalSince1970:timestemp];
            [message setObject:readDate forKey:@"readDate"];
            
            [dbprocess SaveMessage:message];
            
            if(flag) {
                NSLog(@"message <%@> was read", [message objectForKey:@"messageId"]);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"kChatroomShouldHandleMessageWasRead" object:@{@"message" : message}];
            }
        }
    }
}
/**
 處理我從其他裝置傳送的訊息
 
 @param msg 傳送的訊息
 @param topic 頻道
 */
- (void)newMessageFromOtherDeviceHandle:(NSDictionary *)msg onTopic:(NSString *)topic {
    //資料庫沒這筆才新增, MQTT 有時回傳重複的訊息
    //目前找不到 bug, 所以才用這個判斷
    if (![dbprocess didMessageExistWithMessageID:[msg objectForKey:@"mid"]]) {
        NSTimeInterval timestemp = [msg[@"t"]doubleValue] / 1000.0f;
        NSDate *sendDate = [NSDate dateWithTimeIntervalSince1970:timestemp];
        
        NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
        _formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSString *stringSendDate = [_formatter stringFromDate:sendDate];
        
        NSMutableDictionary *dicMessage = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           msg[@"fid"],@"fromId",
                                           msg[@"tid"],@"toId",
                                           msg[@"mid"],@"messageId",
                                           msg[@"say"],@"message",
                                           [NSNumber numberWithBool:YES],@"isSend",
                                           stringSendDate,@"sendDate",
                                           
                                           nil];
        
        //NSLog(@"you send message <%@> from other device", newMessage.messageId);
        NSLog(@"you send message <%@> from other device", [dicMessage objectForKey:@"messageId"]);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kChatroomShouldHandleMessageFromOtherDevice" object:@{@"message" : dicMessage}];
    }
}

/**
 處理我收到的新訊息
 
 @param msg 收到的訓息
 @param topic 頻道
 */
- (void)newMessageHandle:(NSDictionary *)msg onTopic:(NSString *)topic {
    //資料庫沒這筆才新增, MQTT 有時回傳重複的訊息
    //目前找不到 bug, 所以才用這個判斷
    if (![dbprocess didMessageExistWithMessageID:[msg objectForKey:@"mid"]]) {
        
        //判斷是不是正在聊天室
        BOOL inChatRoom = NO;
        if ([[VariableStore sharedInstance].currentChatRoomGroupID isEqualToString:[msg objectForKey:@"tid"]]) { //正在跟該群組聊天
            inChatRoom = YES;
        }
        else if ([VariableStore sharedInstance].isInChatRoom) { //正在個人聊天
            inChatRoom = YES;
        }
        
        NSTimeInterval timestemp = [msg[@"t"]doubleValue] / 1000.0f;
        NSDate *sendDate = [NSDate dateWithTimeIntervalSince1970:timestemp];
        
        NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
        _formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSString *stringSendDate = [_formatter stringFromDate:sendDate];
        
        NSMutableDictionary *dicMessage = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    msg[@"fid"],@"fromId",
                                    msg[@"tid"],@"toId",
                                    msg[@"mid"],@"messageId",
                                    msg[@"say"],@"message",
                                    [NSNumber numberWithBool:YES],@"isSend",
                                    stringSendDate,@"sendDate",
                                    nil];
        
        if (inChatRoom) {
            [dicMessage setObject:stringSendDate forKey:@"readDate"];
        }
        else { //不再聊天室就傳送訊息通知
            [[NotifyBar defaultNotify] show];
        }

        //NSLog(@"dicMessage:%@",dicMessage);
    
        NSLog(@"you got new message <%@>", [dicMessage objectForKey:@"messageId"]);
        
        //群聊跟個聊，要to的id不一樣
        NSString *tid = [dicMessage objectForKey:@"toId"];
        BOOL isGroupMessage = [[NSScanner scannerWithString:tid] scanInt:nil]; //判斷是不是群組訊息（toId是數字）
        if (isGroupMessage) { //群聊
        }
        else { //個聊，存訊息來源者的account
            tid = [dicMessage objectForKey:@"fromId"];
        }
        
        if (!inChatRoom) { //保存起來，以讓按下通知的時候可以知道要跟誰聊天
            if (isGroupMessage) {
                //設定訊息是群聊
                [dicMessage setObject:@"1" forKey:@"isGroupMessage"];
            }
            
            [VariableStore sharedInstance].notifyMemberID = tid;
        }
        
        [dbprocess SaveMessage:dicMessage];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kChatroomShouldHandleYouGotNewMessage" object:@{@"message" : dicMessage}];
        
        //傳送已收到
        NSDictionary *params = @{@"ctrl" : @"channel/recv",
                                 @"fid" : [[User sharedUser] ACCOUNT],
                                 @"sid" : tid,
                                 @"mid" : msg[@"mid"]
                                 };
        NSData *data = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
        [self publish:data onTopic:topic Qos:2 retain:NO];
    }
}
#pragma mark -
#pragma mark API-註冊使用者DXID
-(void) UpdateDxidToServer {
    NSString *account = [[User sharedUser] ACCOUNT];
    NSString *dxid = [[User sharedUser] UDID];
    
    api_mqtt_member_dxid *api = [[api_mqtt_member_dxid alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api UpdateDxidWithAccount:account Dxid:dxid];
}
-(void) api_mqtt_member_dxidComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    if (success) { //更新UI
    }
    else {
    }
}
#pragma mark - JamPush推播
-(void) GetJamPushWithPid:(NSString *)pid {
    //[VariableStore sharedInstance].pushedpxid = pid;
    //[VariableStore sharedInstance].pushedIsFromCoupon = YES;
    
    AppDelegate *delegate = [[UIApplication sharedApplication]delegate];
    UINavigationController *navController = (UINavigationController *)delegate.window.rootViewController;
    
    UIViewController *currentController = navController.viewControllers[0];
    
    
    if([currentController isKindOfClass:NSClassFromString(@"ViewController")])
    {   //MainViewController
        [navController popToRootViewControllerAnimated:NO];
        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"MQTTManager gotoSmartListDetail" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            //[alert show];
            [currentController performSegueWithIdentifier:@"gotoSmartListDetail" sender:nil];
        });
    }
}
#pragma mark -
#pragma mark API-負值取頻道
-(void) GetFreePush:(NSString *)pid {
    /*[VariableStore sharedInstance].pushedpxid = pid;
    
    api_jampush_get_free_push *api = [[api_jampush_get_free_push alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetFreePushWithPid:pid];*/
}
-(void) api_jampush_get_free_pushComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"api_jampush_get_free_pushComplete" message:[NSString stringWithFormat:@"%@",data] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    //[alert show];
    /*if (success) { //更新UI
        NSString *pid = [data objectForKey:@"pid"];
        NSString *channel = [data objectForKey:@"channel"];
        [VariableStore sharedInstance].pushedpxid = pid;
        [VariableStore sharedInstance].pushedIsFromCoupon = NO;
        
        AppDelegate *delegate = [[UIApplication sharedApplication]delegate];
        UINavigationController *navController = (UINavigationController *)delegate.window.rootViewController;
        
        UIViewController *currentController = navController.viewControllers[0];
        
        NSString *SegueWithIdentifier = @"gotoSmartListDetail";
        if ([channel isEqualToString:@"hwu"]) { //招生
            SegueWithIdentifier = @"gotoRecruitFinalDetailView";
        }
        else if ([channel isEqualToString:@"chat"]) {
            //channel為"chat"時
            //將[sn]值TM001551帶入[醒吾Chat]聊天頁面
            SegueWithIdentifier = @"gotoChatRoomViewController";
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"gotoChatRoomViewController" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            //[alert show];
            NSString *sn = [data objectForKey:@"sn"];
            [VariableStore sharedInstance].pushedSn = sn;
        }

        if([currentController isKindOfClass:NSClassFromString(@"ViewController")]) {   
            [navController popToRootViewControllerAnimated:NO];
            double delayInSeconds = 0.1;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"MQTTManager gotoSmartListDetail" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                //[alert show];
                [currentController performSegueWithIdentifier:SegueWithIdentifier sender:nil];
            });
        }
        
    }
    else {
    }*/
}
@end
