
#import <CoreData/CoreData.h>

/**
 SingleBank 訊息物件
 */
@interface Message : NSManagedObject

/**
 送出訊息的 UserId
 */
@property (nonatomic, retain) NSString * fromId;

/**
 接收訊息的 UserId
 */
@property (nonatomic, retain) NSString * toId;

/**
 訊息的 Id
 */
@property (nonatomic, retain) NSString * messageId;

/**
 訊息是否被讀取
 */
@property (nonatomic, retain) NSNumber * isRead;

/**
 訊息是否已送出
 */
@property (nonatomic, retain) NSNumber * isSend;

/**
 訊息是否被接收
 */
@property (nonatomic, retain) NSNumber * isRecv;

/**
 訊息被讀取日期
 */
@property (nonatomic, retain) NSDate * readDate;

/**
 訊息送出日期
 */
@property (nonatomic, retain) NSDate * sendDate;

/**
 訊息被接收日期
 */
@property (nonatomic, retain) NSDate * recvDate;

/**
 訊息送出是否失敗
 */
@property (nonatomic, retain) NSNumber * isFail;

/**
 訊息內容
 */
@property (nonatomic, retain) NSString * message;

@end
