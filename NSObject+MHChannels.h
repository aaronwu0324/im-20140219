
#include <dispatch/dispatch.h>

typedef void (^MHChannelsBlock)(id sender, NSDictionary *dictionary);

/*!
 * A "channel" is like a private NSNotificationCenter between just two objects
 * (although more are possible).
 *
 * Instead of making your objects, such as two view controllers, communicate
 * directly with one another through sharing pointers or making one a delegate
 * of the other, you can have them communicate through a channel. Objects can
 * post messages to the channel and/or listen to messages from other objects.
 *
 * There is no need to create channels before you use them. A channel is
 * identified by a unique name (an NSString). If two objects use the same
 * channel name, then they are communicating.
 *
 * The order in which messages are delivered is arbitrary, so if you have more
 * than one listener on the channel you should not assume anything about which
 * one is called first. To force a delivery order, give each listener its own
 * priority. The default priority is 0. Higher priorities go first.
 *
 * The listener block is always executed synchronously on the thread that the
 * poster runs on, except when queue is not nil. If you pass in a queue, then
 * the block is called _asynchronously_ on that queue.
 *
 * The channel keeps a weak reference to any listeners, so you do not have to
 * explicitly remove the listener from the channel before it gets deallocated.
 */
@interface NSObject (MHChannels)

- (void)mh_post:(NSDictionary *)dictionary toChannel:(NSString *)channelName;

- (void)mh_listenOnChannel:(NSString *)channelName block:(MHChannelsBlock)block;

- (void)mh_listenOnChannel:(NSString *)channelName priority:(NSInteger)priority queue:(dispatch_queue_t)queue block:(MHChannelsBlock)block;

- (void)mh_removeFromChannel:(NSString *)channelName;

- (void)mh_debugChannels;

@end
