#import "CommonTask.h"

@implementation CommonTask

+ (CommonTask *)sharedInstance
{
    // the instance of this class is stored here
    static CommonTask *myInstance = nil;
	
    // check to see if an instance already exists
    if (nil == myInstance) {
        myInstance  = [[[self class] alloc] init];
        // initialize variables here
	}
    // return the instance of this class
    return myInstance;
}
#pragma mark - 設定的震動
-(BOOL) DidVibrateON { //判斷設定的震動有沒有打開
    BOOL DidVibrateON = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *_DidVibrateON = [defaults objectForKey:kDEFAULTS_KEY_SETTING_VIBRATE];
    
    if (!_DidVibrateON) { //第一次執行是null
    }
    else {
        if ([_DidVibrateON isEqualToString:@"N"]) {
            DidVibrateON = NO;
        }
    }
    
    return DidVibrateON;
}
-(void) SetVibrate:(NSString *)OnOrOff { //設定震動
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //儲存起來
    [defaults setObject:OnOrOff forKey:kDEFAULTS_KEY_SETTING_VIBRATE];
    [defaults synchronize];
}
#pragma mark - 設定的聲音
-(BOOL) DidSoundON { //判斷設定的聲音有沒有打開
    BOOL DidSoundON = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *_DidSoundON = [defaults objectForKey:kDEFAULTS_KEY_SETTING_SOUND];
    
    if (!_DidSoundON) { //第一次執行是null
    }
    else {
        if ([_DidSoundON isEqualToString:@"N"]) {
            DidSoundON = NO;
        }
    }
    
    return DidSoundON;
}
-(void) SetSound:(NSString *)OnOrOff { //設定震動
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //儲存起來
    [defaults setObject:OnOrOff forKey:kDEFAULTS_KEY_SETTING_SOUND];
    [defaults synchronize];
}
#pragma mark - 訊息相關
-(NSInteger) GetChatMessageKindWithString:(NSString *)message { //判斷訊息是文字、圖片還是聲音
    NSInteger messageKind = chatMessageKindMessage;
    
    if ([message rangeOfString:@"sticker_"].length > 0 && [message rangeOfString:@".png"].length > 0 ) { //貼圖
        messageKind = chatMessageKindSticker;
    }
    else if (([message rangeOfString:@"http://"].length  > 0 && [message rangeOfString:@".png"].length  > 0) || ([message rangeOfString:@"http://"].length  > 0 && [message rangeOfString:@".jpg"].length  > 0)) { //圖片
        messageKind = chatMessageKindImage;
    }
    else if ([message rangeOfString:@"{"].length  > 0 && [message rangeOfString:@"}"].length  > 0) { //聲音
        messageKind = chatMessageKindSound;
    }
    
    return messageKind;
}
-(NSString *) GetChatMessageKindNameWithString:(NSString *)message { //判斷訊息是文字、圖片還是聲音
    NSString *messageKindName = @"文字";
    
    NSInteger messageKind = [self GetChatMessageKindWithString:message];
    
    if (messageKind == chatMessageKindSticker) { //貼圖
        messageKindName = @"貼圖";
    }
    else if (messageKind == chatMessageKindImage) { //圖片
        messageKindName = @"照片";
    }
    else if (messageKind == chatMessageKindSound) { //聲音
        messageKindName = @"語音訊息";
    }
    
    return messageKindName;
}
-(CGFloat) cellHeightForText:(NSString *)text { //計算訊息的cell高度
    CGFloat cellHeight = 0;
    NSInteger messageKind = [self GetChatMessageKindWithString:text];
    if (messageKind == chatMessageKindImage) {
        cellHeight = 180;
    }
    else if (messageKind == chatMessageKindSticker) {
        cellHeight = 180;
    }
    else if (messageKind == chatMessageKindSound) {
        cellHeight = 60;
    }
    else {
        //注意：以下方法iOS7+ Only
        CGSize maxSize = CGSizeMake(210, 500);
        CGFloat lineHeight = 29;
        
        CGSize StringSize = [text boundingRectWithSize:maxSize options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:14] } context: nil].size;
        //NSLog(@"StringSize height:%f",StringSize.height);
        
        CGFloat shiftHeight = StringSize.height - lineHeight;
        cellHeight = 60 + shiftHeight;
    }
    //NSLog(@"cellHeight:%f",cellHeight);
    
    return cellHeight;
}
#pragma mark - 雜項
-(NSArray *)SortArrayByDate:(NSArray *)data Key:(NSString *)key {
    NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
    _formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSArray *sortedArray;
    sortedArray = [data sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        //NSDate *first = [_formatter dateFromString:[(NSDictionary *)a objectForKey:key]];
        //NSDate *second = [_formatter dateFromString:[(NSDictionary *)b objectForKey:key]];
        NSDate *first = [(NSDictionary *)a objectForKey:key];
        NSDate *second = [(NSDictionary *)b objectForKey:key];
        return [first compare:second];
    }];
    
    return sortedArray;
}
- (void)dealloc {
}
@end
