
@protocol api_sentence_create_sentenceDelegate <NSObject>
@optional
-(void) api_sentence_create_sentenceComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface api_sentence_create_sentence : NSObject <ASIHTTPRequestDelegate> {
    ASIFormDataRequest *ASIRequest;
    __weak id <api_sentence_create_sentenceDelegate> delegate;
}
@property (weak) id delegate;
-(void) CreateSentenceWithAccount:(NSString *)account Sentence:(NSString *)sentence;
@end
