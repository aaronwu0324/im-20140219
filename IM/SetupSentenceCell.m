

#import "SetupSentenceCell.h"

@implementation SetupSentenceCell
@synthesize delegate;
-(void) SetupSentenceCellDeleteAtIndex:(NSInteger)index {
    if ([[self delegate] respondsToSelector:@selector(SetupSentenceCellDeleteAtIndex:)]) {
        [[self delegate] SetupSentenceCellDeleteAtIndex:index];
    }
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(IBAction) Delete {
    [self SetupSentenceCellDeleteAtIndex:self.tag];
}
- (void)dealloc {
    //NSLog(@"SetupSentenceCell dealloc");
}
@end
