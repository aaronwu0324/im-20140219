

#import "ViewChatFaceImageCell.h"
#import "ViewChatFaceImageView.h"

@implementation ViewChatFaceImageCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void) UpdateUIWithData:(NSArray *)data {
    CGFloat viewWidth = 97.0;
    CGFloat viewHeight = 38.0;
    CGFloat gap = 5;
    CGFloat startX = 10;
    
    for (int i = 0 ; i < [data count]; i++) {
        NSString *urlString = [data objectAtIndex:i];
        //NSLog(@"urlString:%@",urlString);
        
        NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"ViewChatFaceImageView" owner:self options:nil];
        ViewChatFaceImageView *myView = [toplevels objectAtIndex:0];
        [myView UpdateUIWithUrl:urlString];
        myView.frame = CGRectMake(startX + (viewWidth+gap) * i,
                                  0,
                                  viewWidth,
                                  viewHeight);
        myView.tag = self.startIndex + i;
        [self.contentView addSubview:myView];
    }
}
- (void)dealloc {
    NSLog(@"ViewChatFaceImageCell dealloc");
}
@end
