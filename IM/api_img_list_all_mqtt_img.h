
@protocol api_img_list_all_mqtt_imgDelegate <NSObject>
@optional
-(void) api_img_list_all_mqtt_imgComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_img_list_all_mqtt_img : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_img_list_all_mqtt_imgDelegate> delegate;
}
@property (weak) id delegate;
-(void) GetListAllMqttImg;
@end
