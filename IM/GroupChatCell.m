

#import "GroupChatCell.h"

@implementation GroupChatCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(IBAction) EditGroup {
    NSNumber *number = [NSNumber numberWithInt:self.cellIndex];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GroupEdit" object:number];
}
- (void)dealloc {
    //NSLog(@"NameListCell dealloc");
}
@end
