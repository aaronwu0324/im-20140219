@protocol ViewChatVoicePlayerDelegate <NSObject>
@optional
-(void) ViewChatVoicePlayerStart; //開始播放
-(void) ViewChatVoicePlayerFinish; //結束播放
@end

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewChatVoicePlayer : NSObject <AVAudioPlayerDelegate> {
    id playbackObserver;        //觀察播到的百分比
    __weak id <ViewChatVoicePlayerDelegate> delegate;
}
@property (weak) id delegate;
@property (strong, nonatomic) AVPlayer *player;
@property (strong, nonatomic) AVPlayerItem *playerItem;
-(void) PlayVoiceWithUrl:(NSString *)urlString;
@end
