
@protocol api_group_list_my_groupDelegate <NSObject>
@optional
-(void) api_group_list_my_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_group_list_my_group : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_group_list_my_groupDelegate> delegate;
}
@property (weak) id delegate;
-(void) GetMyGroupWithAccount:(NSString *)account;
@end
