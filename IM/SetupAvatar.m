
#import "SetupAvatar.h"

@interface SetupAvatar () {
    SPUserResizableView *ViewScaleAndSize;
}
@property (nonatomic,strong) UIImageView *ImageMaskTop;
@property (nonatomic,strong) UIImageView *ImageMaskDown;
@property (nonatomic,strong) UIImageView *ImageMaskLeft;
@property (nonatomic,strong) UIImageView *ImageMaskRight;
@end

@implementation SetupAvatar
@synthesize Image;
@synthesize selectedImage;
@synthesize delegate;
@synthesize ImageMaskTop;
@synthesize ImageMaskDown;
@synthesize ImageMaskLeft;
@synthesize ImageMaskRight;

#pragma mark -
#pragma mark Delegate
-(void) SetupAvatarScaleAndMoveView_Cancel {
    if ([[self delegate] respondsToSelector:@selector(SetupAvatarScaleAndMoveView_Cancel)]) {
        [[self delegate] SetupAvatarScaleAndMoveView_Cancel];
    }
}
-(void) SetupAvatarScaleAndMoveView_Confirm:(UIImage *)capturedImage {
    if ([[self delegate] respondsToSelector:@selector(SetupAvatarScaleAndMoveView_Confirm:)]) {
        [[self delegate] SetupAvatarScaleAndMoveView_Confirm:capturedImage];
    }
}
#pragma mark -
#pragma mark UIViewControll 初始化相關
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initParameter];
    [self initUIKit];
}
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    [self hideTabBar:self.tabBarController];
}
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
    [self showTabBar:self.tabBarController];
}
-(void) viewDidUnload {
    [super viewDidUnload];
}
#pragma mark -
#pragma mark 初始化相關
-(void) initParameter { //初始化參數
}
-(void) initUIKit { //初始化畫面
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) self.edgesForExtendedLayout = UIRectEdgeNone;
    
    Image.image = selectedImage;
    
    ImageMaskTop = [[UIImageView alloc] initWithImage:nil];
    ImageMaskTop.backgroundColor = [UIColor blackColor];
    ImageMaskTop.alpha = 0.6f;
    [Image addSubview:ImageMaskTop];
    
    ImageMaskDown = [[UIImageView alloc] initWithImage:nil];
    ImageMaskDown.backgroundColor = [UIColor blackColor];
    ImageMaskDown.alpha = 0.6f;
    [Image addSubview:ImageMaskDown];
    
    ImageMaskLeft = [[UIImageView alloc] initWithImage:nil];
    ImageMaskLeft.backgroundColor = [UIColor blackColor];
    ImageMaskLeft.alpha = 0.6f;
    [Image addSubview:ImageMaskLeft];
    
    ImageMaskRight = [[UIImageView alloc] initWithImage:nil];
    ImageMaskRight.backgroundColor = [UIColor blackColor];
    ImageMaskRight.alpha = 0.6f;
    [Image addSubview:ImageMaskRight];
    
    CGRect frame = CGRectMake(0, 0, 200, 150);
    ViewScaleAndSize = [[SPUserResizableView alloc] initWithFrame:frame];
    UIView *contentView = [[UIView alloc] initWithFrame:frame];
    [contentView setBackgroundColor:[UIColor clearColor]];
    ViewScaleAndSize.center = CGPointMake(Image.frame.size.width/2, Image.frame.size.height/2);
    ViewScaleAndSize.contentView = contentView;
    [Image addSubview:ViewScaleAndSize];
    ViewScaleAndSize.delegate = self;
    
    [self UpdateMask:nil];
}
-(IBAction) Cancel {
    [self SetupAvatarScaleAndMoveView_Cancel];
    [self Back];
}
-(IBAction) Confirm {
    ViewScaleAndSize.hidden = YES;
    ImageMaskTop.hidden = YES;
    ImageMaskDown.hidden = YES;
    ImageMaskLeft.hidden = YES;
    ImageMaskRight.hidden = YES;
    
    //取出截圖
    UIImage *capturedImage = [self imageFromCombinedContext:ViewScaleAndSize];
    [self SetupAvatarScaleAndMoveView_Confirm:capturedImage];
    
    [self Back];
}
-(UIImage *)imageFromCombinedContext:(UIView *)background {
    UIImage *image;
    CGSize size = self.view.frame.size;
    UIGraphicsBeginImageContext(size);
    [background.layer affineTransform];
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRef imref = CGImageCreateWithImageInRect([image CGImage], background.frame);
    image = [UIImage imageWithCGImage:imref];
    CGImageRelease(imref);
    return image;
}
- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView {
    //NSLog(@"userResizableViewDidBeginEditing:%@",userResizableView);
}
- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView {
    //NSLog(@"userResizableViewDidEndEditing:%@",userResizableView);
}
- (void)userResizableViewDidMoveEditing:(SPUserResizableView *)userResizableView {
    //NSLog(@"userResizableViewDidEndEditing:%@",userResizableView);
    //NSLog(@"ViewScaleAndSize:%@",ViewScaleAndSize);
    [self UpdateMask:nil];
}
#pragma mark -
#pragma mark Mask
-(void) UpdateMask:(SPUserResizableView *)userResizableView {
    CGFloat cornerWidth = 35.0;
    CGFloat cornerHeight = 35.0;
    ImageMaskTop.frame = CGRectMake(0, 0, 320, ViewScaleAndSize.frame.origin.y+cornerHeight/2 - 7);
    
    CGFloat downY = ImageMaskTop.frame.size.height + ViewScaleAndSize.frame.size.height - cornerHeight + 13;
    //NSLog(@"%f %f",downY,Image.frame.size.height);
    ImageMaskDown.frame = CGRectMake(0, downY, 320, Image.frame.size.height - downY);
    
    ImageMaskLeft.frame = CGRectMake(0, ImageMaskTop.frame.size.height, ViewScaleAndSize.frame.origin.x + cornerWidth/2 - 6, downY - ImageMaskTop.frame.size.height);
    
    CGFloat rightX = ImageMaskLeft.frame.size.width + ViewScaleAndSize.frame.size.width - cornerWidth/2 - 4;
    ImageMaskRight.frame = CGRectMake(rightX, ImageMaskTop.frame.size.height, 320 - rightX, downY - ImageMaskTop.frame.size.height);
    
}
#pragma mark - 換頁
-(void)Back {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Tabbar 隱藏/顯示
- (void)hideTabBar:(UITabBarController *) tabbarcontroller {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    CGFloat height = IS_IPHONE_5 ? 568 : 480;
    //NSLog(@"height:%f",height);
    
    for(UIView *view in tabbarcontroller.view.subviews) {
        if([view isKindOfClass:[UITabBar class]]) {
            [view setFrame:CGRectMake(view.frame.origin.x, height, view.frame.size.width, view.frame.size.height)];
        }
        else {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, height)];
        }
    }
    
    [UIView commitAnimations];
}

- (void)showTabBar:(UITabBarController *) tabbarcontroller {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    CGFloat height = IS_IPHONE_5 ? 568 : 480;
    height = height - 49;
    
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        //NSLog(@"%@", view);
        
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, height, view.frame.size.width, view.frame.size.height)];
            
        }
        else
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, height)];
        }
    }
    
    [UIView commitAnimations];
}
#pragma mark - 雜項
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
    delegate = nil;
    selectedImage = nil;
}
@end
