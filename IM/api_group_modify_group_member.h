
@protocol api_group_modify_group_memberDelegate <NSObject>
@optional
-(void) api_group_modify_group_memberComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_group_modify_group_member : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_group_modify_group_memberDelegate> delegate;
}
@property (weak) id delegate;
-(void) ModifyGroupWithAccout:(NSString *)account GroupName:(NSString *)group_name Members:(NSString *)members;
@end
