
@protocol api_mqtt_createDelegate <NSObject>
@optional
-(void) api_mqtt_createComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface api_mqtt_create : NSObject <ASIHTTPRequestDelegate> {
    ASIFormDataRequest *ASIRequest;
    __weak id <api_mqtt_createDelegate> delegate;
}
@property (weak) id delegate;
-(void) CreateMQTTChannelFromMYID:(NSString *)MYID HERID:(NSString *)HERID APPID:(NSString *)APPID UDID:(NSString *)UDID;
@end
