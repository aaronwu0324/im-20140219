#import "ImageViewerVC.h"
#import "HTTPImageView.h"

@interface ImageViewerVC () {
    
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,strong) IBOutlet HTTPImageView *Image;
@end

@implementation ImageViewerVC
#pragma mark - 初始化相關
-(void) initUIKit { //初始化畫面
    [_Image setImageWithURL:_urlString placeholderImage:nil];
}
#pragma mark - UIScrollView Delegate 
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _Image;
}

#pragma mark - 換頁
-(IBAction) Dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - 雜項
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
}
@end
