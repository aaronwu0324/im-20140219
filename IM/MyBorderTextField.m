

#import "MyBorderTextField.h"

@implementation MyBorderTextField

- (void)drawRect:(CGRect)rect {
    [[self layer] setBorderColor:[[UIColor colorWithRed:120.0/255.0 green:150.0/255.0 blue:178.0/255.0 alpha:1.0] CGColor]];
    [[self layer] setBorderWidth:1.5];
}
@end
