

#import "api_member_update_avatar.h"
#import "JSON.h"
#import "Base64.h"

@implementation api_member_update_avatar
@synthesize delegate;
#pragma mark -
#pragma mark Delegate
-(void) api_member_update_avatarComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    if ([[self delegate] respondsToSelector:@selector(api_member_update_avatarComplete:Message:Data:)]) {
        [[self delegate] api_member_update_avatarComplete:success Message:message Data:data];
    }
}
#pragma mark -
#pragma mark ASIHTTPRequest
-(void) UpdateAvatarWithAccount:(NSString *)account Avatar:(UIImage *)avatar {
    NSData* data = UIImageJPEGRepresentation(avatar, 1.0f);
    [Base64 initialize];
    NSString *strEncoded = [Base64 encode:data];
    //NSLog(@"strEncoded:%@",strEncoded);
    
    NSString *urlString = [NSString stringWithFormat:@"%@/api_member/update_avatar",kSYSGatewayTemp];
    NSLog(@"api_member/update_avatar url:%@",urlString);
    
    //urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIRequest = [[ASIFormDataRequest alloc] initWithURL:url];
    [ASIRequest setTimeOutSeconds:kURLTimeoutSecond];
    [ASIRequest setNumberOfTimesToRetryOnTimeout:kURLRetryOnTimeout];
    
    [ASIRequest addPostValue:account forKey:@"account"];
    [ASIRequest addPostValue:strEncoded forKey:@"avatar"];

    [ASIRequest setDelegate:self];
    [ASIRequest startAsynchronous];
}
- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    //NSLog(@"api_member/update_avatar Finished:%@",responseString);
    
    NSError *error = nil;
	SBJSON *parser = [SBJSON new];
	
    //原始資料
	NSDictionary *result = (NSDictionary *) [parser objectWithString:responseString error:&error];
    NSLog(@"api_member/update_avatar result:%@",result);
    
    while (error) {
        NSLog(@"json error:%@",[error localizedDescription]);
        error = [[error userInfo] objectForKey:NSUnderlyingErrorKey];
        NSLog(@"Error trace: %@", parser.errorTrace);
        [self api_member_update_avatarComplete:NO Message:kMessageNetworkFail Data:nil];
        [self performSelector:@selector(Cancle)];
        return;
    }
    
    NSString *sys_msg = [result objectForKey:@"sys_msg"];
    NSString *sys_code = [result objectForKey:@"sys_code"];
    NSLog(@"api_member/update_avatar:(%@)%@",sys_code,sys_msg);
    
    if ([sys_code isEqualToString:@"200"]) { //200成功
        NSDictionary *data = result;
        [self api_member_update_avatarComplete:YES Message:nil Data:data];
    }
    else { //失敗
        [self api_member_update_avatarComplete:NO Message:sys_msg Data:nil];
    }
    
    [self performSelector:@selector(Cancle)];
}
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"requestFailed error:%@",error);
    [self api_member_update_avatarComplete:NO Message:kMessageNetworkFail Data:nil];
    [self performSelector:@selector(Cancle)];
}
#pragma mark -
#pragma mark 雜項
-(void) Cancle {
    // Cancels an asynchronous request
    [ASIRequest cancel];
    
    // Cancels an asynchronous request, clearing all delegates and blocks first
    [ASIRequest clearDelegatesAndCancel];
    
    //移除物件
    [[VariableStore sharedInstance].arrayHTTPRequests removeObject:self];
}
- (void)dealloc {
    ASIRequest = nil;
    delegate = nil;
}
@end