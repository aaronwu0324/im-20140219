
enum chatMessageType { //左邊還是右邊（自己還是對方）
    chatMessageTypeRight = 0,
    chatMessageTypeLeft = 1,
} chatMessageType;

enum chatMessageKind { //訊息種類
    chatMessageKindMessage = 0, //文字訊息
    chatMessageKindImage = 1, //圖片
    chatMessageKindSound = 2, //聲音
    chatMessageKindSticker = 3, //貼圖
} chatMessageKind;

#import <UIKit/UIKit.h>
#import "HTTPImageView.h"
#import "ViewChatVoicePlayer.h"

@interface ChatSubViewCell : UITableViewCell <ViewChatVoicePlayerDelegate,UIActionSheetDelegate> {
    BOOL isPlayingVoice; //是否正在播放聲音
}
@property NSInteger messageType;
@property (nonatomic,weak) NSString *Message;
@property (nonatomic,weak) NSString *message_id;
@property (nonatomic,weak) NSString *nickname;
@property (nonatomic,weak) NSString *member_id;
@property (nonatomic,weak) NSString *avatarUrl;
@property (nonatomic,weak) NSString *datetime;
@property BOOL isRead;
@property BOOL isSend;
@property NSInteger ChatRoomUserType; //群聊還是個聊
@property (nonatomic,weak) IBOutlet UILabel *LabelName;
@property (nonatomic,weak) IBOutlet UILabel *LabelMessage;
@property (nonatomic,weak) IBOutlet UILabel *LabelDate;
@property (nonatomic,weak) IBOutlet UILabel *LabelRead;
@property (nonatomic,weak) IBOutlet UIImageView *ImageRight;
@property (nonatomic,weak) IBOutlet UIImageView *ImageMid;
@property (nonatomic,weak) IBOutlet HTTPImageView *ImageAvatar;
@property (nonatomic,weak) IBOutlet UIButton *ButtonPlaySound;
@property (nonatomic,weak) IBOutlet UIButton *ButtonReadStatus;
@property (nonatomic,weak) IBOutlet UIButton *ButtonShowImage;
-(void) UpdateUI;
@end
