
@protocol api_contact_check_contactDelegate <NSObject>
@optional
-(void) api_contact_check_contactComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_contact_check_contact : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_contact_check_contactDelegate> delegate;
}
@property (weak) id delegate;
-(void) CheckContactWithAccount:(NSString *)account;
@end
