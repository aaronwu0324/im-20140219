
@protocol api_member_member_dataDelegate <NSObject>
@optional
-(void) api_member_member_dataComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_member_member_data : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_member_member_dataDelegate> delegate;
}
@property (weak) id delegate;
-(void) GetMemberDataWithAccount:(NSString *)account;
@end
