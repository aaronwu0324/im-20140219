

#import <UIKit/UIKit.h>
#import "rootViewController.h"
#import "api_member_search_member.h"
#import "api_contact_create_contact.h"
#import "api_contact_check_contact.h"
#import "api_contact_delete_contact_by_id.h"
#import "api_group_list_my_group.h"
#import "api_group_modify_group_member.h"
#import "api_group_create_group.h"

@interface NameList : rootViewController <api_member_search_memberDelegate,UIActionSheetDelegate,api_contact_create_contactDelegate,api_contact_check_contactDelegate,UIGestureRecognizerDelegate,api_contact_delete_contact_by_idDelegate,api_group_list_my_groupDelegate,api_group_modify_group_memberDelegate,api_group_create_groupDelegate>
-(void)GetContact; //在抓一次聯絡人清單，如果收到通知，抓完之後會自動跟該人聊天
@end
