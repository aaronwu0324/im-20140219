

#import "api_mqtt_group_create.h"
#import "JSON.h"

@implementation api_mqtt_group_create
@synthesize delegate;
#pragma mark -
#pragma mark Delegate
-(void) api_mqtt_group_createComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    if ([[self delegate] respondsToSelector:@selector(api_mqtt_group_createComplete:Message:Data:)]) {
        [[self delegate] api_mqtt_group_createComplete:success Message:message Data:data];
    }
}
#pragma mark -
#pragma mark ASIHTTPRequest
-(void) CreateMQTTChannelWithGroupID:(NSString *)gid {
    NSString *urlString = [NSString stringWithFormat:@"%@/mqtt/group_create?gid=%@",kMQTTGateway,gid];
    NSLog(@"api_mqtt/group_create url:%@",urlString);
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIRequest = [[ASIHTTPRequest alloc] initWithURL:url];
    [ASIRequest setTimeOutSeconds:kURLTimeoutSecond];
    [ASIRequest setNumberOfTimesToRetryOnTimeout:kURLRetryOnTimeout];
    [ASIRequest setDelegate:self];
    [ASIRequest startAsynchronous];
}
- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    //NSLog(@"api_mqtt/group_create Finished:%@",responseString);
    
    NSError *error = nil;
	SBJSON *parser = [SBJSON new];
	
    //原始資料
	NSDictionary *result = (NSDictionary *) [parser objectWithString:responseString error:&error];
    NSLog(@"api_mqtt/group_create result:%@",result);
    
    while (error) {
        NSLog(@"json error:%@",[error localizedDescription]);
        error = [[error userInfo] objectForKey:NSUnderlyingErrorKey];
        NSLog(@"Error trace: %@", parser.errorTrace);
        [self api_mqtt_group_createComplete:NO Message:kMessageNetworkFail Data:nil];
        [self performSelector:@selector(Cancle)];
        return;
    }
    
    NSString *sys_msg = [result objectForKey:@"error_msg"];
    NSString *sys_code = [result objectForKey:@"error_code"];
    NSLog(@"api_mqtt/group_create:(%@)%@",sys_code,sys_msg);
    
    if ([sys_code isEqualToString:@"200"]) { //200成功
        NSString *channel = [result objectForKey:@"channel"];
        NSDictionary *data = result;
        [self api_mqtt_group_createComplete:YES Message:channel Data:data];
    }
    else { //失敗
        [self api_mqtt_group_createComplete:NO Message:@"聊天功能失敗，請重試一次" Data:nil];
    }
    
    [self performSelector:@selector(Cancle)];
}
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"requestFailed error:%@",error);
    [self api_mqtt_group_createComplete:NO Message:kMessageNetworkFail Data:nil];
    [self performSelector:@selector(Cancle)];
}
#pragma mark -
#pragma mark 雜項
-(void) Cancle {
    // Cancels an asynchronous request
    [ASIRequest cancel];
    
    // Cancels an asynchronous request, clearing all delegates and blocks first
    [ASIRequest clearDelegatesAndCancel];
    
    //移除物件
    [[VariableStore sharedInstance].arrayHTTPRequests removeObject:self];
}
- (void)dealloc {
    ASIRequest = nil;
    delegate = nil;
}
@end