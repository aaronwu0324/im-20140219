

#import "ChatSubViewCell.h"
#import "DBProcess.h"

@implementation ChatSubViewCell
@synthesize messageType;
@synthesize LabelName;
@synthesize LabelMessage;
@synthesize LabelDate;
@synthesize LabelRead;
@synthesize ImageRight;
@synthesize ImageMid;
@synthesize ImageAvatar;
@synthesize Message;
@synthesize nickname;
@synthesize member_id;
@synthesize avatarUrl;
@synthesize isRead;
@synthesize isSend;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void) UpdateUI {
    //調整UI 左右不同（預設是右）
    if (messageType == chatMessageTypeLeft) {
        ImageAvatar.frame = CGRectMake(5, 0, ImageAvatar.frame.size.width, ImageAvatar.frame.size.height);
        ImageRight.image = [UIImage imageNamed:@"chat_bubble01c.png"];
        ImageRight.frame = CGRectMake(5, ImageRight.frame.origin.y, ImageRight.frame.size.width, ImageRight.frame.size.height);
        //NSLog(@"ImageRight:%@",ImageRight);
        
        ImageMid.image = [UIImage imageNamed:@"chat_bubble01b.png"];
        ImageMid.frame = CGRectMake(16, ImageMid.frame.origin.y, ImageMid.frame.size.width, ImageMid.frame.size.height);
        //NSLog(@"ImageMid:%@",ImageMid);
        
        LabelMessage.frame = CGRectMake(30, LabelMessage.frame.origin.y, LabelMessage.frame.size.width, LabelMessage.frame.size.height);
        LabelMessage.textColor = [UIColor colorWithRed:69.0/255.0 green:102.0/255.0 blue:132.0/255.0 alpha:1.0];
        
        LabelDate.frame = CGRectMake(ImageMid.frame.origin.x+ImageMid.frame.size.width, ImageMid.frame.origin.y+ImageMid.frame.size.height-LabelDate.frame.size.height, LabelDate.frame.size.width, LabelDate.frame.size.height);
        LabelDate.textAlignment = NSTextAlignmentCenter;
    }
    
    CGFloat shiftHeight = 0; //比原本的大多少
    
    //預設參數
    CGFloat originalHeight = self.frame.size.height;
    
    CGFloat cellHeight = [[CommonTask sharedInstance] cellHeightForText:Message];
    
    //判斷是不是圖片或聲音
    NSInteger messageKind = [[CommonTask sharedInstance] GetChatMessageKindWithString:Message];
    if (messageKind == chatMessageKindImage || messageKind == chatMessageKindSticker) { //圖片或貼圖
        //調整背景圖
        //ImageMid.hidden = YES;
        //ImageRight.hidden = YES;
        
        //載入該圖片（暫以avatar圖片物件代替)
        ImageAvatar.hidden = NO;
        ImageAvatar.frame = CGRectMake(0, 0, 150, 150);
        ImageAvatar.center = CGPointMake(ImageMid.center.x, cellHeight/2-7.5);
        
        if (messageKind == chatMessageKindImage) {
            [ImageAvatar setImageWithURL:Message placeholderImage:nil];
        }
        else { //貼圖做特殊處理
            [ImageAvatar setImageWithURL:[NSString stringWithFormat:@"http://202.74.126.64/mqtt_img/%@",Message] placeholderImage:nil];
        }

        _ButtonPlaySound.hidden = YES;
        
        if (messageKind == chatMessageKindImage) {
            _ButtonShowImage.hidden = NO;
            _ButtonShowImage.frame = ImageAvatar.frame;
        }
    }
    else if (messageKind == chatMessageKindSound) {
        //播放圖與按鈕
        ImageAvatar.hidden = NO;
        ImageAvatar.frame = CGRectMake(0, 0, 40, 40);
        ImageAvatar.center = CGPointMake(ImageMid.center.x, cellHeight/2-7.5);
        [self SetVoicePlayerImageReady];
        
        _ButtonShowImage.hidden = YES;
        _ButtonPlaySound.hidden = NO;
        _ButtonPlaySound.frame = ImageAvatar.frame;
    }
    else { //文字
        //調整背景圖
        //ImageMid.hidden = NO;
        //ImageRight.hidden = NO;
        
        ImageAvatar.hidden = YES;
        _ButtonPlaySound.hidden = YES;
        _ButtonShowImage.hidden = YES;
        
        LabelMessage.text = Message;
    }
    
    shiftHeight = cellHeight - originalHeight;
    NSLog(@"shiftHeight:%f",shiftHeight);
    
    //日期與時間
    //NSLog(@"date:%@",self.datetime);
    NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
    _formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *_date = [_formatter dateFromString:self.datetime];
    _formatter.timeStyle = NSDateFormatterShortStyle;
    LabelDate.text = [_formatter stringFromDate:_date];
    
    //判斷已讀跟已發
    if (isRead) {
        self.LabelRead.hidden = NO;
        self.LabelRead.text = @"已讀";
    }
    else if (isSend) {
        self.LabelRead.hidden = NO;
        self.LabelRead.text = @"已送出";
    }
    else {
        self.LabelRead.hidden = YES;
    }
    
    //NSLog(@"shiftHeight:%f",shiftHeight);
    ImageMid.frame = CGRectMake(ImageMid.frame.origin.x, ImageMid.frame.origin.y, ImageMid.frame.size.width, ImageMid.frame.size.height + shiftHeight);
    
    LabelMessage.frame = CGRectMake(LabelMessage.frame.origin.x,ImageMid.frame.origin.y, LabelMessage.frame.size.width, ImageMid.frame.size.height);
    
    //移動日期
    LabelDate.frame = CGRectMake(LabelDate.frame.origin.x,LabelDate.frame.origin.y + shiftHeight + 5, LabelDate.frame.size.width, LabelDate.frame.size.height);
    
    //調整自己的大小
    self.frame = CGRectMake(0, 0, self.frame.size.width, LabelDate.frame.origin.y + LabelDate.frame.size.height + 5);
    
    //如果是文字或Sticker，且只有一行，依據訊息內容調整寬度
    if ((messageKind == chatMessageKindMessage || messageKind == chatMessageKindSticker) && shiftHeight <= 0) {
        //計算寬度
        CGSize maxSize = CGSizeMake(210, 500);
        
        CGSize StringSize = [Message boundingRectWithSize:maxSize options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:14] } context: nil].size;
        CGFloat width = StringSize.width + 40;
        NSLog(@"width:%f",width);
        
        if (messageType == chatMessageTypeLeft) {
            LabelMessage.textAlignment = NSTextAlignmentLeft;
            ImageMid.frame = CGRectMake(16, ImageMid.frame.origin.y, width, ImageMid.frame.size.height);
            
            //移動日期
            LabelDate.frame = CGRectMake(ImageMid.frame.origin.x+ImageMid.frame.size.width,LabelDate.frame.origin.y-5, LabelDate.frame.size.width, LabelDate.frame.size.height);
        }
        else {
            LabelMessage.textAlignment = NSTextAlignmentRight;
            ImageMid.frame = CGRectMake(ImageMid.frame.origin.x+ImageMid.frame.size.width - width, ImageMid.frame.origin.y, width, ImageMid.frame.size.height);
            
            //移動日期
            LabelDate.frame = CGRectMake(ImageMid.frame.origin.x - LabelDate.frame.size.width,LabelDate.frame.origin.y-2, LabelDate.frame.size.width, LabelDate.frame.size.height);
        }
    }
    
    //狀態
    LabelRead.frame = CGRectMake(LabelDate.frame.origin.x,LabelDate.frame.origin.y - LabelRead.frame.size.height , LabelRead.frame.size.width, LabelRead.frame.size.height);
    
    //群聊且是對方發的訊息，顯示並調整他的名稱
    if (messageType == chatMessageTypeLeft && _ChatRoomUserType == ChatRoomUserType_GroupChat) {
        LabelRead.hidden = YES; //不需顯示已讀
        LabelName.hidden = NO;
        LabelName.frame = LabelRead.frame;
        LabelName.text = self.nickname;
        NSLog(@"%@ %@",LabelName.text,LabelName);
    }
    
    if (_ChatRoomUserType == ChatRoomUserType_GroupChat && messageType == chatMessageTypeRight) { //群聊的時候，自己發的，出現讀取狀態按鈕
        _ButtonReadStatus.hidden = NO;
        _ButtonReadStatus.center = LabelRead.center;
        LabelRead.hidden = YES;
    }
}
#pragma mark - 播放聲音
-(IBAction) PlaySound {
    if (isPlayingVoice) return;
    
    isPlayingVoice = YES;
    
    //拆解vocie的格式，拼成url
    NSLog(@"Message:%@",Message);
    NSInteger startPoint = [Message rangeOfString:@"}"].location;
    NSString *key = [Message substringFromIndex:startPoint+1];
    NSString *urlString = [NSString stringWithFormat:@"http://mqttnewyear.jampush.com.tw:3000/voice/%@.mp4",key];
    NSLog(@"urlString:%@",urlString);
    
    ViewChatVoicePlayer *player = [[ViewChatVoicePlayer alloc] init];
    player.delegate = self;
    [player PlayVoiceWithUrl:urlString];
}
-(void) ViewChatVoicePlayerStart {
    [self SetVoicePlayerImagePlay];
}
-(void) ViewChatVoicePlayerFinish {
    [self SetVoicePlayerImageReady];
    
    isPlayingVoice = NO;
}
-(void) SetVoicePlayerImagePlay {
    if (messageType == chatMessageTypeLeft) {
        [ImageAvatar setImage:[UIImage imageNamed:@"chat_voice_playl02.png"]];
    }
    else {
        [ImageAvatar setImage:[UIImage imageNamed:@"chat_voice_playr02.png"]];
    }
    
    //閃爍
    ImageAvatar.alpha = 1.0f;
    [UIView animateWithDuration:1.0f
                          delay:0.0f
                        options:UIViewAnimationOptionAutoreverse
                     animations:^ {
                         [UIView setAnimationRepeatCount:100];
                         ImageAvatar.alpha = 0.0f;
                     } completion:^(BOOL finished) {
                         
                     }];
}
-(void) SetVoicePlayerImageReady {
    if (messageType == chatMessageTypeLeft) {
        [ImageAvatar setImage:[UIImage imageNamed:@"chat_voice_playl01.png"]];
    }
    else {
        [ImageAvatar setImage:[UIImage imageNamed:@"chat_voice_playr01.png"]];
    }
    
    //還原
    [ImageAvatar.layer removeAllAnimations];
    ImageAvatar.alpha = 1.0f;
}
#pragma mark - 圖片
-(IBAction) ShowImage {
    //把訊息（圖片網址）傳到聊天室
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ClickImageAtCellWithUrl" object:Message];
}
#pragma mark - 讀取狀態
-(IBAction) ShowGroupReadStatus {
    NSLog(@"message_id:%@",_message_id);
    
    //從db取出讀取該訊息的有誰
    DBProcess *process = [[DBProcess alloc] init];
    NSArray *arrayAccount = [process GetReadListWithMessageID:_message_id];
    
    UIActionSheet *menu = [[UIActionSheet alloc]
                           initWithTitle:@"讀取狀態"
                           delegate:self
                           cancelButtonTitle:nil
                           destructiveButtonTitle:@"關閉"
                           otherButtonTitles:nil];
    
    //加入title
    for (NSDictionary *_dic  in arrayAccount) {
        NSString *account = [_dic objectForKey:@"account"];
        NSString *_isRead = [_dic objectForKey:@"isRead"];
        
        NSString *text = account;
        if ([_isRead isEqualToString:@"1"]) { //已讀
            text = [NSString stringWithFormat:@"\U0001F6A9 %@",account];
        }
        [menu addButtonWithTitle:text];
    }
    
    //@"\U0001F6A9 \U0001F4CC \u26F3 \u2690 \u2691 \u274F \u25A4 Test"
    //@"\U0001F4D6 \U0001F30E \U0001F30F \u25A6 \U0001F3C1 \U0001F332 \U0001F333 \U0001F334 Test"
    
    //取消按鈕
    //[menu addButtonWithTitle:@"取消"];
    //menu.destructiveButtonIndex = menu.numberOfButtons - 1;
    
    //顯示
    [menu showInView:[self superview]];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"clickedButtonAtIndex:%d",buttonIndex);
    if (buttonIndex < 0) { //iPad按外面的話index是-1
        return;
    }
    
    NSString *selectedString = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    //選到取消
    if ([selectedString isEqualToString:@"取消"]) return;
}
#pragma mark - 雜項
-(void) dealloc {
    //NSLog(@"ChatSubViewCell dealloc");
}
@end
