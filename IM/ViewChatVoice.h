@protocol ViewChatVoiceDelegate <NSObject>
@optional
-(void) ViewChatVoiceRecordingStart; //開始錄音
-(void) ViewChatVoiceRecordingSuccessWithUrl:(NSString *)urlString; //錄音完畢且上傳成功
@end

enum RecordType {
    RecordType_Drive = 1, //行車錄音模式
    RecordType_Normal = 2, //一般錄音模式
}RecordType;

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "api_upload_voice.h"

@interface ViewChatVoice : UIView <AVAudioRecorderDelegate,api_upload_voiceDelegate,UIAlertViewDelegate>  {
    NSInteger numbersOfRecordSecond; //總共錄音了幾秒
    NSInteger recordType;
    __weak id <ViewChatVoiceDelegate> delegate;
}
@property (weak) id delegate;
@property (nonatomic,weak) IBOutlet UIButton *ButtonRecordNormal;
@property (nonatomic,weak) IBOutlet UIButton *ButtonRecordDrive;
@property (nonatomic,weak) IBOutlet UILabel *LabelRecordDrive;
@property (nonatomic,weak) IBOutlet UIButton *ButtonKeyboard;
@property (nonatomic,weak) IBOutlet UIButton *ButtonBack;
@property (nonatomic,weak) IBOutlet UIImageView *ImageSecond;
@property (nonatomic,weak) IBOutlet UILabel *LabelSecond;
@property (nonatomic,weak) IBOutlet UILabel *LabelNotification;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
-(void) UpdateUI;
@end
