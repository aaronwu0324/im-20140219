

#import <UIKit/UIKit.h>
#import "rootViewController.h"
#import "api_mqtt_create.h"
#import "api_mqtt_group_create.h"
#import "api_mqtt_channel_log.h"
#import "api_pic_upload_pic.h"
#import "ViewChatFace.h"
#import "ViewChatVoice.h"
#import "MyImagePicker.h"

@interface MQTTChatRoom : rootViewController <api_mqtt_createDelegate,api_mqtt_channel_logDelegate,ViewChatFaceDelegate,api_pic_upload_picDelegate,UIActionSheetDelegate,ViewChatVoiceDelegate,api_mqtt_group_createDelegate>
@property (nonatomic,weak) NSString *friendMemberId; //要聊天的ID
@property NSInteger ChatRoomUserType; //群聊還是個聊
@property (nonatomic,strong) NSString *gid;   //群聊的群組id
@property (nonatomic,strong) NSString *group_name; //群聊的群組名稱
@end
