enum Tag_Switch {
    Tag_Switch_ON = 0,
    Tag_Switch_OFF = 1,
} Tag_Switch;

#import "Setup.h"
#import "HTTPImageView.h"
#import "User.h"

@interface Setup () {
    MyImagePicker *myPicker;
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet HTTPImageView *ImageTitle;
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic,weak) IBOutlet UIButton *ButtonSetupAvatar;
@property (nonatomic,weak) IBOutlet UIButton *ButtonVibrateSwitch;
@property (nonatomic,weak) IBOutlet UIButton *ButtonSoundSwitch;
@property (nonatomic,weak) IBOutlet UILabel *LabelTrendmark;
@property (nonatomic,weak) IBOutlet UILabel *LabelVersion;
@end

@implementation Setup
#pragma mark - 初始化相關
-(void) initParameter { //初始化參數
    [self GetMemberData];
}
-(void) initUIKit { //初始化畫面
    //判斷目前的設定
    if ([[CommonTask sharedInstance] DidVibrateON]) {
        [self VibrateOn];
    }
    else {
        [self VibrateOff];
    }
    
    if ([[CommonTask sharedInstance] DidSoundON]) {
        [self SoundOn];
    }
    else {
        [self SoundOff];
    }
    
    //調整ScrollView
    if (!IS_IPHONE_5) {
        self.ScrollView.contentSize = CGSizeMake(self.ScrollView.frame.size.width,
                                                 _LabelTrendmark.frame.origin.y+_LabelTrendmark.frame.size.height);
    }
    else {
        self.ScrollView.contentSize = CGSizeMake(self.ScrollView.frame.size.width,
                                                 _LabelTrendmark.frame.origin.y+_LabelTrendmark.frame.size.height+48);
    }
    NSLog(@"size:%@",NSStringFromCGSize(self.ScrollView.contentSize));
    
    //版本
    NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
    _LabelVersion.text = [NSString stringWithFormat:@"版本：%@",versionString];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"登出" style:UIBarButtonItemStyleDone target:self action:@selector(Logout)];
    
}
#pragma mark - API-查詢會員資料
-(void) GetMemberData {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"資料下載中"];
    
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_member_member_data *api = [[api_member_member_data alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetMemberDataWithAccount:account];
}
-(void) api_member_member_dataComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        self.LabelTitle.text = [data objectForKey:@"member_chinese_name"];
        
        NSString *imgUrl = [data objectForKey:@"avatar"];
        if ([imgUrl length] > 0) [self.ImageTitle setImageWithURL:imgUrl placeholderImage:nil];
        
        //把相關資料存起來
        [[User sharedUser] updateInfo:data];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 照片相關
-(IBAction)ChooseImageSource:(id)sender {
    myPicker = [[MyImagePicker alloc] init];
    myPicker.SourceVC = self;
    myPicker.delegate = self;
    [myPicker UpdateUI];
}
-(void) MyImagePickerFinishedWithImage:(UIImage *)image {
    self.ImageTitle.image = image;
    [self UpdateAvatar];
}
#pragma mark - 震動提醒
-(IBAction)ClickVibrateButton:(UIButton *)sender {
    if (sender.tag == Tag_Switch_ON) { //開啟，改為關閉
        [self VibrateOff];
    }
    else { //關閉改為開啟
        [self VibrateOn];
    }
}
-(void) VibrateOn {
    //設定按鈕
    self.ButtonVibrateSwitch.tag = Tag_Switch_ON;
    [self.ButtonVibrateSwitch setBackgroundImage:[UIImage imageNamed:@"setting_switch01.png"] forState:UIControlStateNormal];
    
    //存起來
    [[CommonTask sharedInstance] SetVibrate:@"Y"];
}
-(void) VibrateOff {
    //設定按鈕
    self.ButtonVibrateSwitch.tag = Tag_Switch_OFF;
    [self.ButtonVibrateSwitch setBackgroundImage:[UIImage imageNamed:@"setting_switch02.png"] forState:UIControlStateNormal];
    
    //存起來
    [[CommonTask sharedInstance] SetVibrate:@"N"];
}
#pragma mark - 聲音提醒
-(IBAction)ClickSoundButton:(UIButton *)sender {
    if (sender.tag == Tag_Switch_ON) { //開啟，改為關閉
        [self SoundOff];
    }
    else { //關閉改為開啟
        [self SoundOn];
    }
}
-(void) SoundOn {
    //設定按鈕
    self.ButtonSoundSwitch.tag = Tag_Switch_ON;
    [self.ButtonSoundSwitch setBackgroundImage:[UIImage imageNamed:@"setting_switch01.png"] forState:UIControlStateNormal];
    
    //存起來
    [[CommonTask sharedInstance] SetSound:@"Y"];
}
-(void) SoundOff {
    //設定按鈕
    self.ButtonSoundSwitch.tag = Tag_Switch_OFF;
    [self.ButtonSoundSwitch setBackgroundImage:[UIImage imageNamed:@"setting_switch02.png"] forState:UIControlStateNormal];
    
    //存起來
    [[CommonTask sharedInstance] SetSound:@"N"];
}
#pragma mark - 換頁相關
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if(segue.sourceViewController == self && [segue.identifier isEqualToString:@"GotoSetupAvatar"]) {
        SetupAvatar *vc = segue.destinationViewController;
        [vc.navigationItem setHidesBackButton:YES];
        vc.Image.image = (UIImage *)sender;
        vc.selectedImage = (UIImage *)sender;
        vc.delegate = self;
    }
}
#pragma mark -
#pragma mark 照片縮放結果
-(void) SetupAvatarScaleAndMoveView_Cancel {
}
-(void) SetupAvatarScaleAndMoveView_Confirm:(UIImage *)capturedImage {
    NSLog(@"capturedImage:%@",NSStringFromCGSize(capturedImage.size));
    
    self.ImageTitle.image = capturedImage;
    
    [self UpdateAvatar];
}
#pragma mark - API-上傳大頭照
-(void) UpdateAvatar {
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_member_update_avatar *api = [[api_member_update_avatar alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api UpdateAvatarWithAccount:account Avatar:self.ImageTitle.image];
}
-(void) api_member_update_avatarComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    if (success) { //更新UI
        [self.ImageTitle setImageWithURL:[data objectForKey:@"avatar_url"] placeholderImage:nil];
    }
    else {
    }
}
#pragma mark - 登出
-(IBAction)Logout {
    //清除密碼
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:kDEFAULTS_KEY_MEMBER_PASSWORD];
    [defaults synchronize];
    
    //回登入頁
    [self.presentingViewController  dismissViewControllerAnimated:NO completion:nil];
}
#pragma mark - 常用字詞
-(IBAction)SetupSentence {
    [self performSegueWithIdentifier:@"GotoSetupSentence" sender:nil];
}
#pragma mark - 雜項
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
}
@end
