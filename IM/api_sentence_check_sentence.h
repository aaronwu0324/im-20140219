
@protocol api_sentence_check_sentenceDelegate <NSObject>
@optional
-(void) api_sentence_check_sentenceComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_sentence_check_sentence : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_sentence_check_sentenceDelegate> delegate;
}
@property (weak) id delegate;
-(void) CheckSentenceWithAccount:(NSString *)account;
@end
