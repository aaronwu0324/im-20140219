

#import <UIKit/UIKit.h>
#import "rootViewController.h"
#import "api_sentence_check_sentence.h"
#import "api_sentence_create_sentence.h"
#import "api_sentence_delete_sentence_by_id.h"

@interface SetupSentence : rootViewController <api_sentence_check_sentenceDelegate,api_sentence_create_sentenceDelegate,api_sentence_delete_sentence_by_idDelegate>
@end
