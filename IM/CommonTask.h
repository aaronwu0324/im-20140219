
#import <Foundation/Foundation.h>
#import "ChatSubViewCell.h"

@interface CommonTask : NSObject {
}

// message from which our instance is obtained
+ (CommonTask *)sharedInstance;
-(BOOL) DidVibrateON; //判斷設定的震動有沒有打開
-(void) SetVibrate:(NSString *)OnOrOff; //設定震動
-(BOOL) DidSoundON; //判斷設定的聲音有沒有打開
-(void) SetSound:(NSString *)OnOrOff; //設定聲音
-(NSArray *)SortArrayByDate:(NSArray *)data Key:(NSString *)key;
-(NSInteger) GetChatMessageKindWithString:(NSString *)message; //判斷訊息是文字、圖片還是聲音
-(NSString *) GetChatMessageKindNameWithString:(NSString *)message; //判斷訊息是文字、圖片還是聲音
-(CGFloat) cellHeightForText:(NSString *)text; //計算訊息的cell高度
@end
