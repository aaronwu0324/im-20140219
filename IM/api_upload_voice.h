
@protocol api_upload_voiceDelegate <NSObject>
@optional
-(void) api_upload_voiceComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface api_upload_voice : NSObject <ASIHTTPRequestDelegate> {
    ASIFormDataRequest *ASIRequest;
    __weak id <api_upload_voiceDelegate> delegate;
}
@property (weak) id delegate;
-(void) UploadVoiceWithTitle:(NSString *)title Pic:(NSString *)voiceFilePath;
@end
