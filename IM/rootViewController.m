

#import "rootViewController.h"

@interface rootViewController ()

@end

@implementation rootViewController
#pragma mark -
#pragma mark UIViewControll 初始化相關
- (void)viewDidLoad {
    [super viewDidLoad];
	[self initParameter];
	[self initUIKit];
}
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    [self AddNotification];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar.png"] forBarMetrics:UIBarMetricsDefault]; //避免Navigation影響Status Bar不見的問題
}
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
    [self RemoveNotification];
}
-(void) viewDidUnload {
    [super viewDidUnload];
}
#pragma mark -
#pragma mark 初始化相關
-(void) initParameter { //初始化參數
}
-(void) initUIKit { //初始化畫面
}
#pragma mark -
#pragma mark Notification
-(void) AddNotification {
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SystemLock) name:@"SystemLock" object:nil];
}
-(void) RemoveNotification {
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"SystemLock" object:nil];
}
#pragma mark -
#pragma mark 雜項
- (BOOL)prefersStatusBarHidden {
    return NO;
}
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
}
@end
