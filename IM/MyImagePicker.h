@protocol MyImagePickerDelegate <NSObject>
@optional
-(void) MyImagePickerFinishedWithImage:(UIImage *)image;
@end

#import <Foundation/Foundation.h>

@interface MyImagePicker : NSObject <UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate> {
    UIImagePickerController *imagePicker;
    __weak id <MyImagePickerDelegate> delegate;
}
//@property (nonatomic,strong) UIActionSheet *menu;
@property (weak) id delegate;
@property (weak) UIViewController *SourceVC;
-(void) UpdateUI;
@end
