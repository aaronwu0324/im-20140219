

#import "api_member_search_member.h"
#import "JSON.h"

@implementation api_member_search_member
@synthesize delegate;
#pragma mark -
#pragma mark Delegate
-(void) api_member_search_memberComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    if ([[self delegate] respondsToSelector:@selector(api_member_search_memberComplete:Message:Data:)]) {
        [[self delegate] api_member_search_memberComplete:success Message:message Data:data];
    }
}
#pragma mark -
#pragma mark ASIHTTPRequest
-(void) SearchMemberWithName:(NSString *)member_name {
    NSString *urlString = [NSString stringWithFormat:@"%@/api_member/search_member?member_name=%@",kSYSGatewayTemp,member_name];
    NSLog(@"api_member/search_member url:%@",urlString);
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIRequest = [[ASIHTTPRequest alloc] initWithURL:url];
    [ASIRequest setTimeOutSeconds:kURLTimeoutSecond];
    [ASIRequest setNumberOfTimesToRetryOnTimeout:kURLRetryOnTimeout];
    [ASIRequest setDelegate:self];
    [ASIRequest startAsynchronous];
}
- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    NSLog(@"api_member/search_member Finished:%@",responseString);
    
    NSError *error = nil;
	SBJSON *parser = [SBJSON new];
	
    //原始資料
	NSDictionary *result = (NSDictionary *) [parser objectWithString:responseString error:&error];
    NSLog(@"api_member/search_member result:%@",result);
    
    while (error) {
        NSLog(@"json error:%@",[error localizedDescription]);
        error = [[error userInfo] objectForKey:NSUnderlyingErrorKey];
        NSLog(@"Error trace: %@", parser.errorTrace);
        [self api_member_search_memberComplete:NO Message:kMessageNetworkFail Data:nil];
        [self performSelector:@selector(Cancle)];
        return;
    }
    
    NSString *sys_msg = [result objectForKey:@"sys_msg"];
    NSString *sys_code = [result objectForKey:@"sys_code"];
    NSLog(@"api_member/search_member:(%@)%@",sys_code,sys_msg);
    
    if ([sys_code isEqualToString:@"200"]) { //200成功
        NSArray *data = [result objectForKey:@"member"];
        if ([data count] > 0) {
            [self api_member_search_memberComplete:YES Message:nil Data:data];
        }
        else {
            [self api_member_search_memberComplete:NO Message:@"找不到符合的聯絡人" Data:data];
        }
    }
    else { //失敗
        [self api_member_search_memberComplete:NO Message:sys_msg Data:nil];
    }
    
    [self Cancle];
}
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"requestFailed error:%@",error);
    [self api_member_search_memberComplete:NO Message:kMessageNetworkFail Data:nil];
    [self Cancle];
}
#pragma mark -
#pragma mark 雜項
-(void) Cancle {
    // Cancels an asynchronous request
    [ASIRequest cancel];
    
    // Cancels an asynchronous request, clearing all delegates and blocks first
    [ASIRequest clearDelegatesAndCancel];
    
    //移除物件
    [[VariableStore sharedInstance].arrayHTTPRequests removeObject:self];
}
- (void)dealloc {
    ASIRequest = nil;
    delegate = nil;
}
@end