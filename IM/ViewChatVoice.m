
#import "ViewChatVoice.h"

@implementation ViewChatVoice
@synthesize delegate;
#pragma mark - Delegate
-(void) ViewChatVoiceRecordingStart {
    if ([[self delegate] respondsToSelector:@selector(ViewChatVoiceRecordingStart)]) {
        [[self delegate] ViewChatVoiceRecordingStart];
    }
}
-(void) ViewChatVoiceRecordingSuccessWithUrl:(NSString *)urlString { //錄音完畢且上傳成功
    if ([[self delegate] respondsToSelector:@selector(ViewChatVoiceRecordingSuccessWithUrl:)]) {
        [[self delegate] ViewChatVoiceRecordingSuccessWithUrl:urlString];
    }
}
#pragma mark - 初始化
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}
-(void) UpdateUI {
    //參數初始化
    numbersOfRecordSecond = 0;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        //建立recorder跟player
        NSArray *dirPaths;
        NSString *docsDir;
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = dirPaths[0];
        
        NSString *soundFilePath = [docsDir stringByAppendingPathComponent:@"sound.mp4"];
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        NSDictionary *recordSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        [NSNumber numberWithFloat: 32000], AVSampleRateKey,
                                        [NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                        [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                                        
                                        nil];
        
        NSError *error = nil;
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                            error:nil];
        
        _audioRecorder = [[AVAudioRecorder alloc]
                          initWithURL:soundFileURL
                          settings:recordSettings
                          error:&error];
        _audioRecorder.delegate = self;
        
        if (error) {
            NSLog(@"error: %@", [error localizedDescription]);
        }
        else {
            [_audioRecorder prepareToRecord];
        }
    });
}
#pragma mark - 錄音模式
-(IBAction) RecordDrive { //行車錄音
    _LabelRecordDrive.text = @"輕觸按鈕結束錄音";
    
    //顯示通知並消失
    _LabelNotification.hidden = NO;
    [UIView animateWithDuration:3.0
                     animations:^{
                         _LabelNotification.alpha = 0;
                     }
                     completion:^(BOOL finished){
                     }];
    
    recordType = RecordType_Drive;
    
    [self recordAudio];
}
-(IBAction) RecordNormalStart { //一般錄音
    recordType = RecordType_Normal;
    
    [self recordAudio];
}
#pragma mark - 錄音控制
- (void)recordAudio {
    //按鈕改為結束錄音
    [_ButtonRecordDrive removeTarget:self action:@selector(RecordDrive) forControlEvents:UIControlEventTouchUpInside];
    [_ButtonRecordDrive addTarget:self action:@selector(stopRecordAudio) forControlEvents:UIControlEventTouchUpInside];
    [_ButtonRecordNormal removeTarget:self action:@selector(RecordNormalStart) forControlEvents:UIControlEventTouchDown];
    [_ButtonRecordNormal addTarget:self action:@selector(stopRecordAudio) forControlEvents:UIControlEventTouchUpInside];
    
    if (recordType == RecordType_Drive) { //行車
    }
    else { //一般
        [_ButtonRecordNormal addTarget:self action:@selector(stopRecordAudio) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (!_audioRecorder.recording) {
        NSLog(@"錄音開始");
        
        [_audioRecorder record];
        
        //開始計時
        _ImageSecond.hidden = NO;
        _LabelSecond.hidden = NO;
        [self CalculateRecordSecond];
    }
}
- (void) stopRecordAudio {
    if (_audioRecorder.recording) {
        NSLog(@"錄音結束");
        [_audioRecorder stop];
    }
}
-(void) CalculateRecordSecond { //記錄總共錄音秒數
    numbersOfRecordSecond++;
    
    NSLog(@"已錄音%d秒",numbersOfRecordSecond);
    
    //計算幾分幾秒
    if (recordType == RecordType_Drive) { //行車模式
        _LabelRecordDrive.text = [NSString stringWithFormat:@"輕觸按鈕結束錄音 %@",[self timeFormatted:numbersOfRecordSecond]];
        _LabelSecond.hidden = YES;
        _ImageSecond.hidden = YES;
    }
    else { //一般模式
        _LabelSecond.text = [self timeFormatted:numbersOfRecordSecond];
    }
    
    if (recordType == RecordType_Drive && numbersOfRecordSecond > 30) { //行車模式，錄完30s會自動停止
        [self stopRecordAudio];
    }
    else { //一般模式，或行車模式小於30秒
        //每秒跑一次
        [self performSelector:@selector(CalculateRecordSecond) withObject:nil afterDelay:1.0f];
    }
}
- (NSString *)timeFormatted:(int)totalSeconds {
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (hours > 0) {
        [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    }

    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}
-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
    //取消計時
    _ImageSecond.hidden = YES;
    _LabelSecond.hidden = YES;
    numbersOfRecordSecond = 0;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    NSLog(@"audioRecorderDidFinishRecording");
    if (flag) { //錄音成功
        if (recordType == RecordType_Drive) { //行車模式，詢問要不要上傳
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"傳送語音訊息？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
            [alert show];
        }
        else { //一般模式，錄音成功就上傳
            [self UploadVoice];
        }
    }
}
-(void)audioRecorderEncodeErrorDidOccur:
(AVAudioRecorder *)recorder error:(NSError *)error {
    NSLog(@"Encode Error occurred");
}
#pragma mark - API上傳錄音
-(void) UploadVoice {
    [[VariableStore sharedInstance] ShowHUDInView:self Text:@"上傳中"];
    
    NSArray *dirPaths;
    NSString *docsDir;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    NSString *soundFilePath = [docsDir stringByAppendingPathComponent:@"sound.mp4"];
    
    
    api_upload_voice *api = [[api_upload_voice alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api UploadVoiceWithTitle:@"testTitle" Pic:soundFilePath];
}
-(void) api_upload_voiceComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self];
    
    if (success) {
        /*語音檔案成功上傳至server後，
        以MQTT傳送字串格式如下：
        String message = "{" + DURATION + "}" + JSON.getString("key");
        其中DURATION是聲音檔的長度，用毫秒為單位。
        JSON則是上傳語音檔回傳的json，後面的key是回傳json中的欄位名稱。*/
        
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:_audioRecorder.url options:nil];
        CMTime time = asset.duration;
        double durationInSeconds = CMTimeGetSeconds(time);
        NSInteger duration = durationInSeconds * 1000;
        NSLog(@"durationInSeconds:%f",durationInSeconds);
        
        NSString *key = [data objectForKey:@"key"];
        //NSString *urlString = [NSString stringWithFormat:@"http://mqttnewyear.jampush.com.tw:3000/voice/%@.mp4",key];
        
        NSString *message = [NSString stringWithFormat:@"{%d}%@",duration,key];
        NSLog(@"message:%@",message);
        
        [self ViewChatVoiceRecordingSuccessWithUrl:message];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    [self Back];
}
#pragma mark - 換頁
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *selectedString = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([selectedString isEqualToString:@"確定"]) {
        [self UploadVoice];
    }
}
-(IBAction) Back {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self removeFromSuperview];
}
#pragma mark - 雜項
-(void) dealloc {
    NSLog(@"ViewChatVoice dealloc");
}
@end
