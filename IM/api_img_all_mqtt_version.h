
@protocol api_img_all_mqtt_versionDelegate <NSObject>
@optional
-(void) api_img_all_mqtt_versionComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_img_all_mqtt_version : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_img_all_mqtt_versionDelegate> delegate;
}
@property (weak) id delegate;
-(void) GetMQTTImageVersion;
@end
