

#import "GroupEditCell.h"

@implementation GroupEditCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(IBAction) DeleteMember {
    NSNumber *number = [NSNumber numberWithInt:self.cellIndex];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteMember" object:number];
}
- (void)dealloc {
    //NSLog(@"NameListCell dealloc");
}
@end
