
@protocol api_mqtt_member_dxidDelegate <NSObject>
@optional
-(void) api_mqtt_member_dxidComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_mqtt_member_dxid : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_mqtt_member_dxidDelegate> delegate;
}
@property (weak) id delegate;
-(void) UpdateDxidWithAccount:(NSString *)account Dxid:(NSString *)dxid;
@end
