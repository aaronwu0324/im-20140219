
#import <UIKit/UIKit.h>
#import "HTTPImageView.h"

@interface ViewChatFaceImageView : UIView {
}
@property (nonatomic,weak) IBOutlet HTTPImageView *ImageTitle;
-(void) UpdateUIWithUrl:(NSString *)urlString;
@end
