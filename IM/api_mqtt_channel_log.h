
@protocol api_mqtt_channel_logDelegate <NSObject>
@optional
-(void) api_mqtt_channel_logComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_mqtt_channel_log : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_mqtt_channel_logDelegate> delegate;
}
@property (weak) id delegate;
-(void) GetChannelLogWithChannel:(NSString *)channel Timestamp:(NSString *)timestamp;
@end
