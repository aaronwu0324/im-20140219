#import "ChatList.h"
#import "ChatListCell.h"
#import "DBProcess.h"
#import "MQTTChatRoom.h"

@interface ChatList () {
    NSInteger selectedIndex;
    NSMutableArray *arrayTableView;
}
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@end

@implementation ChatList
#pragma mark - 初始化相關
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    [self GetDataFromDB];
}
-(void) initParameter { //初始化參數
    arrayTableView = [[NSMutableArray alloc] init];
}
-(void) initUIKit { //初始化畫面
}
#pragma mark - 資料取出
-(void) GetDataFromDB {
    //參數歸零
    [arrayTableView removeAllObjects];
    selectedIndex = 0;
    
    NSString *account = [[User sharedUser] ACCOUNT];
    
    //取出跟我聊天的人員資料
    DBProcess *process = [[DBProcess alloc] init];
    NSArray *array = [process GetChatListWithAccount:account];
    for (NSDictionary *_dic in array) [arrayTableView addObject:_dic];
    //NSLog(@"arrayTableView:%@",arrayTableView);
    
    //refresh tableview
    [self.TableView reloadData];
}
#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}
#pragma mark - tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	//製作可重複利用的表格欄位Cell
    static NSString *CellIdentifier = @"ChatListCell";
    
    ChatListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[ChatListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    NSLog(@"_dic:%@",_dic);
    
    NSString *fromId = [_dic objectForKey:@"fromId"];
    //標題
    cell.LabelTitle.text = fromId;

    //Avatar
    NSString *avatarUrl = [NSString stringWithFormat:@"http://mqttnewyearmem.cloudapp.net:8088/upload/avatar/%@.png",fromId];
    NSLog(@"avatarUrl:%@",avatarUrl);
    [cell.ImageTitle setImageWithURL:avatarUrl placeholderImage:nil];
    
    //內容
    NSString *message = [_dic objectForKey:@"message"];
    NSInteger messageKind = [[CommonTask sharedInstance] GetChatMessageKindWithString:message];
    if (messageKind == chatMessageKindMessage) { //顯示文字內容
        cell.LabelContent.text = message;
    }
    else { //顯示類別
        cell.LabelContent.text = [[CommonTask sharedInstance] GetChatMessageKindNameWithString:message];
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex = indexPath.row;
    
    [self GetMemberData];
}
#pragma mark - API-查詢會員資料
-(void) GetMemberData {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    //下載要聊天對象的資料
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *account = [_dic objectForKey:@"fromId"];
    
    api_member_member_data *api = [[api_member_member_data alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetMemberDataWithAccount:account];
}
-(void) api_member_member_dataComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        //跟該人聊天
        [self performSegueWithIdentifier:@"GotoMQTTChatRoom" sender:data];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 換頁
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"GotoMQTTChatRoom"]) {
        NSDictionary *_dic = (NSDictionary *)sender;
        NSLog(@"_dic:%@",_dic);
        
        [User sharedUser].chatTarget = [_dic mutableCopy];
        MQTTChatRoom *vc = segue.destinationViewController;
        vc.hidesBottomBarWhenPushed = YES; //隱藏下方的TabBar
        vc.friendMemberId = [_dic objectForKey:@"account"];
        vc.ChatRoomUserType = ChatRoomUserType_SingleChat;
    }
    
    [self.TableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0] animated:YES];
}

#pragma mark - 雜項
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
}
@end
