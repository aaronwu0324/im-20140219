
#import "ViewChatFace.h"
#import "GDataXMLNode.h"
#import "ViewChatFaceImageCell.h"

#define objectPerRow 3

@implementation ViewChatFace
@synthesize delegate;
#pragma mark - Delegate
-(void) ViewChatFaceSelectedFace:(NSString *)selectedFace { //選擇到的表情符號
    if ([[self delegate] respondsToSelector:@selector(ViewChatFaceSelectedFace:)]) {
        [[self delegate] ViewChatFaceSelectedFace:selectedFace];
    }
}
-(void) ViewChatFaceSelectedImage:(NSString *)urlString { //選擇到的圖片網址
    if ([[self delegate] respondsToSelector:@selector(ViewChatFaceSelectedImage:)]) {
        [[self delegate] ViewChatFaceSelectedImage:urlString];
    }
}
#pragma mark - 初始化
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}
-(void) UpdateUI {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClickImageAtChatFace:) name:@"ClickImageAtChatFace" object:nil];
    
    //初始參數
    arrayFaces = [[NSMutableArray alloc] init];
    arrayTableView = [[NSMutableArray alloc] init];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        //讀取XML資料
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"emoticon" ofType:@"xml"];
        NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:filePath];
        NSError *error;
        GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData
                                                               options:0 error:&error];
        if (doc == nil) {
        }
        
        //NSLog(@"%@", doc.rootElement);
        
        NSArray *_arrayFaces = [doc.rootElement elementsForName:@"item"];
        //NSLog(@"_arrayFaces:%@",_arrayFaces);
        for (GDataXMLElement *item in _arrayFaces) {
            //NSLog(@"item:%@",item.stringValue);
            [arrayFaces addObject:item.stringValue];
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            [self GenerateFaceButton];
        });
    });
    }
#pragma mark - 表情符號
-(void) GenerateFaceButton {
    //NSLog(@"arrayFaces:%@",arrayFaces);
    NSLog(@"資料總共有%d個",[arrayFaces count]);
    
    //依據XML的資料產生按鈕
    CGFloat viewWidth = 100.0;
    CGFloat viewHeight = 41.0;
    CGFloat gap = 5;
    CGFloat startX = 6;
    CGFloat startY = 7;
    NSInteger numberPerRow = 3; //每行有幾個
    CGFloat totalHeight = 0;
    
    for (int i = 0 ; i < [arrayFaces count]; i++) {
        NSInteger indexColumn = i % numberPerRow; //該行的第幾個
        NSInteger indexRow =  i / numberPerRow; //第幾行
        //NSLog(@"第%d行 第%d個",indexRow,indexColumn);
        
        NSString *_face = [arrayFaces objectAtIndex:i];
        //NSLog(@"_face:%@",_face);
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:_face forState:UIControlStateNormal];
        button.frame = CGRectMake(startX + (viewWidth+gap) * indexColumn,
                                  startY + (viewHeight+gap) * indexRow,
                                  viewWidth,
                                  viewHeight);
        [button setBackgroundImage:[UIImage imageNamed:@"chat_sending_emotion.png"] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(SelectFace:) forControlEvents:UIControlEventTouchUpInside];
        //NSLog(@"button.frame:%@",NSStringFromCGRect(button.frame));
        
        [self.ScrollViewFace addSubview:button];
        
        //處理到最後一個，記錄總高度
        if (i == [arrayFaces count] - 1) {
            totalHeight = startY + button.frame.origin.y + button.frame.size.height;
        }
        //NSLog(@"totalHeight:%f",totalHeight);
    }
    
    //調整ScrollView
    [self.ScrollViewFace setContentSize:CGSizeMake(self.ScrollViewFace.frame.size.width, totalHeight)];
    
    [self ClickFace];
}
-(void) SelectFace:(UIButton *)sender {
    NSString *face = sender.titleLabel.text;
    [self ViewChatFaceSelectedFace:face];
    
    //關閉
    [self Back];
}
-(IBAction) ClickFace {
    //調整UI
    [self.ButtonFace setBackgroundImage:[UIImage imageNamed:@"chat_emotion_btn2.png"] forState:UIControlStateNormal];
    [self.ButtonImage setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.ButtonCommon setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.ButtonFace setTitleColor:[UIColor colorWithRed:148.0/255.0 green:196.0/255.0 blue:235.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.ButtonImage setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.ButtonCommon setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.ScrollViewFace.hidden = NO;
    self.TableView.hidden = YES;
}
#pragma mark - 貼圖
-(void) GetMQTTImageList { //API-取得MQTT 圖片清單
    [[VariableStore sharedInstance] ShowHUDInView:self.TableView Text:@"連線中"];
    
    //參數歸0
    [arrayTableView removeAllObjects];
    numbersOfData = 0;
    numbersOfCells = 0;
    
    api_img_list_all_mqtt_img *api = [[api_img_list_all_mqtt_img alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetListAllMqttImg];
}
-(void) api_img_list_all_mqtt_imgComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.TableView];
    if (success) { //更新UI
        for (NSString *urlString in data) [arrayTableView addObject:urlString];
        
        //每n個為一排
        numbersOfData = [arrayTableView count];
        numbersOfCells = numbersOfData / objectPerRow;
        if (numbersOfData % objectPerRow > 0) {
            numbersOfCells++;
        }
        NSLog(@"資料總共有%d筆，分為%d行",numbersOfData,numbersOfCells);
        
        [self.TableView reloadData];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
-(IBAction) ClickImage {
    //繼續抓圖片
    [self GetMQTTImageList];
    
    //調整UI
    [self.ButtonFace setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.ButtonImage setBackgroundImage:[UIImage imageNamed:@"chat_emotion_btn2.png"] forState:UIControlStateNormal];
    [self.ButtonCommon setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.ButtonFace setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.ButtonImage setTitleColor:[UIColor colorWithRed:148.0/255.0 green:196.0/255.0 blue:235.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.ButtonCommon setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.ScrollViewFace.hidden = YES;
    self.TableView.hidden = NO;
}
-(void) ClickImageAtChatFace:(NSNotification *)notif{
    for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations) { //避免多重request沒做完造成crash的問題
        [req cancel];
        [req setDelegate:nil];
    }
    
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    //NSLog(@"index:%d",index);
    NSString *urlString = [arrayTableView objectAtIndex:index];
    //NSLog(@"ClickImageAtChatFace:%@",urlString);
    [self ViewChatFaceSelectedImage:urlString];
    
    //把該View移除
    [self Back];
}
#pragma mark -
#pragma mark tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return numbersOfCells;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ViewChatFaceImageCell"];
    
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ViewChatFaceImageCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    ViewChatFaceImageCell *myCell = (ViewChatFaceImageCell *)cell;
    myCell.backgroundColor = [UIColor clearColor];
    
    NSMutableArray *arrayData = [NSMutableArray array];
    NSInteger startIndex,endIndex;
    startIndex = indexPath.row * objectPerRow;
    if (numbersOfData - startIndex >= objectPerRow) { //該行滿n個
        endIndex = startIndex + (objectPerRow - 1);
    }
    else { //該行不滿n個
        endIndex = numbersOfData - 1;
    }
    
    for (int i = startIndex; i <= endIndex; i++) {
        [arrayData addObject:[arrayTableView objectAtIndex:i]];
    }
    NSLog(@"startIndex:%d",startIndex);
    NSLog(@"endIndex:%d",endIndex);
    NSLog(@"資料數量:%d",[arrayData count]);
    
    myCell.startIndex = startIndex;
    myCell.endIndex = endIndex;
    [myCell UpdateUIWithData:arrayData];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
#pragma mark - 常用
-(IBAction) ClickCommon {
    //調整UI
    [self.ButtonFace setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.ButtonImage setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.ButtonCommon setBackgroundImage:[UIImage imageNamed:@"chat_emotion_btn2.png"] forState:UIControlStateNormal];
    [self.ButtonFace setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.ButtonImage setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.ButtonCommon setTitleColor:[UIColor colorWithRed:148.0/255.0 green:196.0/255.0 blue:235.0/255.0 alpha:1.0] forState:UIControlStateNormal];
}
#pragma mark - 換頁
-(void) Back {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ClickImageAtChatFace" object:nil];
    
    [self removeFromSuperview];
}
#pragma mark - 雜項
-(void) dealloc {
    NSLog(@"ViewChatFace dealloc");
}
@end
