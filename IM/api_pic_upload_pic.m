

#import "api_pic_upload_pic.h"
#import "JSON.h"
#import "Base64.h"

@implementation api_pic_upload_pic
@synthesize delegate;
#pragma mark -
#pragma mark Delegate
-(void) api_pic_upload_picComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    if ([[self delegate] respondsToSelector:@selector(api_pic_upload_picComplete:Message:Data:)]) {
        [[self delegate] api_pic_upload_picComplete:success Message:message Data:data];
    }
}
#pragma mark -
#pragma mark ASIHTTPRequest
-(void) UploadPicWithGuid:(NSString *)guid Pic:(UIImage *)pic {
    //NSData* data = UIImageJPEGRepresentation(pic, 1.0f);
    NSData* data = UIImagePNGRepresentation(pic);
    [Base64 initialize];
    NSString *strEncoded = [Base64 encode:data];
    //NSLog(@"strEncoded:%@",strEncoded);
    
    NSString *urlString = [NSString stringWithFormat:@"http://mqttnewyearmem.cloudapp.net:8088/api_pic/upload_pic"];
    NSLog(@"api_pic/upload_pic url:%@",urlString);

    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIRequest = [[ASIFormDataRequest alloc] initWithURL:url];
    [ASIRequest setTimeOutSeconds:kURLTimeoutSecond];
    [ASIRequest setNumberOfTimesToRetryOnTimeout:kURLRetryOnTimeout];
    
    //guid = [guid lowercaseString];
    //NSLog(@"guid:%@",guid);
    //NSLog(@"pic:%@",strEncoded);
    
    [ASIRequest addPostValue:guid forKey:@"guid"];
    [ASIRequest addPostValue:strEncoded forKey:@"pic"];

    [ASIRequest setDelegate:self];
    [ASIRequest startAsynchronous];
}
- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    NSLog(@"api_pic/upload_pic Finished:%@",responseString);
    
    NSError *error = nil;
	SBJSON *parser = [SBJSON new];
	
    //原始資料
	NSDictionary *result = (NSDictionary *) [parser objectWithString:responseString error:&error];
    NSLog(@"api_pic/upload_pic result:%@",result);
    
    while (error) {
        NSLog(@"json error:%@",[error localizedDescription]);
        error = [[error userInfo] objectForKey:NSUnderlyingErrorKey];
        NSLog(@"Error trace: %@", parser.errorTrace);
        [self api_pic_upload_picComplete:NO Message:kMessageNetworkFail Data:nil];
        [self performSelector:@selector(Cancle)];
        return;
    }
    
    NSString *sys_msg = [result objectForKey:@"sys_msg"];
    NSString *sys_code = [result objectForKey:@"sys_code"];
    NSLog(@"api_pic/upload_pic:(%@)%@",sys_code,sys_msg);
    
    if ([sys_code isEqualToString:@"200"]) { //200成功
        NSDictionary *data = result;
        [self api_pic_upload_picComplete:YES Message:nil Data:data];
    }
    else { //失敗
        [self api_pic_upload_picComplete:NO Message:sys_msg Data:nil];
    }
    
    [self performSelector:@selector(Cancle)];
}
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"requestFailed error:%@",error);
    [self api_pic_upload_picComplete:NO Message:kMessageNetworkFail Data:nil];
    [self performSelector:@selector(Cancle)];
}
#pragma mark -
#pragma mark 雜項
-(void) Cancle {
    // Cancels an asynchronous request
    [ASIRequest cancel];
    
    // Cancels an asynchronous request, clearing all delegates and blocks first
    [ASIRequest clearDelegatesAndCancel];
    
    //移除物件
    [[VariableStore sharedInstance].arrayHTTPRequests removeObject:self];
}
- (void)dealloc {
    ASIRequest = nil;
    delegate = nil;
}
@end