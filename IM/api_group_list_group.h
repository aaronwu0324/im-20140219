
@protocol api_group_list_groupDelegate <NSObject>
@optional
-(void) api_group_list_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_group_list_group : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_group_list_groupDelegate> delegate;
}
@property (weak) id delegate;
-(void) GetGroupWithAccount:(NSString *)account;
@end
