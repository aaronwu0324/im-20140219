

#import "ViewChatVoicePlayer.h"

static const NSString *ItemStatusContext;

@implementation ViewChatVoicePlayer
@synthesize delegate;
#pragma mark - Delegate
-(void) ViewChatVoicePlayerStart {
    if ([[self delegate] respondsToSelector:@selector(ViewChatVoicePlayerStart)]) {
        [[self delegate] ViewChatVoicePlayerStart];
    }
}
-(void) ViewChatVoicePlayerFinish {
    if ([[self delegate] respondsToSelector:@selector(ViewChatVoicePlayerFinish)]) {
        [[self delegate] ViewChatVoicePlayerFinish];
    }
}
-(void) PlayVoiceWithUrl:(NSString *)urlString {
    
    NSURL *url = [NSURL URLWithString:urlString];
    _playerItem = [AVPlayerItem playerItemWithURL:url];
    [_playerItem addObserver:self forKeyPath:@"status" options:0 context:&ItemStatusContext];
    _player = [AVPlayer playerWithPlayerItem:_playerItem];
    
    // Register with the notification center after creating the player item.
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(playerItemDidReachEnd:)
     name:AVPlayerItemDidPlayToEndTimeNotification
     object:_playerItem];

    [self AddTimeObserver];

    return;
}
-(void) AddTimeObserver { //增加觀察播放時間
    //__weak typeof(AVPlayer) *weakPlayer = _player;
    
    CMTime interval = CMTimeMake(33, 1000);  // 30fps
    playbackObserver = [_player addPeriodicTimeObserverForInterval:interval queue:dispatch_get_main_queue() usingBlock: ^(CMTime time) {
        CMTime endTime = CMTimeConvertScale(_player.currentItem.asset.duration, _player.currentTime.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
        if (CMTimeCompare(endTime, kCMTimeZero) != 0) {
            //double normalizedTime = (double) _player.currentTime.value / (double) endTime.value;
            //NSLog(@"normalizedTime:%f",normalizedTime);
        }
    }];
}
-(void) RemoveTimeObserver { //移除觀察播放時間
    [_player removeTimeObserver:playbackObserver];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    
    if (context == &ItemStatusContext) {
        AVPlayer *thePlayer = (AVPlayer *)object;
        if ([thePlayer status] == AVPlayerStatusReadyToPlay) { //可以播放了
            [self play:nil];
            return;
        }
        // Deal with other status change if appropriate.
    }
    
    // Deal with other change notifications if appropriate.
    //[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    return;
}
- (void)play:sender {
    [self ViewChatVoicePlayerStart];
    [_player play];
}
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [self ViewChatVoicePlayerFinish];
    
    [_player seekToTime:kCMTimeZero];
    [self RemoveTimeObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:_playerItem];
    _player = nil;
    _playerItem = nil;
    playbackObserver = nil;
}
-(void) dealloc {
    NSLog(@"ViewChatVoicePlayer dealloc");
}
@end
