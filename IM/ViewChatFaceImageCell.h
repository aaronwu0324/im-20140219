

#import <UIKit/UIKit.h>

@interface ViewChatFaceImageCell : UITableViewCell
@property NSInteger startIndex;
@property NSInteger endIndex;
-(void) UpdateUIWithData:(NSArray *)data;
@end
