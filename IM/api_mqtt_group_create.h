
@protocol api_mqtt_group_createDelegate <NSObject>
@optional
-(void) api_mqtt_group_createComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_mqtt_group_create : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_mqtt_group_createDelegate> delegate;
}
@property (weak) id delegate;
-(void) CreateMQTTChannelWithGroupID:(NSString *)gid;
@end
