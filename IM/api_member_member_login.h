
@protocol api_member_member_loginDelegate <NSObject>
@optional
-(void) api_member_member_loginComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_member_member_login : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_member_member_loginDelegate> delegate;
}
@property (weak) id delegate;
-(void) LoginWithAccount:(NSString *)account Password:(NSString *)password Dxid:(NSString *)dxid;
@end
