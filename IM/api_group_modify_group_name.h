
@protocol api_group_modify_group_nameDelegate <NSObject>
@optional
-(void) api_group_modify_group_nameComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_group_modify_group_name : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_group_modify_group_nameDelegate> delegate;
}
@property (weak) id delegate;
-(void) ModifyGroupNameWithAccout:(NSString *)account GroupName:(NSString *)group_name NewGroupName:(NSString *)new_group_name;
@end
