
@protocol api_group_delete_groupDelegate <NSObject>
@optional
-(void) api_group_delete_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface api_group_delete_group : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_group_delete_groupDelegate> delegate;
}
@property (weak) id delegate;
-(void) DeleteGroupWithAccount:(NSString *)account GroupName:(NSString *)group_name;
@end
