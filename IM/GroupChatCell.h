

#import <UIKit/UIKit.h>
#import "HTTPImageView.h"

@interface GroupChatCell : UITableViewCell

@property (nonatomic, assign) IBOutlet UILabel *LabelTitle;
@property (nonatomic, assign) IBOutlet UILabel *LabelContent;
@property (nonatomic, assign) IBOutlet HTTPImageView *ImageTitle;
@property (nonatomic, assign) IBOutlet UILabel *LabelUnreadCount;
@property (nonatomic, assign) IBOutlet UIImageView *ImageUnread;
@property NSInteger cellIndex;
@end
