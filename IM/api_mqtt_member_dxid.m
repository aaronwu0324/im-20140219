

#import "api_mqtt_member_dxid.h"
#import "JSON.h"

@implementation api_mqtt_member_dxid
@synthesize delegate;
#pragma mark -
#pragma mark Delegate
-(void) api_mqtt_member_dxidComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    if ([[self delegate] respondsToSelector:@selector(api_mqtt_member_dxidComplete:Message:Data:)]) {
        [[self delegate] api_mqtt_member_dxidComplete:success Message:message Data:data];
    }
}
#pragma mark -
#pragma mark ASIHTTPRequest
-(void) UpdateDxidWithAccount:(NSString *)account Dxid:(NSString *)dxid {
    NSString *urlString = [NSString stringWithFormat:@"%@/mqtt/member_dxid?account=%@&dxid=%@",kSYSGatewayTemp,account,dxid];
    NSLog(@"api_mqtt/member_dxid url:%@",urlString);
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIRequest = [[ASIHTTPRequest alloc] initWithURL:url];
    [ASIRequest setTimeOutSeconds:kURLTimeoutSecond];
    [ASIRequest setNumberOfTimesToRetryOnTimeout:2];
    [ASIRequest setDelegate:self];
    [ASIRequest startAsynchronous];
}
- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    //NSLog(@"api_mqtt/member_dxid Finished:%@",responseString);
    
    NSError *error = nil;
	SBJSON *parser = [SBJSON new];
	
    //原始資料
	NSDictionary *result = (NSDictionary *) [parser objectWithString:responseString error:&error];
    NSLog(@"api_mqtt/member_dxid result:%@",result);
    
    while (error) {
        NSLog(@"json error:%@",[error localizedDescription]);
        error = [[error userInfo] objectForKey:NSUnderlyingErrorKey];
        NSLog(@"Error trace: %@", parser.errorTrace);
        [self api_mqtt_member_dxidComplete:NO Message:kMessageNetworkFail Data:nil];
        [self performSelector:@selector(Cancle)];
        return;
    }
    
    NSString *sys_msg = [result objectForKey:@"sys_msg"];
    NSString *sys_code = [result objectForKey:@"sys_code"];
    NSLog(@"api_mqtt/member_dxid:(%@)%@",sys_code,sys_msg);
    
    if ([sys_code isEqualToString:@"200"]) { //200成功
        NSDictionary *data = result;
        [self api_mqtt_member_dxidComplete:YES Message:sys_msg Data:data];
    }
    else { //失敗
        [self api_mqtt_member_dxidComplete:NO Message:sys_msg Data:nil];
    }
    
    [self performSelector:@selector(Cancle)];
}
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"requestFailed error:%@",error);
    [self api_mqtt_member_dxidComplete:NO Message:kMessageNetworkFail Data:nil];
    [self performSelector:@selector(Cancle)];
}
#pragma mark -
#pragma mark 雜項
-(void) Cancle {
    // Cancels an asynchronous request
    [ASIRequest cancel];
    
    // Cancels an asynchronous request, clearing all delegates and blocks first
    [ASIRequest clearDelegatesAndCancel];
    
    //移除物件
    [[VariableStore sharedInstance].arrayHTTPRequests removeObject:self];
}
- (void)dealloc {
    ASIRequest = nil;
    delegate = nil;
}
@end