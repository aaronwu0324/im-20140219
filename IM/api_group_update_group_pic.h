
@protocol api_group_update_group_picDelegate <NSObject>
@optional
-(void) api_group_update_group_picComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface api_group_update_group_pic : NSObject <ASIHTTPRequestDelegate> {
    ASIFormDataRequest *ASIRequest;
    __weak id <api_group_update_group_picDelegate> delegate;
}
@property (weak) id delegate;
-(void) UploadGroupPicWithGid:(NSString *)gid Pic:(UIImage *)pic;
@end
