

#import "api_img_list_all_mqtt_img.h"
#import "JSON.h"

@implementation api_img_list_all_mqtt_img
@synthesize delegate;
#pragma mark -
#pragma mark Delegate
-(void) api_img_list_all_mqtt_imgComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    if ([[self delegate] respondsToSelector:@selector(api_img_list_all_mqtt_imgComplete:Message:Data:)]) {
        [[self delegate] api_img_list_all_mqtt_imgComplete:success Message:message Data:data];
    }
}
#pragma mark -
#pragma mark ASIHTTPRequest
-(void) GetListAllMqttImg {
    NSString *urlString = [NSString stringWithFormat:@"http://202.74.126.64/index.php/api_img/list_all_mqtt_img"];
    NSLog(@"api_img_list_all_mqtt_imgComplete url:%@",urlString);
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIRequest = [[ASIHTTPRequest alloc] initWithURL:url];
    [ASIRequest setTimeOutSeconds:kURLTimeoutSecond];
    [ASIRequest setNumberOfTimesToRetryOnTimeout:kURLRetryOnTimeout];
    [ASIRequest setDelegate:self];
    [ASIRequest startAsynchronous];
}
- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    //NSLog(@"api_img_list_all_mqtt_imgComplete Finished:%@",responseString);
    
    NSError *error = nil;
	SBJSON *parser = [SBJSON new];
	
    //原始資料
	NSArray *result = (NSArray *) [parser objectWithString:responseString error:&error];
    //NSLog(@"api_img_list_all_mqtt_imgComplete result:%@",result);
    
    while (error) {
        NSLog(@"json error:%@",[error localizedDescription]);
        error = [[error userInfo] objectForKey:NSUnderlyingErrorKey];
        NSLog(@"Error trace: %@", parser.errorTrace);
        [self api_img_list_all_mqtt_imgComplete:NO Message:kMessageNetworkFail Data:nil];
        [self performSelector:@selector(Cancle)];
        return;
    }
    
    if ([result count] > 0) {
        [self api_img_list_all_mqtt_imgComplete:YES Message:nil Data:result];
    }
    else { //失敗
        [self api_img_list_all_mqtt_imgComplete:NO Message:@"發生不明原因錯誤" Data:nil];
    }
    
    [self Cancle];
}
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"requestFailed error:%@",error);
    [self api_img_list_all_mqtt_imgComplete:NO Message:kMessageNetworkFail Data:nil];
    [self performSelector:@selector(Cancle)];
}
#pragma mark -
#pragma mark 雜項
-(void) Cancle {
    // Cancels an asynchronous request
    [ASIRequest cancel];
    
    // Cancels an asynchronous request, clearing all delegates and blocks first
    [ASIRequest clearDelegatesAndCancel];
    
    //移除物件
    [[VariableStore sharedInstance].arrayHTTPRequests removeObject:self];
}
- (void)dealloc {
    ASIRequest = nil;
    delegate = nil;
}
@end