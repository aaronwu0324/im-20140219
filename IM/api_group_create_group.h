
@protocol api_group_create_groupDelegate <NSObject>
@optional
-(void) api_group_create_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface api_group_create_group : NSObject <ASIHTTPRequestDelegate> {
    ASIFormDataRequest *ASIRequest;
    __weak id <api_group_create_groupDelegate> delegate;
}
@property (weak) id delegate;
-(void) CreateGroupWithAccount:(NSString *)account Group_Name:(NSString *)group_name Members:(NSString *)members;
@end
