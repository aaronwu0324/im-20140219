
@protocol api_group_list_in_groupDelegate <NSObject>
@optional
-(void) api_group_list_in_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_group_list_in_group : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_group_list_in_groupDelegate> delegate;
}
@property (weak) id delegate;
-(void) GetInGroupWithAccount:(NSString *)account;
@end
