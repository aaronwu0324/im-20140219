

#import <UIKit/UIKit.h>
#import "rootViewController.h"
#import "api_member_search_member.h"
#import "api_group_find_group_by_id.h"
#import "api_group_delete_group.h"
#import "api_group_modify_group_member.h"
#import "api_group_modify_group_name.h"
#import "api_group_update_group_pic.h"
#import "MyImagePicker.h"

@interface GroupEdit : rootViewController <api_member_search_memberDelegate,UIActionSheetDelegate,api_group_find_group_by_idDelegate,api_group_delete_groupDelegate,UIAlertViewDelegate,api_group_modify_group_memberDelegate,api_group_modify_group_nameDelegate,api_group_update_group_picDelegate, MyImagePickerDelegate>
@property (nonatomic,strong) NSString *gid; //群組ID
@end
