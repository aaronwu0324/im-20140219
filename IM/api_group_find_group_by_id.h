
@protocol api_group_find_group_by_idDelegate <NSObject>
@optional
-(void) api_group_find_group_by_idComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface api_group_find_group_by_id : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_group_find_group_by_idDelegate> delegate;
}
@property (weak) id delegate;
-(void) GetGroupWithID:(NSString *)gid;
@end
