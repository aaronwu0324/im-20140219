
@protocol api_contact_delete_contact_by_idDelegate <NSObject>
@optional
-(void) api_contact_delete_contact_by_idComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_contact_delete_contact_by_id : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_contact_delete_contact_by_idDelegate> delegate;
}
@property (weak) id delegate;
-(void) DeleteContactByID:(NSString *)ID;
@end
