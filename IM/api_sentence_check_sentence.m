

#import "api_sentence_check_sentence.h"
#import "JSON.h"

@implementation api_sentence_check_sentence
@synthesize delegate;
#pragma mark -
#pragma mark Delegate
-(void) api_sentence_check_sentenceComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    if ([[self delegate] respondsToSelector:@selector(api_sentence_check_sentenceComplete:Message:Data:)]) {
        [[self delegate] api_sentence_check_sentenceComplete:success Message:message Data:data];
    }
}
#pragma mark -
#pragma mark ASIHTTPRequest
-(void) CheckSentenceWithAccount:(NSString *)account {
    NSString *urlString = [NSString stringWithFormat:@"%@/api_sentence/check_sentence?account=%@",kSYSGatewayTemp,account];
    NSLog(@"api_sentence/check_sentence url:%@",urlString);
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIRequest = [[ASIHTTPRequest alloc] initWithURL:url];
    [ASIRequest setTimeOutSeconds:kURLTimeoutSecond];
    [ASIRequest setNumberOfTimesToRetryOnTimeout:kURLRetryOnTimeout];
    [ASIRequest setDelegate:self];
    [ASIRequest startAsynchronous];
}
- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    //NSLog(@"api_sentence/check_sentence Finished:%@",responseString);
    
    NSError *error = nil;
	SBJSON *parser = [SBJSON new];
	
    //原始資料
	NSDictionary *result = (NSDictionary *) [parser objectWithString:responseString error:&error];
    NSLog(@"api_sentence/check_sentence result:%@",result);
    
    while (error) {
        NSLog(@"json error:%@",[error localizedDescription]);
        error = [[error userInfo] objectForKey:NSUnderlyingErrorKey];
        NSLog(@"Error trace: %@", parser.errorTrace);
        [self api_sentence_check_sentenceComplete:NO Message:kMessageNetworkFail Data:nil];
        [self performSelector:@selector(Cancle)];
        return;
    }
    
    NSString *sys_msg = [result objectForKey:@"sys_msg"];
    NSString *sys_code = [result objectForKey:@"sys_code"];
    NSLog(@"api_sentence/check_sentenceComplete:(%@)%@",sys_code,sys_msg);
    
    if ([sys_code isEqualToString:@"200"]) { //200成功
        NSArray *data = [result objectForKey:@"data"];
        [self api_sentence_check_sentenceComplete:YES Message:nil Data:data];
    }
    else { //失敗
        [self api_sentence_check_sentenceComplete:NO Message:sys_msg Data:nil];
    }
    
    [self Cancle];
}
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"requestFailed error:%@",error);
    [self api_sentence_check_sentenceComplete:NO Message:kMessageNetworkFail Data:nil];
    [self performSelector:@selector(Cancle)];
}
#pragma mark -
#pragma mark 雜項
-(void) Cancle {
    // Cancels an asynchronous request
    [ASIRequest cancel];
    
    // Cancels an asynchronous request, clearing all delegates and blocks first
    [ASIRequest clearDelegatesAndCancel];
    
    //移除物件
    [[VariableStore sharedInstance].arrayHTTPRequests removeObject:self];
}
- (void)dealloc {
    ASIRequest = nil;
    delegate = nil;
}
@end