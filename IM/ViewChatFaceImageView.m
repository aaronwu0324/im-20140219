

#import "ViewChatFaceImageView.h"

@implementation ViewChatFaceImageView
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandler:)];
        tapGesture.numberOfTapsRequired = 1;
        tapGesture.numberOfTouchesRequired = 1;
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}
-(void) UpdateUIWithUrl:(NSString *)urlString {
    NSLog(@"urlString:%@",urlString);
    
    //圖檔
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    [self.ImageTitle setImageWithURL:urlString placeholderImage:nil];
}
#pragma mark - 按鈕相關事件
- (void)tapGestureHandler:(UIGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        NSNumber *number = [NSNumber numberWithInt:self.tag];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClickImageAtChatFace" object:number];
    }
}
#pragma mark - 雜項
-(void) dealloc {
    NSLog(@"ViewChatFaceImageView dealloc");
}
@end
