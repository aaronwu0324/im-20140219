
@protocol api_sentence_delete_sentence_by_idDelegate <NSObject>
@optional
-(void) api_sentence_delete_sentence_by_idComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_sentence_delete_sentence_by_id : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_sentence_delete_sentence_by_idDelegate> delegate;
}
@property (weak) id delegate;
-(void) DeleteSentenceByID:(NSString *)ID;
@end
