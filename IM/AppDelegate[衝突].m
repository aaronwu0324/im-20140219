

#import "AppDelegate.h"
#import "VariableStore.h"
#import "User.h"
#import "MQTTManager.h"
//#import "MessageManager.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [[VariableStore sharedInstance] initVariables];
    [[VariableStore sharedInstance] ReadSetting];
    
    //初始化 User
    [User sharedUser];
    
    [self initAppearance];
    
    // Override point for customization after application launch.
    return YES;
}
- (void)initAppearance {
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navigation_bar.png"] forBarMetrics:UIBarMetricsDefault];
    
    CGFloat version =[[UIDevice currentDevice].systemVersion floatValue];
    if (version >= 7.0) {
        [UINavigationBar appearance].tintColor = [UIColor colorWithRed:42.0/255.0 green:169.0/255.0 blue:56.0/255.0 alpha:1.0];
        return;
    }
    else {
        // Setbackgroundimage for all backbuttons
        /*UIImage * backImg = [UIImage imageNamed:@"navigation_back.png"];
        UIImage * scaleBackImg = [UIImage imageWithCGImage:backImg.CGImage scale:2.0f orientation:UIImageOrientationUp];
        
        UIImage *backButtonPortait= [scaleBackImg
                                     resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonPortait
                                                          forState:UIControlStateNormal
                                                        barMetrics:UIBarMetricsDefault];*/
    }
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    //停止 MQTT
    [[MQTTManager sharedInstance] stop];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kDEFAULTS_KEY_IS_LOGIN]) {
        [[MQTTManager sharedInstance] start];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
