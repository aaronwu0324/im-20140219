
@protocol api_pic_upload_picDelegate <NSObject>
@optional
-(void) api_pic_upload_picComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface api_pic_upload_pic : NSObject <ASIHTTPRequestDelegate> {
    ASIFormDataRequest *ASIRequest;
    __weak id <api_pic_upload_picDelegate> delegate;
}
@property (weak) id delegate;
-(void) UploadPicWithGuid:(NSString *)guid Pic:(UIImage *)pic;
@end
