@protocol SetupAvatarDelegate <NSObject>
@optional
-(void) SetupAvatarScaleAndMoveView_Cancel;
-(void) SetupAvatarScaleAndMoveView_Confirm:(UIImage *)capturedImage;
@end

#import <UIKit/UIKit.h>
#import "SPUserResizableView.h"
#import <QuartzCore/QuartzCore.h>

@interface SetupAvatar : UIViewController <SPUserResizableViewDelegate> {
    __weak id <SetupAvatarDelegate> delegate;
}
@property (weak) id delegate;
@property (nonatomic,strong) IBOutlet UIImageView *Image;
@property (nonatomic,strong) UIImage *selectedImage;
-(IBAction) Confirm;
-(IBAction) Cancel;
@end
