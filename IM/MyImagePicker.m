
#import "MyImagePicker.h"

@implementation MyImagePicker
@synthesize delegate;
#pragma mark - Delegate
-(void) MyImagePickerFinishedWithImage:(UIImage *)image {
    if ([[self delegate] respondsToSelector:@selector(MyImagePickerFinishedWithImage:)]) {
        [[self delegate] MyImagePickerFinishedWithImage:image];
    }
}
-(void) UpdateUI {
    [self initImagePicker];
    [self ChooseImageSource];
}
#pragma mark - 來源選擇
-(void) ChooseImageSource {
    UIActionSheet *menu = [[UIActionSheet alloc]
                           initWithTitle:@"請選擇上傳方式"
                           delegate:self
                           cancelButtonTitle:nil
                           destructiveButtonTitle:nil
                           otherButtonTitles:nil];
    [menu addButtonWithTitle:@"拍攝照片"];
    [menu addButtonWithTitle:@"選擇照片"];
    
    //取消按鈕
    [menu addButtonWithTitle:@"取消"];
    menu.destructiveButtonIndex = menu.numberOfButtons - 1;

    
    [menu showFromTabBar:_SourceVC.tabBarController.tabBar];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *selectedString = [actionSheet buttonTitleAtIndex:buttonIndex];
    //NSLog(@"selectedString:%@",selectedString);
    if ([selectedString isEqualToString:@"取消"]) {
        return;
    }
    
    if ([selectedString isEqualToString:@"拍攝照片"]) {
        [self OpenCamera:nil];
        return;
    }
    
    if ([selectedString isEqualToString:@"選擇照片"]) {
        [self OpenAlbum:nil];
        return;
    }
}
#pragma mark - 相機
-(void) initImagePicker {
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    //imagePicker.allowsEditing = YES;
}
-(void)OpenCamera:(id)sender {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { //沒相機
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"無法開啟相機！" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [_SourceVC presentViewController:imagePicker animated:YES completion:nil];
}
-(void)OpenAlbum:(id)sender {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) { //沒相簿
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"無法取得照片！" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [_SourceVC presentViewController:imagePicker animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //NSString *url = [info objectForKey:UIImagePickerControllerReferenceURL];
    //NSLog(@"url:%@",url);
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]){
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        NSLog(@"original image:%@",NSStringFromCGSize(image.size));
        
        //UIImage *imageModified = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        //NSLog(@"modified an image:%@",NSStringFromCGSize(imageModified.size));
        
        //UIImage *imageResize = [imageModified imageByScalingAndCroppingForSize:CGSizeMake(320, 320)];
        //NSLog(@"resize an image:%@",NSStringFromCGSize(imageResize.size));
        
        [self MyImagePickerFinishedWithImage:image];
    }
    else if ([mediaType isEqualToString:@"public.movie"]){
        NSLog(@"found a video");
        //NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        //NSData *webData = [NSData dataWithContentsOfURL:videoURL];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - 客製化
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [viewController.navigationItem setTitle:@""]; //隱藏標題
}
#pragma mark - 雜項
-(void) dealloc {
    delegate = nil;
}
@end
