
@protocol api_member_update_avatarDelegate <NSObject>
@optional
-(void) api_member_update_avatarComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface api_member_update_avatar : NSObject <ASIHTTPRequestDelegate> {
    ASIFormDataRequest *ASIRequest;
    __weak id <api_member_update_avatarDelegate> delegate;
}
@property (weak) id delegate;
-(void) UpdateAvatarWithAccount:(NSString *)account Avatar:(UIImage *)avatar;
@end
