#import "SetupSentence.h"
#import "SetupSentenceCell.h"
#import "User.h"

@interface SetupSentence () {
    NSInteger selectedIndex;
    NSMutableArray *arrayTableView;
}
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@property (nonatomic,weak) IBOutlet UITextField *TextSentence;
@end

@implementation SetupSentence
#pragma mark - 初始化相關
-(void) initParameter { //初始化參數
    arrayTableView = [[NSMutableArray alloc] init];
}
-(void) initUIKit { //初始化畫面
    [self GetSentence];
}
#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self AddSentence];
    
    [textField resignFirstResponder];
    
    return YES;
}
#pragma mark - tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	//製作可重複利用的表格欄位Cell
    static NSString *CellIdentifier = @"SetupSentenceCell";
    
    SetupSentenceCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[SetupSentenceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    NSLog(@"_dic:%@",_dic);
    
    cell.tag = indexPath.row;
    cell.delegate = self;
    
    cell.LabelTitle.text = [_dic objectForKey:@"sentence_content"];

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex = indexPath.row;
}
#pragma mark - 刪除
-(void) SetupSentenceCellDeleteAtIndex:(NSInteger)index { //刪除
    selectedIndex = index;
    
    //顯示確認
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"確認要刪除嗎？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確認", nil];
    [alert show];
}
-(void) ConfirmDelete {
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSLog(@"_dic:%@",_dic);
    NSString *sentence_id = [_dic objectForKey:@"sentence_id"];
    
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"刪除中"];
    
    api_sentence_delete_sentence_by_id *api = [[api_sentence_delete_sentence_by_id alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api DeleteSentenceByID:sentence_id];
}
-(void) api_sentence_delete_sentence_by_idComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        //建立成功就refresh清單
        [self GetSentence];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 搜尋
-(BOOL) CanGoNext {
    BOOL CanGoNext = NO;
    
    NSString *alertMessage = @"";
    if  ([self.TextSentence.text length] == 0) {
        alertMessage = @"請輸入詞彙";
    }
    else {
        CanGoNext = YES;
    }
    
    if (!CanGoNext) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertMessage message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    return CanGoNext;
}
-(IBAction)AddSentence {
    if (![self CanGoNext]) return;
    
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"新增詞彙中"];
    
    //取得自己的account
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_sentence_create_sentence *api = [[api_sentence_create_sentence alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api CreateSentenceWithAccount:account Sentence:self.TextSentence.text];
}
-(void) api_sentence_create_sentenceComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        //參數歸0
        selectedIndex = 0;
        [arrayTableView removeAllObjects];
        
        //Text復歸
        self.TextSentence.text = @"";
        [self.TextSentence resignFirstResponder];
        
        //重抓資料
        [self GetSentence];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 取得聯絡人清單
-(void)GetSentence {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    //參數復歸
    selectedIndex = 0;
    [arrayTableView removeAllObjects];
    
    //取得自己的account
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_sentence_check_sentence *api = [[api_sentence_check_sentence alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api CheckSentenceWithAccount:account];
}
-(void) api_sentence_check_sentenceComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        for (NSDictionary *_dic in data) [arrayTableView addObject:_dic];
        
        //更新TableView
        [self.TableView reloadData];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 雜項
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"確認"]) {
        [self ConfirmDelete];
    }
}
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
}
@end
