#import "NameList.h"
#import "NameListCell.h"
#import "MQTTChatRoom.h"
#import "GroupEdit.h"
#import "MyBorderTextField.h"

#define ActionSheetTag_LongPress 1  //常按
#define ActionSheetTag_AddToGroup 2 //加入群組
#define ActionSheetTag_Search 3 //搜尋

@interface NameList () {
    NSInteger selectedIndex;
    NSInteger selectedIndex_SearchResult;
    NSInteger selectedIndex_AddToGroup;
    NSMutableArray *arrayTableView;
    NSMutableArray *arraySearchResult;
    NSMutableArray *arrayAddToGroup;
    NSString *groupNameWhenCreate;  //建立群組時的暫存變數
    NSString *groupIDWhenCreate;  //建立群組時的暫存變數
    BOOL isGettingContactList;
}
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@property (nonatomic,weak) IBOutlet MyBorderTextField *TextSearchName;
@end

@implementation NameList
#pragma mark - 初始化相關
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self GetContact];
}
-(void) initParameter { //初始化參數
    arrayTableView = [[NSMutableArray alloc] init];
    arraySearchResult = [[NSMutableArray alloc] init];
    arrayAddToGroup = [[NSMutableArray alloc] init];
}
-(void) initUIKit { //初始化畫面
    [self TableViewAddLongPress]; //TableView加上長按功能選單
}
#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self Search];
    
    [textField resignFirstResponder];
    
    return YES;
}
#pragma mark - tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	//製作可重複利用的表格欄位Cell
    static NSString *CellIdentifier = @"NameListCell";
    
    NameListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[NameListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    //NSLog(@"_dic:%@",_dic);
    
    //姓名
    cell.LabelName.text = [_dic objectForKey:@"chinese_name"];
    
    //姓名
    cell.LabelEnglishName.text = [_dic objectForKey:@"english_name"];

    //位置
    cell.LabelLocation.text = [_dic objectForKey:@"location"];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex = indexPath.row;
    
    //跟該人聊天
    [self performSegueWithIdentifier:@"GotoMQTTChatRoom" sender:nil];
}
#pragma mark - 長按TableVeiw cell聯絡人
-(void) TableViewAddLongPress {
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    longPressGesture.minimumPressDuration = 1.0; //seconds
    longPressGesture.delegate = self;
    [self.TableView addGestureRecognizer:longPressGesture];
}
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    NSLog(@"state:%d",gestureRecognizer.state);
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint p = [gestureRecognizer locationInView:self.TableView];
        
        NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:p];
        if (indexPath == nil) {
            NSLog(@"long press on table view but not on a row");
        }
        else {
            NSLog(@"long press on table view at row %d", indexPath.row);
            selectedIndex = indexPath.row;
            
            [self ShowLongPressMenu]; //長按選單
        }
    }
}
-(void) ShowLongPressMenu {
    UIActionSheet *menu = [[UIActionSheet alloc]
                           initWithTitle:@"請選擇"
                           delegate:self
                           cancelButtonTitle:nil
                           destructiveButtonTitle:nil
                           otherButtonTitles:nil];
    
    [menu addButtonWithTitle:@"聊天"];
    [menu addButtonWithTitle:@"加入群組"];
    [menu addButtonWithTitle:@"刪除"];
    
    //取消按鈕
    [menu addButtonWithTitle:@"取消"];
    menu.destructiveButtonIndex = menu.numberOfButtons - 1;
    
    menu.tag = ActionSheetTag_LongPress;
    
    //顯示
    [menu showInView:self.view];
}
#pragma mark - 搜尋
-(BOOL) CanGoNext {
    BOOL CanGoNext = NO;
    
    NSString *alertMessage = @"";
    if  ([self.TextSearchName.text length] == 0) {
        alertMessage = @"請輸入搜尋關鍵字";
    }
    else {
        CanGoNext = YES;
    }
    
    if (!CanGoNext) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertMessage message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    return CanGoNext;
}
-(IBAction)Search {
    if (![self CanGoNext]) return;
    
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    api_member_search_member *api = [[api_member_search_member alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api SearchMemberWithName:self.TextSearchName.text];
}
-(void) api_member_search_memberComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        //參數歸0
        selectedIndex_SearchResult = 0;
        [arraySearchResult removeAllObjects];
        
        //加入資料
        for (NSDictionary *_dic  in data) [arraySearchResult addObject:_dic];
        
        //用actionsheet顯示結果
        [self ShowActionSheet];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
-(void) ShowActionSheet {
    UIActionSheet *menu = [[UIActionSheet alloc]
                            initWithTitle:@"新增聯絡人"
                            delegate:self
                            cancelButtonTitle:nil
                            destructiveButtonTitle:nil
                            otherButtonTitles:nil];
    
    //加入title
    for (NSDictionary *_dic  in arraySearchResult) {
        NSString *CN = [_dic objectForKey:@"chinese_name"];
        [menu addButtonWithTitle:CN];
    }
    
    //取消按鈕
    [menu addButtonWithTitle:@"取消"];
    menu.destructiveButtonIndex = menu.numberOfButtons - 1;
    
    menu.tag = ActionSheetTag_Search;
    
    //顯示
    [menu showInView:self.view];
}
#pragma mark - ActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"clickedButtonAtIndex:%d",buttonIndex);
    if (buttonIndex < 0) { //iPad按外面的話index是-1
        return;
    }
    
    NSString *selectedString = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    //選到取消
    if ([selectedString isEqualToString:@"取消"]) return;
    
    if (actionSheet.tag == ActionSheetTag_LongPress) { //長按
        if ([selectedString isEqualToString:@"聊天"]) {
            [self Chat];
        }
        else if ([selectedString isEqualToString:@"加入群組"]) {
            [self GetGroupList]; //先取得自己的群組
        }
        else if ([selectedString isEqualToString:@"刪除"]) { //刪除選擇的聯絡人
            NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
            [self DeleteContactWithID:[_dic objectForKey:@"id"]];
        }
    }
    else if (actionSheet.tag == ActionSheetTag_Search) { //搜尋聯絡人的sheet
        //Text復歸
        self.TextSearchName.text = @"";
        [self.TextSearchName resignFirstResponder];
        
        //選擇結果
        selectedIndex_SearchResult = buttonIndex;
        //NSLog(@"selectedIndex_SearchResult:%d",selectedIndex_SearchResult);
        NSDictionary *_dic = [arraySearchResult objectAtIndex:selectedIndex_SearchResult];
        //NSLog(@"_dic:%@",_dic);
        [self CreateContactWithContactName:[_dic objectForKey:@"account"]];
    }
    else if (actionSheet.tag == ActionSheetTag_AddToGroup) { //加入群組
        if ([selectedString isEqualToString:@"新增群組"]) { //建立新群組
            [self PromptNewGroupName];
        }
        else { //把選到的人加入群組
            //朋友account
            NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
            NSString *friendAccount = [_dic objectForKey:@"account"];
            
            selectedIndex_AddToGroup = buttonIndex;
            NSDictionary *_dicGroup = [arrayAddToGroup objectAtIndex:selectedIndex_AddToGroup];
            
            groupIDWhenCreate = [NSString stringWithFormat:@"%@",[_dicGroup objectForKey:@"gid"]];
            
            NSString *group_name = [_dicGroup objectForKey:@"group_name"];
            [self AddMemberWithID:friendAccount GroupName:group_name];
        }
        
    }
}
#pragma mark - 建立聯絡人
-(void)CreateContactWithContactName:(NSString *)contact {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"加入清單中"];
    
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_contact_create_contact *api = [[api_contact_create_contact alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api CreateContactWithAccount:account Contact:contact];
}
-(void) api_contact_create_contactComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        //建立成功就refresh聯絡人清單
        [self GetContact];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 刪除聯絡人
-(void)DeleteContactWithID:(NSString *)ID {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"刪除聯絡人中"];

    api_contact_delete_contact_by_id *api = [[api_contact_delete_contact_by_id alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api DeleteContactByID:ID];
}
-(void) api_contact_delete_contact_by_idComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        //建立成功就refresh聯絡人清單
        [self GetContact];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 取得聯絡人清單
-(void)GetContact {
    if (isGettingContactList) return;
    
    isGettingContactList = YES;
    
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    //參數復歸
    selectedIndex = 0;
    [arrayTableView removeAllObjects];
    
    //取得自己的account
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_contact_check_contact *api = [[api_contact_check_contact alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api CheckContactWithAccount:account];
}
-(void) api_contact_check_contactComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    isGettingContactList = NO;
    
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        for (NSDictionary *_dic in data) [arrayTableView addObject:_dic];
        
        //更新TableView
        [self.TableView reloadData];
        
        if ([[VariableStore sharedInstance].notifyMemberID length] > 0) { //經由通知到此畫面，，找出該朋友的相關資料，然後直接進入聊天畫面
            for (int i = 0; i < [arrayTableView count]; i++) {
                NSDictionary *_dic = [arrayTableView objectAtIndex:i];
                if ([[_dic objectForKey:kDEFAULTS_KEY_MEMBER_ACCOUNT] isEqualToString:[VariableStore sharedInstance].notifyMemberID]) {
                    selectedIndex = i;
                    break;
                }
            }
            
            [self performSegueWithIdentifier:@"GotoMQTTChatRoom" sender:nil];
            
            //參數歸0
            [VariableStore sharedInstance].notifyMemberID = @"";
        }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 取得自己帳號的群組清單
-(void) GetGroupList { //取得自己帳號的群組清單
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    //參數歸零
    selectedIndex_AddToGroup = 0;
    [arrayAddToGroup removeAllObjects];
    
    //取得自己的account
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_group_list_my_group *api = [[api_group_list_my_group alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetMyGroupWithAccount:account];
}
-(void) api_group_list_my_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) { //更新UI
        UIActionSheet *menu = [[UIActionSheet alloc]
                               initWithTitle:@"加入"
                               delegate:self
                               cancelButtonTitle:nil
                               destructiveButtonTitle:nil
                               otherButtonTitles:nil];
        
        //加入title
        for (NSDictionary *_dic  in data) {
            NSString *group_name = [_dic objectForKey:@"group_name"];
            [menu addButtonWithTitle:group_name];
            
            [arrayAddToGroup addObject:_dic];
        }
        
        //新增群組
        [menu addButtonWithTitle:@"新增群組"];
        
        //取消按鈕
        [menu addButtonWithTitle:@"取消"];
        menu.destructiveButtonIndex = menu.numberOfButtons - 1;
        
        menu.tag = ActionSheetTag_AddToGroup;
        
        //顯示
        [menu showInView:self.view];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 新增群組
-(void) PromptNewGroupName {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"請輸入群組名稱" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *selectedString = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([selectedString isEqualToString:@"確定"]) {
        UITextField *textField = [alertView textFieldAtIndex:0];
        NSString *groupName = textField.text;
        [self CreateGroupWithName:groupName];
        NSLog(@"要新增的群組名稱：%@",groupName);
    }
}
-(void) CreateGroupWithName:(NSString *)group_name { //建立群組
    if ([group_name length] == 0) return; //防呆
    
    groupNameWhenCreate = [[NSString alloc] initWithString:group_name];
    
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    //取得自己的account
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_group_create_group *api = [[api_group_create_group alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api CreateGroupWithAccount:account Group_Name:group_name Members:account];
}
-(void) api_group_create_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) { //更新UI
        //把建立的群組id存起來
        groupIDWhenCreate = [NSString stringWithFormat:@"%@",[data objectForKey:@"gid"]];
        
        //把選到的人跟自己加入該群組
        NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
        NSString *friendAccount = [_dic objectForKey:@"account"];
        
        NSString *account = [[User sharedUser] ACCOUNT];
        
        NSString *member = [NSString stringWithFormat:@"%@|%@",account,friendAccount];
        
        [self ModifyGroupWithMembers:member GroupName:groupNameWhenCreate];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 增加/刪除群組成員
-(void) AddMemberWithID:(NSString *)memberID GroupName:(NSString *)group_name {
    //取出選到的群組的成員資料
    NSDictionary *_dic = [arrayAddToGroup objectAtIndex:selectedIndex_AddToGroup];
    NSArray *members_arr = [_dic objectForKey:@"members_arr"];
    
    //把現有的成員組成字串
    NSString *members = @"";
    for (NSDictionary *_dic in members_arr) {
        members = [members stringByAppendingFormat:@"%@|",[_dic objectForKey:@"account"]];
    }
    members = [members stringByAppendingString:memberID];
    NSLog(@"要更新的成員字串：%@",members);
    [self ModifyGroupWithMembers:members GroupName:group_name];
}
-(void) ModifyGroupWithMembers:(NSString *)members GroupName:(NSString *)group_name {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"讀取中，請稍後"];
    
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_group_modify_group_member *api = [[api_group_modify_group_member alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api ModifyGroupWithAccout:account GroupName:group_name Members:members];
}
-(void) api_group_modify_group_memberComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) {
        NSString *gid = groupIDWhenCreate;
        
        [self performSegueWithIdentifier:@"GotoGroupEdit" sender:gid];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 聊天
-(void) Chat {
    [self performSegueWithIdentifier:@"GotoMQTTChatRoom" sender:nil];
}
#pragma mark - 換頁
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"GotoMQTTChatRoom"]) {
        NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
        NSLog(@"_dic:%@",_dic);
        
        [User sharedUser].chatTarget = [_dic mutableCopy];
        MQTTChatRoom *vc = segue.destinationViewController;
        vc.hidesBottomBarWhenPushed = YES; //隱藏下方的TabBar
        vc.friendMemberId = [_dic objectForKey:@"account"];
        vc.ChatRoomUserType = ChatRoomUserType_SingleChat;
    }
    else if([segue.identifier isEqualToString:@"GotoGroupEdit"]) {
        NSString *gid = sender;
        GroupEdit *vc = segue.destinationViewController;
        vc.gid = gid;
        vc.hidesBottomBarWhenPushed = YES; //隱藏下方的TabBar
    }
    
    [self.TableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0] animated:YES];
}
#pragma mark - 雜項
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
}
@end
