

#import "api_upload_voice.h"
#import "JSON.h"

@implementation api_upload_voice
@synthesize delegate;
#pragma mark -
#pragma mark Delegate
-(void) api_upload_voiceComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    if ([[self delegate] respondsToSelector:@selector(api_upload_voiceComplete:Message:Data:)]) {
        [[self delegate] api_upload_voiceComplete:success Message:message Data:data];
    }
}
#pragma mark -
#pragma mark ASIHTTPRequest
-(void) UploadVoiceWithTitle:(NSString *)title Pic:(NSString *)voiceFilePath {
    NSString *urlString = [NSString stringWithFormat:@"http://mqttnewyear.jampush.com.tw:3000"];
    NSLog(@"api_upload_voice url:%@",urlString);

    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIRequest = [[ASIFormDataRequest alloc] initWithURL:url];
    [ASIRequest setTimeOutSeconds:kURLTimeoutSecond];
    [ASIRequest setNumberOfTimesToRetryOnTimeout:2];
    
    //NSLog(@"title:%@",title);
    //NSLog(@"voice:%@",voiceFilePath);
    
    [ASIRequest addPostValue:title forKey:@"title"];
    [ASIRequest setFile:voiceFilePath forKey:@"voice"];
    [ASIRequest setDelegate:self];
    [ASIRequest startAsynchronous];
}
- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    //NSLog(@"api_upload_voice Finished:%@",responseString);
    
    NSError *error = nil;
	SBJSON *parser = [SBJSON new];
	
    //原始資料
	NSDictionary *result = (NSDictionary *) [parser objectWithString:responseString error:&error];
    NSLog(@"api_upload_voice result:%@",result);
    
    while (error) {
        NSLog(@"json error:%@",[error localizedDescription]);
        error = [[error userInfo] objectForKey:NSUnderlyingErrorKey];
        NSLog(@"Error trace: %@", parser.errorTrace);
        [self api_upload_voiceComplete:NO Message:kMessageNetworkFail Data:nil];
        [self performSelector:@selector(Cancle)];
        return;
    }
    
    NSString *key = [result objectForKey:@"key"];
    NSLog(@"api_upload_voice:(%@)",key);
    
    if ([key length] > 0) { //成功
        [self api_upload_voiceComplete:YES Message:nil Data:result];
    }
    else { //失敗
        [self api_upload_voiceComplete:NO Message:@"上傳失敗" Data:nil];
    }
    
    [self performSelector:@selector(Cancle)];
}
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"requestFailed error:%@",error);
    [self api_upload_voiceComplete:NO Message:kMessageNetworkFail Data:nil];
    [self performSelector:@selector(Cancle)];
}
#pragma mark -
#pragma mark 雜項
-(void) Cancle {
    // Cancels an asynchronous request
    [ASIRequest cancel];
    
    // Cancels an asynchronous request, clearing all delegates and blocks first
    [ASIRequest clearDelegatesAndCancel];
    
    //移除物件
    [[VariableStore sharedInstance].arrayHTTPRequests removeObject:self];
}
- (void)dealloc {
    ASIRequest = nil;
    delegate = nil;
}
@end