#import "GroupEdit.h"
#import "HTTPImageView.h"
#import "GroupEditCell.h"
#import "MyBorderTextField.h"

#define AlertTag_ConfirmDelete 1
#define AlertTag_DeleteFinish 2
#define AlertTag_DeleteMemberSelf 3
#define AlertTag_NewGroupName 4

@interface GroupEdit () {
    NSInteger selectedIndex;
    NSInteger selectedIndex_SearchResult;
    NSMutableArray *arrayTableView;
    NSMutableArray *arraySearchResult;
    NSString *group_name;
    NSString *group_owner;
    MyImagePicker *myPicker;
}
@property (nonatomic,weak) IBOutlet HTTPImageView *ImageGroupAvatar;
@property (nonatomic,weak) IBOutlet UILabel *LabelGroupName;
@property (nonatomic,weak) IBOutlet MyBorderTextField *TextSearchName;
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@end

@implementation GroupEdit
#pragma mark - 初始化相關
-(void) initParameter { //初始化參數
    arrayTableView = [[NSMutableArray alloc] init];
    arraySearchResult = [[NSMutableArray alloc] init];
    [self GetGroupInfo];
}
-(void) initUIKit { //初始化畫面
}
#pragma mark - 取得群組資料
-(void) GetGroupInfo {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    //參數歸0
    selectedIndex = 0;
    [arrayTableView removeAllObjects];
    
    api_group_find_group_by_id *api = [[api_group_find_group_by_id alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetGroupWithID:_gid];
}
-(void) api_group_find_group_by_idComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) { //更新UI
        //把群組名稱記錄起來
        group_name = [[NSString alloc] initWithFormat:@"%@",[data objectForKey:@"group_name"]];
        
        //群組owner
        group_owner = [[NSString alloc] initWithFormat:@"%@",[data objectForKey:@"owner"]];
        _LabelGroupName.text = group_name;
        
        [_ImageGroupAvatar setImageWithURL:[data objectForKey:@"group_pic"] placeholderImage:nil];
        
        NSArray *arrayMember = [data objectForKey:@"members_arr"];
        for (NSDictionary *_dic in arrayMember) [arrayTableView addObject:_dic];
        
        [_TableView reloadData];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self Search];
    
    [textField resignFirstResponder];
    
    return YES;
}
#pragma mark - 搜尋
-(BOOL) CanGoNext {
    BOOL CanGoNext = NO;
    
    NSString *alertMessage = @"";
    if  ([self.TextSearchName.text length] == 0) {
        alertMessage = @"請輸入搜尋關鍵字";
    }
    else if  (![[[User sharedUser] ACCOUNT] isEqualToString:group_owner]) { //判斷自己是群組owner
        alertMessage = @"很抱歉，只有群組建立人可以邀請組員";
    }
    else {
        CanGoNext = YES;
    }
    
    if (!CanGoNext) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertMessage message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [_TextSearchName resignFirstResponder];
    }
    
    return CanGoNext;
}
-(IBAction)Search {
    if (![self CanGoNext]) return;
    
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    api_member_search_member *api = [[api_member_search_member alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api SearchMemberWithName:self.TextSearchName.text];
}
-(void) api_member_search_memberComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        //參數歸0
        selectedIndex_SearchResult = 0;
        [arraySearchResult removeAllObjects];
        
        //加入資料
        for (NSDictionary *_dic  in data) [arraySearchResult addObject:_dic];
        
        //用actionsheet顯示結果
        [self ShowActionSheet];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
-(void) ShowActionSheet {
    UIActionSheet *menu = [[UIActionSheet alloc]
                           initWithTitle:@"新增群組成員"
                           delegate:self
                           cancelButtonTitle:nil
                           destructiveButtonTitle:nil
                           otherButtonTitles:nil];
    
    //加入title
    for (NSDictionary *_dic  in arraySearchResult) {
        NSString *CN = [_dic objectForKey:@"chinese_name"];
        [menu addButtonWithTitle:CN];
    }
    
    //取消按鈕
    [menu addButtonWithTitle:@"取消"];
    menu.destructiveButtonIndex = menu.numberOfButtons - 1;
    
    //顯示
    [menu showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"clickedButtonAtIndex:%d",buttonIndex);
    if (buttonIndex < 0) { //iPad按外面的話index是-1
        return;
    }
    
    NSString *selectedString = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    //選到取消
    if ([selectedString isEqualToString:@"取消"]) return;
    else { //搜尋聯絡人的sheet
        //Text復歸
        self.TextSearchName.text = @"";
        [self.TextSearchName resignFirstResponder];
        
        //選擇結果
        selectedIndex_SearchResult = buttonIndex;
        //NSLog(@"selectedIndex_SearchResult:%d",selectedIndex_SearchResult);
        NSDictionary *_dic = [arraySearchResult objectAtIndex:selectedIndex_SearchResult];
        //NSLog(@"_dic:%@",_dic);
        [self AddMemberWithID:[_dic objectForKey:@"account"]];
    }
}
#pragma mark - tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	//製作可重複利用的表格欄位Cell
    static NSString *CellIdentifier = @"GroupEditCell";
    
    GroupEditCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[GroupEditCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.cellIndex = indexPath.row;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    //NSLog(@"_dic:%@",_dic);
    
    //標題
    cell.LabelTitle.text = [NSString stringWithFormat:@"%@ %@",[_dic objectForKey:@"english_name"],[_dic objectForKey:@"chinese_name"]];
    
    //圖
    [cell.ImageTitle setImage:[UIImage imageNamed:@"list_photo.png"]];
    [cell.ImageTitle setImageWithURL:[_dic objectForKey:@"avatar"] placeholderImage:nil];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex = indexPath.row;
}
#pragma mark - 增加/刪除群組成員
-(void) AddMemberWithID:(NSString *)memberID {
    //把現有的成員組成字串
    NSString *members = @"";
    for (NSDictionary *_dic in arrayTableView) {
        members = [members stringByAppendingFormat:@"%@|",[_dic objectForKey:@"account"]];
    }
    members = [members stringByAppendingString:memberID];
    //NSLog(@"要更新的成員字串：%@",members);
    [self ModifyGroupWithMembers:members];
}
-(void) DeleteMemberAtIndex:(NSNotification *)notif {
    if  (![[[User sharedUser] ACCOUNT] isEqualToString:group_owner]) { //判斷自己是群組owner
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"很抱歉，只有群組建立人可以刪除組員" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"確定", nil];
        [alert show];
        return;
    }
    
    NSNumber *number = [notif object];
    selectedIndex = [number intValue];
    //NSLog(@"DeleteMemberAtIndex:%d",selectedIndex);
    
    //取得要刪除的成員代號
    NSDictionary *dicToDelete = [arrayTableView objectAtIndex:selectedIndex];
    NSString *memberID = [dicToDelete objectForKey:@"account"];
    
    //判斷是不是要刪掉自己
    NSString *account = [[User sharedUser] ACCOUNT];
    if ([account isEqualToString:memberID]) { //要刪掉自己就顯示警告
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"刪除您自己(群組建立人)會一併刪除此群組" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
        alert.tag = AlertTag_DeleteMemberSelf;
        [alert show];
    }
    else {
        //組成字串
        NSString *members = @"";
        for (int i = 0; i < [arrayTableView count]; i++) {
            NSDictionary *_dic = [arrayTableView objectAtIndex:i];
            if ([[_dic objectForKey:@"account"] isEqualToString:memberID]) { //要刪的那一筆
                
            }
            else { //其他
                members = [members stringByAppendingFormat:@"%@|",[_dic objectForKey:@"account"]];
            }
        }
        //去掉最右邊的|
        NSString *lastWord = [members substringFromIndex:members.length-1];
        if ([lastWord isEqualToString:@"|"]) {
            members = [members substringToIndex:members.length-1];
        }
        NSLog(@"要更新的成員字串：%@",members);
        [self ModifyGroupWithMembers:members];
    }
}
-(void) ModifyGroupWithMembers:(NSString *)members {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"讀取中，請稍後"];
    
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_group_modify_group_member *api = [[api_group_modify_group_member alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api ModifyGroupWithAccout:account GroupName:group_name Members:members];
}
-(void) api_group_modify_group_memberComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) {
        [self GetGroupInfo]; //重抓一次
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 群組更改名稱
-(IBAction) PromptNewGroupName {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"請輸入群組名稱" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.tag = AlertTag_NewGroupName;
    [alert show];
}
-(void) ModifyGroupWithNewName:(NSString *)new_group_name { //變更群組名稱
    if ([new_group_name length] == 0) return; //防呆
    
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    //取得自己的account
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_group_modify_group_name *api = [[api_group_modify_group_name alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api ModifyGroupNameWithAccout:account GroupName:group_name NewGroupName:new_group_name];
}
-(void) api_group_modify_group_nameComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) { //更新UI
        [self GetGroupInfo]; //重抓一次
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 刪除群組
-(IBAction) ConfirmDelete {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"確定刪除群組?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
    alert.tag = AlertTag_ConfirmDelete;
    [alert show];
}
-(void) DeleteGroup {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"讀取中，請稍後"];
    
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_group_delete_group *api = [[api_group_delete_group alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api DeleteGroupWithAccount:account GroupName:group_name];
}
-(void) api_group_delete_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"群組刪除成功" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"確定", nil];
        alert.tag = AlertTag_DeleteFinish;
        [alert show];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 照片相關
-(IBAction)ChooseImageSource {
    if  (![[[User sharedUser] ACCOUNT] isEqualToString:group_owner]) { //判斷自己是群組owner
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"很抱歉，只有群組建立人可以修改群組照片" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"確定", nil];
        [alert show];
        return;
    }
    
    myPicker = [[MyImagePicker alloc] init];
    myPicker.SourceVC = self;
    myPicker.delegate = self;
    [myPicker UpdateUI];
}
-(void) MyImagePickerFinishedWithImage:(UIImage *)image {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"上傳中"];
    
    //API-上傳群組照片
    api_group_update_group_pic *api = [[api_group_update_group_pic alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api UploadGroupPicWithGid:_gid Pic:image];
}
-(void) api_group_update_group_picComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) { //更新UI
        [self.ImageGroupAvatar setImageWithURL:[data objectForKey:@"avatar_url"] placeholderImage:nil];
    }
    else {
    }
}
#pragma mark - Notification
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DeleteMemberAtIndex:) name:@"DeleteMember" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DeleteMember" object:nil];
}
#pragma mark - 雜項
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *selectedString = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([selectedString isEqualToString:@"確定"]) {
        if (alertView.tag == AlertTag_ConfirmDelete || alertView.tag == AlertTag_DeleteMemberSelf) { //確認刪除，或是刪除自己
            [self DeleteGroup];
        }
        else if (alertView.tag == AlertTag_DeleteFinish) { //刪除完畢
            [self.navigationController popViewControllerAnimated:YES];
        }
        else if (alertView.tag == AlertTag_NewGroupName) { //群組名稱
            UITextField *textField = [alertView textFieldAtIndex:0];
            NSString *newGroupName = textField.text;
            NSLog(@"要變更的群組名稱：%@",newGroupName);
            [self ModifyGroupWithNewName:newGroupName];
        }
    }
}
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
}
@end
