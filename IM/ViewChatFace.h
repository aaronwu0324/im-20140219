@protocol ViewChatFaceDelegate <NSObject>
@optional
-(void) ViewChatFaceSelectedFace:(NSString *)selectedFace; //選擇到的表情符號
-(void) ViewChatFaceSelectedImage:(NSString *)urlString; //選擇到的圖片網址
@end

enum ViewChatFaceTag {
    ViewChatFaceTag_Face = 1,//表情符號
    ViewChatFaceTag_Image = 2, //貼圖
    ViewChatFaceTag_Common = 3, //常用
} ViewChatFaceTag;

#import <UIKit/UIKit.h>
#import "api_img_list_all_mqtt_img.h"

@interface ViewChatFace : UIView  <NSXMLParserDelegate,api_img_list_all_mqtt_imgDelegate> {
    __weak id <ViewChatFaceDelegate> delegate;
    NSMutableArray *arrayFaces;
    NSMutableArray *arrayTableView;
    NSInteger numbersOfCells; //TableView行數(圖)
    NSInteger numbersOfData; //總資料數(圖)
}
@property (weak) id delegate;
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollViewFace;
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollViewImage;
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollViewCommon;
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@property (nonatomic,weak) IBOutlet UIButton *ButtonFace;
@property (nonatomic,weak) IBOutlet UIButton *ButtonImage;
@property (nonatomic,weak) IBOutlet UIButton *ButtonCommon;
-(void) UpdateUI;
@end
