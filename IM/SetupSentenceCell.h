@protocol SetupSentenceCellDelegate <NSObject>
@optional
-(void) SetupSentenceCellDeleteAtIndex:(NSInteger)index;
@end

#import <UIKit/UIKit.h>

@interface SetupSentenceCell : UITableViewCell {
    __weak id <SetupSentenceCellDelegate> delegate;
}
@property (weak) id delegate;
@property (nonatomic, weak) IBOutlet UILabel *LabelTitle;
@end
