

#import "ViewController.h"
#import "User.h"
#import "MQTTManager.h"
#import <QuartzCore/QuartzCore.h> 
#import "MyBorderTextField.h"
#import "NSData+CommonCrypto.h"

@interface ViewController () {
    BOOL isAnimating;
    BOOL shouldAnimatingBack;
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet MyBorderTextField *TextDomain;
@property (nonatomic,weak) IBOutlet MyBorderTextField *TextAccount;
@property (nonatomic,weak) IBOutlet MyBorderTextField *TextPassword;
@end

@implementation ViewController
#pragma mark -
#pragma mark UIViewControll 初始化相關
- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
-(void) viewDidUnload {
    [super viewDidUnload];
}
#pragma mark -
#pragma mark 初始化相關
-(void) initParameter { //初始化參數
}
-(void) initUIKit { //初始化畫面
    //有成功登入就直接登入
    NSError *error = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *dataPassword = [defaults objectForKey:kDEFAULTS_KEY_MEMBER_PASSWORD];
    dataPassword = [dataPassword decryptedAES256DataUsingKey:kENCRYPT_KEY error:&error];
    
    while (error) {
        NSLog(@"密碼解密錯誤:%@",[error localizedDescription]);
    }
    
    NSString *password = [[NSString alloc] initWithData:dataPassword encoding:NSASCIIStringEncoding];
    _TextPassword.text = password;
    
    _TextAccount.text = [[User sharedUser] ACCOUNT];
    
    if ([_TextAccount.text length] > 0 && [_TextPassword.text length] > 0) {
        [self Login];
    }
    
    
    //測試
    return;
    
    self.TextDomain.text = @"debug";

#if TARGET_IPHONE_SIMULATOR
    self.TextAccount.text = @"leo.chen";
#else
    self.TextAccount.text = @"peter.you";
#endif
    self.TextPassword.text = @"123";
}
#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    shouldAnimatingBack = NO;
    isAnimating = NO;
    
    if (textField == self.TextDomain) {
        isAnimating = YES;
        if (IS_IPHONE_5) {
            [self.ScrollView setContentOffset:CGPointMake(0, 70) animated:YES];
        }
        else {
            [self.ScrollView setContentOffset:CGPointMake(0, 150) animated:YES];
        }
    }
    else if (textField == self.TextAccount) {
        isAnimating = YES;
        if (IS_IPHONE_5) {
            [self.ScrollView setContentOffset:CGPointMake(0, 110) animated:YES];
        }
        else {
            [self.ScrollView setContentOffset:CGPointMake(0, 210) animated:YES];
        }
    }
    else if (textField == self.TextPassword) {
        isAnimating = YES;
        if (IS_IPHONE_5) {
            [self.ScrollView setContentOffset:CGPointMake(0, 170) animated:YES];
        }
        else {
            [self.ScrollView setContentOffset:CGPointMake(0, 210) animated:YES];
        }
    }
    else {
        //[[VariableStore sharedInstance].AnimationClass MakeTranslationBack:self.view time:0.4f];
    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    /*if (textField == self.TextAnnoymous) {
        return NO;
    }*/
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (shouldAnimatingBack) {
        [self.ScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    shouldAnimatingBack = YES;
    [textField resignFirstResponder];
    
    return YES;
}
#pragma mark - 捐款API
-(BOOL) CanGoNext {
    BOOL CanGoNext = NO;
    
    NSString *alertMessage = @"";
    if  ([self.TextDomain.text length] == 0) {
        alertMessage = @"請輸入網域";
    }
    else if  ([self.TextAccount.text length] == 0) {
        alertMessage = @"請輸入帳號";
    }
    else if  ([self.TextPassword.text length] == 0) {
        alertMessage = @"請輸入密碼";
    }
    else {
        CanGoNext = YES;
    }
    
    if (!CanGoNext) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertMessage message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    return CanGoNext;
}
-(IBAction)Login {
    if (![self CanGoNext]) return;

    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    api_member_member_login *api = [[api_member_member_login alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api LoginWithAccount:self.TextAccount.text Password:self.TextPassword.text Dxid:[[User sharedUser] UDID]];
}
-(void) api_member_member_loginComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        NSDictionary *_dic = [data objectForKey:@"data"];
        [self GetMemberDataWithAccount:[_dic objectForKey:@"account"]]; //取得自己的詳細資料
        
        //把帳密儲存起來
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSError *error = nil;
        NSString *passwrod = _TextPassword.text;
        NSData* dataPassword = [passwrod dataUsingEncoding:NSUTF8StringEncoding];
        dataPassword = [dataPassword AES256EncryptedDataUsingKey:kENCRYPT_KEY error:&error];
        while (error) {
            NSLog(@"密碼加密錯誤:%@",[error localizedDescription]);
        }
        [defaults setObject:dataPassword forKey:kDEFAULTS_KEY_MEMBER_PASSWORD];
        
        //帳號
        //[defaults setObject:[_dic objectForKey:@"account"] forKey:kDEFAULTS_KEY_MEMBER_ACCOUNT];
        [defaults synchronize];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - API-查詢會員資料
-(void) GetMemberDataWithAccount:(NSString *)account {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"資料下載中"];
    
    api_member_member_data *api = [[api_member_member_data alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetMemberDataWithAccount:account];
}
-(void) api_member_member_dataComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        //把帳號密碼清空，for登出
        _TextAccount.text = @"";
        _TextPassword.text = @"";
        
        //把相關資料存起來
        [[User sharedUser] updateInfo:data];
        
        [[MQTTManager sharedInstance] start];
        
        [self GotoHome];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark -
#pragma mark 換頁
-(void) GotoHome {
    [self performSegueWithIdentifier:@"GotoHome" sender:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Assign tab bar item with titles
    UITabBarController *tabBarController = (UITabBarController *)segue.destinationViewController;;
    UITabBar *tabBar = tabBarController.tabBar;
    
    //客製化TabBar的圖
    NSArray *arrayItems = tabBar.items;
    NSArray *arrayImagesFile = [NSArray arrayWithObjects:@"bottom_icon01.png",@"bottom_icon02.png",@"bottom_icon03.png",@"bottom_icon04.png",nil];
    NSArray *arrayImagesSelectFile = [NSArray arrayWithObjects:@"bottom_icon01.png",@"bottom_icon02.png",@"bottom_icon03.png",@"bottom_icon04.png",nil];
    
    for (int i = 0; i < [arrayItems count]; i++) {
        UITabBarItem *tabBarItem = [arrayItems objectAtIndex:i];
        
        UIImage *Image = [UIImage imageNamed:[arrayImagesFile objectAtIndex:i]];
        UIImage *ImageSelect = [UIImage imageNamed:[arrayImagesSelectFile objectAtIndex:i]];
        Image = [Image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        ImageSelect = [ImageSelect imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [tabBarItem setImage:Image];
        [tabBarItem setSelectedImage:ImageSelect];
        
        //tabBarItem1.title = @"名單";
    }
    
    // Change the tab bar background
    UIImage* tabBarBackground = [UIImage imageNamed:@"bottom_btn_bg.png"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"bottom_btn_on.png"]];
    
    // Change the title color of tab bar items
    UIColor *titleHighlightedColor = [UIColor colorWithRed:187/255.0 green:202/255.0 blue:216/255.0 alpha:1.0];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       titleHighlightedColor, NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       titleHighlightedColor, NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateSelected];
}
#pragma mark - 雜項
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
}
@end
