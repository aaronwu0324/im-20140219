

#import <UIKit/UIKit.h>
#import "HTTPImageView.h"

@interface GroupEditCell : UITableViewCell

@property (nonatomic, assign) IBOutlet UILabel *LabelTitle;
@property (nonatomic, assign) IBOutlet HTTPImageView *ImageTitle;
@property NSInteger cellIndex;
@end
