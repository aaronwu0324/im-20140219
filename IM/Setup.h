

#import <UIKit/UIKit.h>
#import "rootViewController.h"
#import "api_member_member_data.h"
#import "SetupAvatar.h"
#import "api_member_update_avatar.h"
#import "MyImagePicker.h"

@interface Setup : rootViewController <api_member_member_dataDelegate,SetupAvatarDelegate,UIActionSheetDelegate,api_member_update_avatarDelegate,MyImagePickerDelegate>
@end
