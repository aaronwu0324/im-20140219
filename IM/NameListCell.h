

#import <UIKit/UIKit.h>

@interface NameListCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *LabelName;
@property (nonatomic, weak) IBOutlet UILabel *LabelEnglishName;
@property (nonatomic, weak) IBOutlet UILabel *LabelLocation;
@end
