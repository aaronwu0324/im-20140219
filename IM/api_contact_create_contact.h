
@protocol api_contact_create_contactDelegate <NSObject>
@optional
-(void) api_contact_create_contactComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_contact_create_contact : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_contact_create_contactDelegate> delegate;
}
@property (weak) id delegate;
-(void) CreateContactWithAccount:(NSString *)account Contact:(NSString *)contact;
@end
