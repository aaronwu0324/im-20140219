

#import <UIKit/UIKit.h>
#import "rootViewController.h"
#import "api_member_member_login.h"
#import "api_member_member_data.h"

@interface ViewController : rootViewController <api_member_member_loginDelegate,api_member_member_dataDelegate>

@end
