#import "GroupChat.h"
#import "GroupEdit.h"
#import "GroupChatCell.h"
#import "DBProcess.h"
#import "MQTTChatRoom.h"

@interface GroupChat () {
    NSInteger selectedIndex;
    NSInteger selectedIndex_SearchResult;
    NSMutableArray *arrayTableView;
    NSMutableArray *arraySearchResult;
    BOOL isGettingGroupList;
}
@property (nonatomic,weak) IBOutlet UIButton *ButtonAdd;
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@end

@implementation GroupChat
#pragma mark - 初始化相關
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self GetGroupList];
}
-(void) initParameter { //初始化參數
    arrayTableView = [[NSMutableArray alloc] init];
    arraySearchResult = [[NSMutableArray alloc] init];
}
-(void) initUIKit { //初始化畫面
}
#pragma mark - 取得自己帳號的群組清單
-(void) GetGroupList {
    if (isGettingGroupList) return;
    
    isGettingGroupList = YES;
    
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    //參數復歸
    selectedIndex = 0;
    [arrayTableView removeAllObjects];
    
    //取得自己的account
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_group_list_group *api = [[api_group_list_group alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetGroupWithAccount:account];
}
-(void) api_group_list_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    isGettingGroupList = NO;
        
    //[[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) { //更新UI
        /*NSArray *arrayInGroup = [data objectForKey:@"in_group"];
        NSArray *arrayMyGroup = [data objectForKey:@"my_group"];
        
        NSMutableArray *arrayData = [[NSMutableArray alloc] init];
        for (NSDictionary *_dic in arrayInGroup) [arrayData addObject:_dic]; //in_group(被加的)
        for (NSDictionary *_dic in arrayMyGroup) [arrayData addObject:_dic]; //my_group(加人的)
        
        for (NSDictionary *_dic in arrayData) [arrayTableView addObject:_dic];
        
        //更新TableView
        [self.TableView reloadData];*/
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    [self GetMyGroupList];
}
#pragma mark - 取得自己建的群組清單
-(void) GetMyGroupList {
    //取得自己的account
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_group_list_my_group *api = [[api_group_list_my_group alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetMyGroupWithAccount:account];
}
-(void) api_group_list_my_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    //[[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) { //更新UI
        for (NSDictionary *_dic in data) [arrayTableView addObject:_dic];
    }
    else {
    }
    
    [self GetInGroupList];
}
#pragma mark - 取得自己所屬的群組清單
-(void) GetInGroupList {
    //取得自己的account
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_group_list_in_group *api = [[api_group_list_in_group alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetInGroupWithAccount:account];
}
-(void) api_group_list_in_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) { //更新UI
        for (NSDictionary *_dic in data) {
            //判斷重複的群組就不用再加入了
            BOOL shouldAdd = YES;
            
            NSString *gid = [_dic objectForKey:@"gid"];
            for (NSDictionary *_dicGroup in arrayTableView) {
                NSString *gidGroup = [_dicGroup objectForKey:@"gid"];
                if ([gid isEqualToString:gidGroup]) {
                    shouldAdd = NO;
                    break;
                }
            }
            
            if (shouldAdd) {
                [arrayTableView addObject:_dic];
            }
        }
        [_TableView reloadData];
    }
    else {
    }
    
    if ([[VariableStore sharedInstance].notifyMemberID length] > 0) { //經由通知到此畫面，，找出該群組的相關資料，然後直接進入聊天畫面
        
        DBProcess *process = [[DBProcess alloc] init];
        
        for (int i = 0; i < [arrayTableView count]; i++) {
            NSDictionary *_dic = [arrayTableView objectAtIndex:i];
            NSString *gid = [process GetGroupIDWithGroupName:[_dic objectForKey:@"group_name"]];
            if ([gid isEqualToString:[VariableStore sharedInstance].notifyMemberID]) {
                selectedIndex = i;
                break;
            }
        }
        
        [self performSegueWithIdentifier:@"GotoMQTTChatRoom" sender:nil];
        
        //參數歸0
        [VariableStore sharedInstance].notifyMemberID = @"";
    }
}
#pragma mark - 新增群組
-(IBAction) PromptNewGroupName {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"請輸入群組名稱" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *selectedString = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([selectedString isEqualToString:@"確定"]) {
        UITextField *textField = [alertView textFieldAtIndex:0];
        NSString *groupName = textField.text;
        [self CreateGroupWithName:groupName];
        NSLog(@"要新增的群組名稱：%@",groupName);
    }
}
-(void) CreateGroupWithName:(NSString *)group_name { //建立群組
    if ([group_name length] == 0) return; //防呆
    
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"載入中"];
    
    //取得自己的account
    NSString *account = [[User sharedUser] ACCOUNT];
    
    api_group_create_group *api = [[api_group_create_group alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api CreateGroupWithAccount:account Group_Name:group_name Members:account];
}
-(void) api_group_create_groupComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) { //更新UI
        NSString *gid = [NSString stringWithFormat:@"%@",[data objectForKey:@"gid"]];
        [self performSegueWithIdentifier:@"GotoGroupEdit" sender:gid];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	//製作可重複利用的表格欄位Cell
    static NSString *CellIdentifier = @"GroupChatCell";
    
    GroupChatCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[GroupChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.cellIndex = indexPath.row;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    NSLog(@"_dic:%@",_dic);
    
    //標題
    cell.LabelTitle.text = [_dic objectForKey:@"group_name"];
    
    //圖
    NSString *urlString = [_dic objectForKey:@"group_pic"];
    //NSLog(@"urlString:%@",urlString);
    [cell.ImageTitle setImageWithURL:urlString placeholderImage:nil];
    
    //內容
    //cell.LabelContent.text = [_dic objectForKey:@"message"];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex = indexPath.row;
    
    //跟該人聊天
    [self performSegueWithIdentifier:@"GotoMQTTChatRoom" sender:nil];
}
#pragma mark - 編輯群組
-(void) GroupEditAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    selectedIndex = [number intValue];
    NSLog(@"selectedIndex:%d",selectedIndex);
    
    //取得group id
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *group_name = [_dic objectForKey:@"group_name"];
    
    //取得gid
    DBProcess *process = [[DBProcess alloc] init];
    NSString *gid = [process GetGroupIDWithGroupName:group_name];
    
    [self performSegueWithIdentifier:@"GotoGroupEdit" sender:gid];
}
#pragma mark - 換頁
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"GotoGroupEdit"]) {
        NSString *gid = sender;
        GroupEdit *vc = segue.destinationViewController;
        vc.gid = gid;
        vc.hidesBottomBarWhenPushed = YES; //隱藏下方的TabBar
    }
    else if([segue.identifier isEqualToString:@"GotoMQTTChatRoom"]) {
        NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
        NSLog(@"_dic:%@",_dic);
        
        //群組名稱
        NSString *group_name = [NSString stringWithFormat:@"%@",[_dic objectForKey:@"group_name"]];
        
        //取得gid
        DBProcess *process = [[DBProcess alloc] init];
        NSString *gid = [process GetGroupIDWithGroupName:group_name];
        
        //聊天室
        MQTTChatRoom *vc = segue.destinationViewController;
        vc.hidesBottomBarWhenPushed = YES; //隱藏下方的TabBar
        vc.gid = gid;
        vc.group_name = group_name;
        vc.ChatRoomUserType = ChatRoomUserType_GroupChat;
    }
    
    [self.TableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0] animated:YES];
}
#pragma mark - Notification
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GroupEditAtIndex:) name:@"GroupEdit" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GroupEdit" object:nil];
}
#pragma mark - 雜項
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
}
@end
