#import "MQTTChatRoom.h"
#import "User.h"
#import "MQTTManager.h"
#import "DBProcess.h"
#import "NotifyBar.h"
#import "ChatSubViewCell.h"
#import "MyBorderTextView.h"
#import "ImageViewerVC.h"

#define INPUT_HEIGHT 35.0f

@interface MQTTChatRoom () {
    NSInteger selectedIndex;
    NSMutableArray *arrayTableView;
    NSMutableDictionary *chatTarget; //聊天對象
    NSDateFormatter *formatter;
    NSMutableArray *arrayMessages;
    DBProcess *process;
    NSString *currentChannel;
    MyImagePicker *myPicker;
}
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@property (nonatomic,weak) IBOutlet UIView *ViewTypingText;
@property (nonatomic,weak) IBOutlet MyBorderTextView *TextViewMessage;
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic,weak) IBOutlet UILabel *LabelChatTargetName;
@property (nonatomic,weak) IBOutlet UIButton *ButtonDismissTypingView;
@end

@implementation MQTTChatRoom
@synthesize ViewTypingText;
@synthesize TextViewMessage;
#pragma mark - 初始化相關
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleWillShowKeyboard:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleWillHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClickImageAtCellWithUrl:) name:@"ClickImageAtCellWithUrl" object:nil];
    [VariableStore sharedInstance].isInChatRoom = YES;
    if (ChatRoomUserType == ChatRoomUserType_GroupChat) [VariableStore sharedInstance].currentChatRoomGroupID = _gid;
}
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ClickImageAtCellWithUrl" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kChatroomShouldHandleMessageWasSend" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kChatroomShouldHandleMessageWasRead" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kChatroomShouldHandleYouGotNewMessage" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kChatroomShouldHandleMessageFromOtherDevice" object:nil];
    [VariableStore sharedInstance].isInChatRoom = NO;
    if (ChatRoomUserType == ChatRoomUserType_GroupChat) [VariableStore sharedInstance].currentChatRoomGroupID = @"";
}
-(void) initParameter { //初始化參數
    arrayTableView = [[NSMutableArray alloc] init];
    arrayMessages = [[NSMutableArray alloc] init];
    process = [[DBProcess alloc] init];
    
    if (_ChatRoomUserType == ChatRoomUserType_SingleChat) { //個聊
        chatTarget = [User sharedUser].chatTarget;
        [self CreateChannel];
    }
    else { //群聊
        [self CreateGroupChannel];
    }
}
-(void) initUIKit { //初始化畫面
    if (_ChatRoomUserType == ChatRoomUserType_SingleChat) { //個聊
        self.LabelTitle.text = [chatTarget objectForKey:@"location"];
        self.LabelChatTargetName.text = [chatTarget objectForKey:@"chinese_name"];
    }
    else { //群聊
        self.LabelTitle.frame = CGRectMake(0, 0, 320, 44);
        self.LabelTitle.text = _group_name;
        self.LabelChatTargetName.hidden = YES;
    }
}
#pragma mark - API-訂閱聊天頻道(個聊)
-(void) CreateChannel {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"連線中"];
    
    //自己的帳戶
    NSString *ACCOUNT = [[User sharedUser] ACCOUNT];
    NSString *APPID = [[User sharedUser] APPID];
    NSString *UDID = [[User sharedUser] UDID];
    
    api_mqtt_create *api = [[api_mqtt_create alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api CreateMQTTChannelFromMYID:ACCOUNT HERID:self.friendMemberId APPID:APPID UDID:UDID];
}
-(void) api_mqtt_createComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        //把這個聊天的頻道存起來
        currentChannel = [[NSString alloc] initWithString:message];
        
        [self setupParams];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - API-訂閱聊天頻道(群聊)
-(void) CreateGroupChannel {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"連線中"];
    
    api_mqtt_group_create *api = [[api_mqtt_group_create alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api CreateMQTTChannelWithGroupID:_gid];
}
-(void) api_mqtt_group_createComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    if (success) { //更新UI
        //把這個聊天的頻道存起來
        currentChannel = [[NSString alloc] initWithString:message];
        
        [self setupParams];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
#pragma mark - 初始參數
- (void)setupParams {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self setupMHChannel];
        formatter = [[NSDateFormatter alloc]init];
        
        [self GetMessageFromApi]; //取得離線訊息
    });
}
#pragma mark - 取得離線訊息
-(void) GetMessageFromApi {
    //聊天對象ID
    //NSString *toID = [self GetTalkingID];
    
    //NSString *timestamp = [process GetLastMessageTimestampWithToID:toID];
    //NSLog(@"timestamp:%@",timestamp);
    
    //問後台有沒有離線訊息
    NSString *timestamp = @"";
    if ([timestamp length] == 0) {
        NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
        _formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        timestamp = [_formatter stringFromDate:[NSDate date]];
    }
    
    api_mqtt_channel_log *api = [[api_mqtt_channel_log alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api GetChannelLogWithChannel:currentChannel Timestamp:timestamp];
}
-(void) api_mqtt_channel_logComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data {
    if (success) { //更新UI
        //存起來到sqlite
        
        //後台資料格式
        //"channel": "channel/f6bb7ef5b2369db2a3eacd63cab162f6/data",
        //"fid": "1021405040",
        //"mid": "4A1019BC-9F03-4467-8B60-096E40270525",
        //"say": "123",
        //"t": "1379232421161",
        //"tid": "1021405041"
        
        for (NSDictionary *_dic in data) { //把後台的資料轉成message
            //NSLog(@"_dic:%@",_dic);
            NSString *fromId = [_dic objectForKey:@"fid"];
            //NSString *isFail = [_dic objectForKey:@"isFail"];
            //NSString *isRead = [_dic objectForKey:@"isRead"];
            //NSString *isRecv = [_dic objectForKey:@"isRecv"];
            //NSString *isSend = [_dic objectForKey:@"isSend"];
            NSString *message = [_dic objectForKey:@"say"];
            NSString *messageId = [_dic objectForKey:@"mid"];
            //NSString *readDate = [_dic objectForKey:@"readDate"];
            //NSString *recvDate = [_dic objectForKey:@"recvDate"];
            NSString *toId = [_dic objectForKey:@"tid"];
         
            NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
            _formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            NSDate *date_t = [NSDate dateWithTimeIntervalSince1970:[[_dic objectForKey:@"t"] doubleValue]/1000.0]; //換算成日期
            NSString *stringDate = [_formatter stringFromDate:date_t];
            //NSLog(@"t1:%@",[_dic objectForKey:@"t"]);
            //NSLog(@"t:%f",[[_dic objectForKey:@"t"] doubleValue]);
            //NSLog(@"date_t:%@",date_t);
            //NSLog(@"stringDate:%@",stringDate);
            NSString *sendDate = stringDate;
            //NSLog(@"sendDate:%@",sendDate);
         
            NSMutableDictionary *myDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          fromId,@"fromId",
                                          //isFail,@"isFail",
                                          //isRead,@"isRead",
                                          //isRecv,@"isRecv",
                                          //isSend,@"isSend",
                                          message,@"message",
                                          messageId,@"messageId",
                                          //readDate,@"readDate",
                                          //recvDate,@"recvDate",
                                          sendDate,@"sendDate",
                                          toId,@"toId",nil];
            //NSLog(@"myDic:%@",myDic);
            
            //設定訊息是群聊
            if (_ChatRoomUserType == ChatRoomUserType_GroupChat) {
                [myDic setObject:@"1" forKey:@"isGroupMessage"];
            }
            
            //如果訊息不存在就存起來
            if (![process didMessageExistWithMessageID:[myDic objectForKey:@"messageId"]]) {
                [process SaveMessage:myDic];
            }
         }
        
        [self GetMessagesFromDB];
    }
    else {
    }
}
#pragma mark - 從DB取出訊息
- (void)GetMessagesFromDB {
    NSString *account = [[User sharedUser] ACCOUNT];
    
    NSString *MEMID = account;
    
    //聊天對象ID
    NSString *TMEMID = [self GetTalkingID];
    
    //載入所有歷史訊息
    BOOL isGroupMessasge = _ChatRoomUserType == ChatRoomUserType_GroupChat ? YES : NO;
    NSArray *_messages = [process GetMessagesWithFromMemberID:MEMID ToMemeberID:TMEMID isGroupMessasge:isGroupMessasge];
    
    NSArray *messages = [[CommonTask sharedInstance] SortArrayByDate:_messages Key:@"sendDate"];
    
    /*//reverse排序
    NSMutableArray *messages = [NSMutableArray arrayWithCapacity:[_messages count]];
    NSEnumerator *enumerator = [_messages reverseObjectEnumerator];
    for (id element in enumerator) {
        [messages addObject:element];
    }*/
    
    [messages enumerateObjectsUsingBlock:^(NSMutableDictionary *message, NSUInteger idx, BOOL *stop) {
        //NSLog(@"message:%@",[message objectForKey:@"message"]);
        //整理我傳送失敗的訊息
        if([[message objectForKey:@"fromId"] isEqualToString:MEMID] && ![[message objectForKey:@"isSend"] boolValue]) {
            [message setObject:[NSNumber numberWithBool:YES] forKey:@"isFail"];
        }
        
        //處理未讀訊息
        //if([[message objectForKey:@"toId"] isEqualToString:MEMID] && ![[message objectForKey:@"isRead"] boolValue]) {
        if(![[message objectForKey:@"isRead"] boolValue]) {
            //傳送已收到但未讀取
            
            NSString *sid = _ChatRoomUserType == ChatRoomUserType_GroupChat ? _gid : [message objectForKey:@"fromId"]; //接收的人
            
            NSDictionary *params = @{@"ctrl" : @"channel/read",
                                     @"fid" : [[User sharedUser] ACCOUNT], //[message objectForKey:@"toId"]
                                     @"sid" : sid,
                                     @"mid" : [message objectForKey:@"messageId"]
                                     };
            
            NSData *data = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
            [[MQTTManager sharedInstance]publish:data onTopic:[self channelTopicFromThisChatroom] Qos:2 retain:NO];
        }
        
        [arrayMessages addObject:message];
    }];
    
    //依日期劃分資料
    for (NSDictionary *_dic in arrayMessages) {
        NSString *sendDate = [[_dic objectForKey:@"sendDate"] substringToIndex:10];
        //NSLog(@"sendDate:%@",sendDate);
        
        BOOL shouldAddNewArray = YES;
        for (NSMutableArray *array in arrayTableView) { //如果沒有一樣的日期，就增加一個新的陣列存放相同日期的資料
            for (NSDictionary *_dicData in array) {
                NSString *_sendDate = [[_dicData objectForKey:@"sendDate"] substringToIndex:10];
                if ([sendDate isEqualToString:_sendDate]) {
                    shouldAddNewArray = NO;
                    [array addObject:_dic];
                    break;
                }
            }
        }
        
        if (shouldAddNewArray) {
            NSMutableArray *array = [[NSMutableArray alloc] init];
            [array addObject:_dic];
            [arrayTableView addObject:array];
        }
    }
    
    NSLog(@"arrayTableView:%@",arrayTableView);
    NSLog(@"聊天訊息共有%d個不同的日期",[arrayTableView count]);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.TableView reloadData];
        [self scrollToBottomAnimated:NO];
    });
}
#pragma mark - 設置接收訊息的Notification
- (void)setupMHChannel {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(messageWasSendHandle:) name:@"kChatroomShouldHandleMessageWasSend" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(messageWasReadHandle:) name:@"kChatroomShouldHandleMessageWasRead" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(youGotMessageHandle:) name:@"kChatroomShouldHandleYouGotNewMessage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMessageFromOtherDeviceHandle:) name:@"kChatroomShouldHandleMessageFromOtherDevice" object:nil];
}
#pragma mark - 處理訊息已傳送
- (void)messageWasSendHandle:(NSNotification *)notification {
    NSDictionary *message = [notification object];
    NSLog(@"messageWasSendHandle:%@",message);
    NSMutableDictionary *sendMessage = [message objectForKey:@"message"];
    
    NSString *toID = [self GetTalkingID];
    
    //判斷訊息的接收者，是否屬於這間聊天室
    if([[sendMessage objectForKey:@"toId"] isEqualToString:toID]) {
        NSIndexPath *indexPath = [self CalIndexPathWithMessageID:[sendMessage objectForKey:@"messageId"]];
        if (indexPath) {
            //調整資料
            NSArray *array = [arrayTableView objectAtIndex:indexPath.section];
            NSMutableDictionary *message = [array objectAtIndex:indexPath.row];
            [message setObject:[NSNumber numberWithBool:YES] forKey:@"isSend"];
            
            //refresh tableview
            //[self.TableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
    }
}
#pragma mark - 處理訊息已被讀取
- (void)messageWasReadHandle:(NSNotification *)notification {
    NSDictionary *message = [notification object];
    NSLog(@"messageWasReadHandle:%@",message);
    NSMutableDictionary *readMessage = [message objectForKey:@"message"];
    
    NSString *toID = [self GetTalkingID];
    
    //屬於這間聊天室的訊息
    if([[readMessage objectForKey:@"toId"] isEqualToString:toID]) {
        //計算資料應該在哪個section跟row
        NSInteger sectionToAdd = 0;
        NSInteger rowToAdd = 0;
        
        for (int section = 0; section < [arrayTableView count]; section++) {
            NSMutableArray *array = [arrayTableView objectAtIndex:section];
            for (int row = 0; row < [array count]; row++) {
                NSMutableDictionary *dicMessage = [array objectAtIndex:row];
                if ([[dicMessage objectForKey:@"messageId"] isEqualToString:[readMessage objectForKey:@"messageId"]]) {
                    [dicMessage setObject:[NSNumber numberWithBool:YES] forKey:@"isRead"];
                    sectionToAdd = section;
                    rowToAdd = row;
                }
            }
        }
        
        NSIndexPath *index = [NSIndexPath indexPathForRow:rowToAdd inSection:sectionToAdd];
        NSArray *arrayToAdd = [NSArray arrayWithObjects:index, nil];
        NSLog(@"在第%d個section，第%drow更新已讀",sectionToAdd,rowToAdd);
        [self.TableView reloadRowsAtIndexPaths:arrayToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
}
#pragma mark - 處理接收到新訊息
- (void)youGotMessageHandle:(NSNotification *)notification {
    NSDictionary *message = [notification object];
    NSLog(@"youGotMessageHandle:%@",message);
    
    NSMutableDictionary *newMessage = [message objectForKey:@"message"];
    
    //判斷是不是這個聊天室的訊息
    BOOL isThisRoomMessage = NO;
    if (_ChatRoomUserType == ChatRoomUserType_SingleChat) { //個聊
        if([[newMessage objectForKey:@"fromId"] isEqualToString:[self GetTalkingID]]) { //正在聊天對象傳來的
            isThisRoomMessage = YES;
        }
    }
    else { //群聊
        //判斷是不是這個群組的訊息
        NSString *toID = [newMessage objectForKey:@"toId"];
        isThisRoomMessage = [toID isEqualToString:_gid];
    }
    
    if(isThisRoomMessage) {
        NSLog(@"是這個聊天室的訊息");
        //[arrayMessages addObject:newMessage];
        [self AddDataToTableView:newMessage];
        //[self.TableView reloadData];
        [self scrollToBottomAnimated:YES];
        
        //傳送已讀取
        NSString *sid = _ChatRoomUserType == ChatRoomUserType_GroupChat ? _gid : [newMessage objectForKey:@"fromId"]; //接收的人
        
        NSDictionary *params = @{@"ctrl" : @"channel/read",
                                 @"fid" : [[User sharedUser] ACCOUNT], //[[NSUserDefaults standardUserDefaults] objectForKey:@"member_id"]
                                 @"sid" : sid,
                                 @"mid" : [newMessage objectForKey:@"messageId"]
                                 };
        NSLog(@"params:%@",params);
        NSData *data = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
        [[MQTTManager sharedInstance]publish:data onTopic:[self channelTopicFromThisChatroom] Qos:2 retain:NO];
    }
    else { //其他人傳來的
        [[NotifyBar defaultNotify] showInChatRoom];
    }
}
#pragma mark - 處理你從別的 device 發出的訊息
- (void)newMessageFromOtherDeviceHandle:(NSNotification *)notification {
    NSDictionary *message = [notification object];
    NSMutableDictionary *newMessage = [message objectForKey:@"message"];
    
    [arrayMessages addObject:newMessage];
    [self.TableView reloadData];
    [self scrollToBottomAnimated:YES];
}
#pragma mark - 資料加入
-(void) AddDataToTableView:(NSDictionary *)_dic {
    //依日期劃分資料
    NSString *_sendDate = [_dic objectForKey:@"sendDate"];
    NSString *sendDate;
    if ([_sendDate isKindOfClass:[NSString class]]) {
        sendDate = [_sendDate substringToIndex:10];
    }
    else if ([_sendDate isKindOfClass:[NSDate class]]) { //從MQTT來的，可能是NSDate
        NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
        _formatter.dateFormat = @"yyyy-MM-dd HH:mm"; //yyyy/MM/dd HH:mm:ss
        sendDate = [[_formatter stringFromDate:(NSDate *)_sendDate] substringToIndex:10];
    }
    
    //NSLog(@"sendDate:%@",sendDate);
    
    //計算資料應該加在哪個section跟row
    BOOL shouldAddNewArray = YES;
    NSInteger sectionToAdd = 0;
    NSInteger rowToAdd = 0;
    
    //如果沒有一樣的日期，就增加一個新的陣列存放相同日期的資料
    for (int section = 0; section < [arrayTableView count]; section++) {
        NSMutableArray *array = [arrayTableView objectAtIndex:section];
        for (int row = 0; row < [array count]; row++) {
            NSDictionary *_dicData = [array objectAtIndex:row];
            NSString *_sendDate = [[_dicData objectForKey:@"sendDate"] substringToIndex:10];
            if ([sendDate isEqualToString:_sendDate]) {
                shouldAddNewArray = NO;
                [array addObject:_dic];
                sectionToAdd = section;
                rowToAdd = [array count] - 1;
                break;
            }
        }
        sectionToAdd = section;
    }
    
    if (shouldAddNewArray) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [array addObject:_dic];
        [arrayTableView addObject:array];
        rowToAdd = 0;
    }
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:rowToAdd inSection:sectionToAdd];
    NSArray *arrayToAdd = [NSArray arrayWithObjects:index, nil];
    NSLog(@"在第%d個section，第%drow加入cell",sectionToAdd,rowToAdd);
    //NSLog(@"arrayTableView:%@",arrayTableView);
    if (sectionToAdd == 0 && rowToAdd == 0) {
        [self.TableView reloadData];
    }
    else {
        [self.TableView insertRowsAtIndexPaths:arrayToAdd withRowAnimation:UITableViewRowAnimationRight];
    }
}
-(NSIndexPath *) CalIndexPathWithMessageID:(NSString *)messageID { //依據message的id判斷是哪一個cell
    NSIndexPath *indexPathToReturn = nil;
    
    NSInteger numberOfSection = [arrayTableView count];
    NSInteger section = -1;
    NSInteger row = -1;
    
    for (int i = 0; i < numberOfSection; i++) {
        NSArray *array = [arrayTableView objectAtIndex:i];
        NSInteger numberOfRow = [array count];
        for (int j = 0; j < numberOfRow; j++) {
            NSMutableDictionary *message = [array objectAtIndex:j];
            NSString *_message = [message objectForKey:@"messageId"];
            //NSLog(@"%@ %@",messageID,_message);
            if ([messageID isEqualToString:_message]) {
                section = i;
                row = j;
                break;
            }
        }
    }
    
    if (section >= 0 && row >= 0) { //防呆
        indexPathToReturn = [NSIndexPath indexPathForRow:row inSection:section];
    }
    
    NSLog(@"訊息在第%d個Section 第%d個Row",section,row);
    
    return indexPathToReturn;
}
#pragma mark - UITableView delegate dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [arrayTableView count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *array = [arrayTableView objectAtIndex:section];
    return [array count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *array = [arrayTableView objectAtIndex:indexPath.section];
    NSMutableDictionary *message = [array objectAtIndex:indexPath.row];
    
    return [[CommonTask sharedInstance] cellHeightForText:[message objectForKey:@"message"]];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSArray *array = [arrayTableView objectAtIndex:section];
    NSMutableDictionary *message = [array objectAtIndex:0];
    
    NSString *_sendDate = [message objectForKey:@"sendDate"];
    NSString *sendDate;
    if ([_sendDate isKindOfClass:[NSString class]]) {
        sendDate = [_sendDate substringToIndex:10];
    }
    else if ([_sendDate isKindOfClass:[NSDate class]]) { //從MQTT來的，可能是NSDate
        NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
        _formatter.dateFormat = @"yyyy-MM-dd HH:mm"; //yyyy/MM/dd HH:mm:ss
        sendDate = [[_formatter stringFromDate:(NSDate *)_sendDate] substringToIndex:10];
    }
    //NSLog(@"sendDate:%@",sendDate);
    //把日期轉為月/日(星期)，如7/15(一）
    //日期與時間
    NSString *month = [sendDate substringWithRange:NSMakeRange(5, 2)];
    //NSLog(@"month:%@",month);
    NSString *day = [sendDate substringWithRange:NSMakeRange(8, 2)];
    //NSLog(@"day:%@",day);
    NSString *date = [NSString stringWithFormat:@"%@/%@",month,day];
    //NSLog(@"date:%@",date);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:16];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:103.0/255.0 green:92.0/255.0 blue:83.0/255.0 alpha:1.0f];
    label.text = date;
    return label;
}
/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSArray *array = [arrayTableView objectAtIndex:section];
    NSMutableDictionary *message = [array objectAtIndex:0];
    return [[message objectForKey:@"sendDate"] substringToIndex:10];
}*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatSubViewCell"];
    
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ChatSubViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    ChatSubViewCell *myCell = (ChatSubViewCell *)cell;
    myCell.contentView.userInteractionEnabled = NO; //為了讓Button可以trigger
    cell.tag = indexPath.row;
    [myCell setBackgroundColor:[UIColor clearColor]];
    
    NSArray *array = [arrayTableView objectAtIndex:indexPath.section];
    NSMutableDictionary *message = [array objectAtIndex:indexPath.row];
    NSLog(@"message:%@",message);
    
    //訊息id
    myCell.message_id = [message objectForKey:@"messageId"];
    
    //我
    NSString *account = [[User sharedUser] ACCOUNT];
    if([[message objectForKey:@"fromId"] isEqualToString:account]) { //自己發的
        myCell.messageType = chatMessageTypeRight;
    }
    else { //對方
        myCell.messageType = chatMessageTypeLeft;
        myCell.nickname = [message objectForKey:@"fromId"];
    }
    
    //群組相關調整
    myCell.ChatRoomUserType = _ChatRoomUserType;
    
    myCell.Message = [message objectForKey:@"message"];
    myCell.member_id = [message objectForKey:@"fromId"];
    
    //判斷已讀
    if ([[message objectForKey:@"isRead"] boolValue] && [[message objectForKey:@"fromId"] isEqualToString:account]) { //是自己發的且已被讀取，就標示已讀
        myCell.isRead = YES;
    }
    
    //判斷已發
    if ([[message objectForKey:@"isSend"] boolValue] && [[message objectForKey:@"fromId"] isEqualToString:account]) { //是自己發的且已發，就標示已發
        myCell.isSend = YES;
    }
    
    //日期
    NSString *sendDate = [message objectForKey:@"sendDate"];
    if ([sendDate isKindOfClass:[NSString class]]) {
        myCell.datetime = sendDate;
    }
    else if ([sendDate isKindOfClass:[NSDate class]]) { //從MQTT來的，可能是NSDate
        NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
        _formatter.dateFormat = @"yyyy-MM-dd HH:mm"; //yyyy/MM/dd HH:mm:ss
        NSString *stringDate = [_formatter stringFromDate:[NSDate date]];
        myCell.datetime = stringDate;
    }
    
    [myCell UpdateUI];
    
    return cell;
}
- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}
/*- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    NSMutableDictionary *message = [arrayMessages objectAtIndex:indexPath.row];
    
    NSString *account = [[User sharedUser] ACCOUNT];
    if([[message objectForKey:@"fromId"] isEqualToString:account]) {
        return (action == @selector(deleteMessage:) || action == @selector(resendMessage:));
    }
    else {
        return action == @selector(deleteMessage:);
    }
    return NO;
}*/
- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
}
#pragma mark - tableView 捲動到底部
- (void)scrollToBottomAnimated:(BOOL)animated {
    NSInteger section = [self.TableView numberOfSections];
    if (section == 0) return; //防呆
    
    NSInteger rows = [self.TableView numberOfRowsInSection:section - 1];
    if (rows == 0) return; //防呆
    
    [self.TableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rows - 1 inSection:section - 1] atScrollPosition:UITableViewScrollPositionBottom animated:animated];
}
#pragma mark -
#pragma mark 傳/收訊息
-(IBAction)ClickSendButton:(id)sender {
    [self SendMessage:TextViewMessage.text];
}
-(void) SendMessage:(NSString *)message {
    //NSLog(@"ctrl:%@",@"channel/chat");
    //NSLog(@"fid:%@",[[User sharedUser] ACCOUNT]);
    //NSLog(@"tid:%@",[self GetTalkingID]);
    //NSLog(@"mid:%@",[[User sharedUser] generateUUID]);
    //NSLog(@"say:%@",message);
    //NSLog(@"dxid:%@",[[User sharedUser]UDID]);
    
    if ([message length] == 0) return; //空白不處理
    
    [self DismissTypingView];
    
    //貼圖做特殊處理
    if ([[CommonTask sharedInstance] GetChatMessageKindWithString:message] == chatMessageKindSticker) {
        message = [message stringByReplacingOccurrencesOfString:@"http://202.74.126.64/mqtt_img/" withString:@""];
    }
    
    NSMutableDictionary *newMessage = [[NSMutableDictionary alloc] init];
    [newMessage setObject:[[User sharedUser] ACCOUNT] forKey:@"fromId"];
    [newMessage setObject:[self GetTalkingID] forKey:@"toId"];
    [newMessage setObject:[[User sharedUser] generateUUID] forKey:@"messageId"];
    [newMessage setObject:message forKey:@"message"];
    
    NSDateFormatter *_formatter = [[NSDateFormatter alloc] init];
    _formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *stringDate = [_formatter stringFromDate:[NSDate date]];
    [newMessage setObject:stringDate forKey:@"sendDate"];
    
    //設定訊息是群聊
    if (_ChatRoomUserType == ChatRoomUserType_GroupChat) {
        [newMessage setObject:@"1" forKey:@"isGroupMessage"];
    }
    
    DBProcess *_process = [[DBProcess alloc] init];
    [_process SaveMessage:newMessage];
    
    [self AddDataToTableView:newMessage];
    [self scrollToBottomAnimated:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        //[arrayMessages addObject:newMessage];
        
        NSDictionary *payload = @{@"ctrl" : @"channel/chat",
                                  @"fid" : [newMessage objectForKey:@"fromId"],
                                  @"tid" : [newMessage objectForKey:@"toId"],
                                  @"mid" : [newMessage objectForKey:@"messageId"],
                                  @"say" : [newMessage objectForKey:@"message"],
                                  @"dxid" : [[User sharedUser]UDID]
                                  };
        //NSLog(@"payload:%@",payload);
        NSData *data = [NSJSONSerialization dataWithJSONObject:payload options:NSJSONWritingPrettyPrinted error:nil];
        
        NSString *channelTopic = [self channelTopicFromThisChatroom];
        //NSLog(@"channelTopic:%@",channelTopic);
        
        [[MQTTManager sharedInstance] publish:data onTopic:channelTopic Qos:2 retain:NO];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //[self.TableView reloadData];
            //[self scrollToBottomAnimated:YES];
        });
    });
}
-(IBAction)DismissTypingView {
    [TextViewMessage resignFirstResponder];
    TextViewMessage.text = @"";
}
#pragma mark - 表情符號
-(IBAction) ShowFaceView {
    //[self.TextViewMessage becomeFirstResponder];
    
    NSString *nibName = @"ViewChatFace";
    if(!IS_IPHONE_5) {
        nibName = @"ViewChatFace"; //ViewChatFace_iPhone4
    }
    
    //CGFloat screenHeight = self.view.frame.size.height;
    
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    ViewChatFace *myView = [topLevelObjects objectAtIndex:0];
    myView.frame = CGRectMake(0, 64, myView.frame.size.width, myView.frame.size.height);
    myView.delegate = self;

    [self.view insertSubview:myView belowSubview:self.ViewTypingText];
    [myView UpdateUI];
    
    NSLog(@"myView.frame:%@",NSStringFromCGRect(myView.frame));
    
}
-(void) ViewChatFaceSelectedFace:(NSString *)selectedFace { //選到的表情符號
    //NSLog(@"selectedFace:%@",selectedFace);
    self.TextViewMessage.text = [self.TextViewMessage.text stringByAppendingString:selectedFace];
}
-(void) ViewChatFaceSelectedImage:(NSString *)urlString { //選擇到的圖片網址
    //NSLog(@"選擇到的圖片網址:%@",urlString);
    [self SendMessage:urlString];
}
#pragma mark - 點擊訊息的照片
- (void)ClickImageAtCellWithUrl:(NSNotification *)notification {
    NSString *urlString = [notification object];
    //NSLog(@"ClickImageAtCellWithUrl:%@",urlString);
    [self performSegueWithIdentifier:@"GotoImageViewerVC" sender:urlString];
}
#pragma mark - 照片相關
-(IBAction)ChooseImageSource:(id)sender {
    myPicker = [[MyImagePicker alloc] init];
    myPicker.SourceVC = self;
    myPicker.delegate = self;
    [myPicker UpdateUI];
}
-(void) MyImagePickerFinishedWithImage:(UIImage *)image {
    [self UpdateImageWithImage:image];
}
#pragma mark - API-上傳照片
-(void) UpdateImageWithImage:(UIImage *)image {
    [[VariableStore sharedInstance] ShowHUDInView:self.view Text:@"上傳中"];
    
    //產生隨機變數
    [formatter setDateFormat:@"YYYYMMddHHmmss"];
    NSString *randonDate = [formatter stringFromDate:[NSDate date]];
    int x = arc4random() % 1000;
    NSString *stringRandom = [NSString stringWithFormat:@"%@%d",randonDate,x];
    
    api_pic_upload_pic *api = [[api_pic_upload_pic alloc] init];
    api.delegate = self;
    [[VariableStore sharedInstance].arrayHTTPRequests addObject:api];
    [api UploadPicWithGuid:stringRandom Pic:image];
}
-(void) api_pic_upload_picComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    [[VariableStore sharedInstance] HideHUDInView:self.view];
    
    if (success) {
        //上傳圖片成功，把網址透過MQTT傳出去
        NSString *urlString = [data objectForKey:@"pic_url"];
        [self SendMessage:urlString];
    }
    else {
    }
}
#pragma mark - 錄音
-(IBAction) ShowRecordingView {
    NSString *nibName = @"ViewChatVoice";
    if(!IS_IPHONE_5) {
        nibName = @"ViewChatVoice_iPhone4";
    }
    
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    ViewChatFace *myView = [topLevelObjects objectAtIndex:0];
    myView.delegate = self;
    
    [self.view addSubview:myView];
    [myView UpdateUI];
    
    //NSLog(@"myView.frame:%@",NSStringFromCGRect(myView.frame));
}
-(void) ViewChatVoiceRecordingSuccessWithUrl:(NSString *)urlString {
    NSLog(@"錄音成功，網址：%@",urlString);
    [self SendMessage:urlString];
}
#pragma mark - Keyboard notifications
- (void)handleWillShowKeyboard:(NSNotification *)notification {
    [self keyboardWillShowHide:notification WiiHide:NO];
}
- (void)handleWillHideKeyboard:(NSNotification *)notification {
    [self keyboardWillShowHide:notification WiiHide:YES];
}
- (void)keyboardWillShowHide:(NSNotification *)notification WiiHide:(BOOL)willHide {
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    CGFloat yOffset = IS_IPHONE_5 ? 216 : 216-88;
    if (!willHide) {
        _ButtonDismissTypingView.hidden = NO;
        [self.TableView setContentSize:CGSizeMake(320, self.TableView.contentSize.height + yOffset)];
        CGPoint bottomOffset = CGPointMake(0, self.TableView.contentSize.height - self.TableView.bounds.size.height);
        [self.TableView setContentOffset:bottomOffset animated:NO];
    }
    else {
        _ButtonDismissTypingView.hidden = YES;
        [self.TableView setContentSize:CGSizeMake(320, self.TableView.contentSize.height - yOffset)];
    }
    
    [UIView animateWithDuration:duration
                          delay:0.0f
                        options:[self animationOptionsForCurve:curve]
                     animations:^{
                         CGFloat keyboardY = [self.view convertRect:keyboardRect fromView:nil].origin.y;
                         
                         CGRect inputViewFrame = ViewTypingText.frame;
                         CGFloat inputViewFrameY = keyboardY - inputViewFrame.size.height;
                         
                         // for ipad modal form presentations
                         CGFloat messageViewFrameBottom = self.view.frame.size.height - INPUT_HEIGHT;
                         if(inputViewFrameY > messageViewFrameBottom)
                             inputViewFrameY = messageViewFrameBottom;
                         
                         CGFloat offset = 0;
                         if (willHide) offset = 0; //原-48 因為廣告的關係
                         ViewTypingText.frame = CGRectMake(inputViewFrame.origin.x,inputViewFrameY + offset,inputViewFrame.size.width,inputViewFrame.size.height);
                     }
                     completion:^(BOOL finished) {
                     }];
}
- (UIViewAnimationOptions)animationOptionsForCurve:(UIViewAnimationCurve)curve {
    switch (curve) {
        case UIViewAnimationCurveEaseInOut:
            return UIViewAnimationOptionCurveEaseInOut;
            break;
        case UIViewAnimationCurveEaseIn:
            return UIViewAnimationOptionCurveEaseIn;
            break;
        case UIViewAnimationCurveEaseOut:
            return UIViewAnimationOptionCurveEaseOut;
            break;
        case UIViewAnimationCurveLinear:
            return UIViewAnimationOptionCurveLinear;
            break;
    }
    
    return kNilOptions;
}
#pragma mark - 這間聊天室的 Channel Topic
- (NSString *)channelTopicFromThisChatroom { //取得這間聊天室的 Channel Topic
    NSString *account = [[User sharedUser] ACCOUNT];
    NSLog(@"account:%@",account);
    
    NSString *topic = nil;
    if (_ChatRoomUserType == ChatRoomUserType_SingleChat) { //個聊
        NSArray *members = @[account, [self GetTalkingID]];
        //NSLog(@"members:%@",members);
        
        NSArray *sortMembers = [members sortedArrayUsingSelector:@selector(compare:)];
        //NSLog(@"sortMembers:%@",sortMembers);
        
        //NSLog(@"topic:%@",[MQTTManager sharedInstance].topics);
        
        topic = [MQTTManager sharedInstance].topics[sortMembers];
    }
    else { //群聊
        for(id talkingMembers in [[MQTTManager sharedInstance].topics allKeys]) {
            //NSLog(@"talkingMembers:%@",talkingMembers);
            if ([talkingMembers isKindOfClass:[NSArray class]]) {
                for (NSString *member in talkingMembers) {
                    if ([member isEqualToString:_gid]) {
                        topic = [[MQTTManager sharedInstance].topics objectForKey:talkingMembers];
                    }
                }
            }
        }
    }
    
    NSLog(@"這間聊天室的 Channel Topic:%@",topic);
    return topic;
}
#pragma mark - 換頁
-(void) Back {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"GotoImageViewerVC"]) {
        NSString *urlString = sender;
        ImageViewerVC *vc = segue.destinationViewController;
        vc.urlString = urlString;
    }
}
#pragma mark - 雜項
-(NSString *) GetTalkingID { //取得聊天對象ID
    NSString *toID;
    if (_ChatRoomUserType == ChatRoomUserType_SingleChat) { //個聊
        toID = [chatTarget objectForKey:@"account"];
    }
    else { //群聊
        toID = _gid;
    }
    return toID;
}
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self Back]; //有錯誤看到訊息後返回
}
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
    NSLog(@"MQTTChatRoom dealloc");
}
@end
