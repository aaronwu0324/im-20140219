

#import "api_mqtt_create.h"
#import "JSON.h"

@implementation api_mqtt_create
@synthesize delegate;
#pragma mark -
#pragma mark Delegate
-(void) api_mqtt_createComplete:(BOOL)success Message:(NSString *)message Data:(NSDictionary *)data {
    if ([[self delegate] respondsToSelector:@selector(api_mqtt_createComplete:Message:Data:)]) {
        [[self delegate] api_mqtt_createComplete:success Message:message Data:data];
    }
}
#pragma mark -
#pragma mark ASIHTTPRequest
-(void) CreateMQTTChannelFromMYID:(NSString *)MYID HERID:(NSString *)HERID APPID:(NSString *)APPID UDID:(NSString *)UDID {
    NSString *urlString = [NSString stringWithFormat:@"%@/mqtt/create",kMQTTGateway];
    NSLog(@"api_mqtt/create url:%@",urlString);
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIRequest = [[ASIFormDataRequest alloc] initWithURL:url];
    
    [ASIRequest addPostValue:MYID forKey:@"MYID"];
    [ASIRequest addPostValue:HERID forKey:@"HERID"];
    [ASIRequest addPostValue:APPID forKey:@"APPID"];
    [ASIRequest addPostValue:UDID forKey:@"UDID"];
    
    NSLog(@"MYID:%@",MYID);
    NSLog(@"HERID:%@",HERID);
    NSLog(@"APPID:%@",APPID);
    NSLog(@"UDID:%@",UDID);
    
    [ASIRequest setTimeOutSeconds:kURLTimeoutSecond];
    [ASIRequest setNumberOfTimesToRetryOnTimeout:kURLRetryOnTimeout];
    [ASIRequest setDelegate:self];
    [ASIRequest startAsynchronous];
}
- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    NSLog(@"api_mqtt/create Finished:%@",responseString);
    
    //回傳資料格式
    /*stdClass Object
    (
     [error_code] => 200
     [error_msg] =>
     [channel] => channel/482908d30aafeb3ef7c85549f937af85/data
     )*/
    
    //開始拆解內容
    NSRange range_start = [responseString rangeOfString:@"[error_code] => "];
    //NSLog(@"start:%d %d",range_start.location,range_start.length);
    NSRange range_end = [responseString rangeOfString:@"[error_msg]"];
    //NSLog(@"end:%d %d",range_end.location,range_start.length);
    
    NSUInteger StartPoint = range_start.location+range_start.length;
    NSUInteger ContendLength = range_end.location - StartPoint;
    NSString *result_string = [[responseString substringWithRange:NSMakeRange(StartPoint, ContendLength)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"api_mqtt/create:(%@)",result_string);
    
    if ([result_string isEqualToString:@"200"]) { //200成功
        //取出channel
        NSRange range_start = [responseString rangeOfString:@"[channel] => "];
        //NSLog(@"start:%d %d",range_start.location,range_start.length);
        NSRange range_end = [responseString rangeOfString:@")"];
        //NSLog(@"end:%d %d",range_end.location,range_start.length);
        
        NSUInteger StartPoint = range_start.location+range_start.length;
        NSUInteger ContendLength = range_end.location - StartPoint;
        NSString *channel = [[responseString substringWithRange:NSMakeRange(StartPoint, ContendLength)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"channel:%@",channel);
        
        [self api_mqtt_createComplete:YES Message:channel Data:nil];
    }
    else { //失敗
        //2013.09.16
        //Luke說就是當你要跟一開始和某個人聊天的那一支API，
        //後台會傳值，他說200以外的都是沒有dxid的
        [self api_mqtt_createComplete:NO Message:@"聊天功能失敗，請重試一次" Data:nil];
    }
    
    /*NSError *error = nil;
	SBJSON *parser = [SBJSON new];
	
    //原始資料
	NSDictionary *result = (NSDictionary *) [parser objectWithString:responseString error:&error];
    NSLog(@"api_mqtt/create result:%@",result);
    
    while (error) {
        NSLog(@"json error:%@",[error localizedDescription]);
        error = [[error userInfo] objectForKey:NSUnderlyingErrorKey];
        NSLog(@"Error trace: %@", parser.errorTrace);
        [self api_mqtt_createComplete:NO Message:kMessageNetworkFail Data:nil];
        [self performSelector:@selector(Cancle)];
        return;
    }
    
    NSString *sys_msg = [result objectForKey:@"sys_msg"];
    NSString *sys_code = [result objectForKey:@"sys_code"];
    NSLog(@"api_mqtt/create:(%@)%@",sys_code,sys_msg);
    
    if ([sys_code isEqualToString:@"200"]) { //200成功
        NSDictionary *data = result;
        [self api_mqtt_createComplete:YES Message:sys_msg Data:data];
    }
    else { //失敗
        //2013.09.16
        //Luke說就是當你要跟一開始和某個人聊天的那一支API，
        //後台會傳值，他說200以外的都是沒有dxid的
        [self api_mqtt_createComplete:NO Message:@"此帳號無聊天功能" Data:nil];
    }*/
    
    [self performSelector:@selector(Cancle)];
}
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"requestFailed error:%@",error);
    [self api_mqtt_createComplete:NO Message:kMessageNetworkFail Data:nil];
    [self performSelector:@selector(Cancle)];
}
#pragma mark -
#pragma mark 雜項
-(void) Cancle {
    // Cancels an asynchronous request
    [ASIRequest cancel];
    
    // Cancels an asynchronous request, clearing all delegates and blocks first
    [ASIRequest clearDelegatesAndCancel];
    
    //移除物件
    [[VariableStore sharedInstance].arrayHTTPRequests removeObject:self];
}
- (void)dealloc {
    ASIRequest = nil;
    delegate = nil;
}
@end