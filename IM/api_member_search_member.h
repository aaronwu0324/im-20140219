
@protocol api_member_search_memberDelegate <NSObject>
@optional
-(void) api_member_search_memberComplete:(BOOL)success Message:(NSString *)message Data:(NSArray *)data;
@end

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface api_member_search_member : NSObject <ASIHTTPRequestDelegate> {
    ASIHTTPRequest *ASIRequest;
    __weak id <api_member_search_memberDelegate> delegate;
}
@property (weak) id delegate;
-(void) SearchMemberWithName:(NSString *)member_name;
@end
