

#import <UIKit/UIKit.h>
#import "rootViewController.h"
#import "api_group_list_group.h"
#import "api_group_create_group.h"
#import "api_group_list_my_group.h"
#import "api_group_list_in_group.h"

@interface GroupChat : rootViewController <api_group_list_groupDelegate,UIAlertViewDelegate,api_group_create_groupDelegate,api_group_list_my_groupDelegate,api_group_list_in_groupDelegate>
-(void) GetGroupList;
@end
