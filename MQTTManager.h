

#import "SPMQTTClient.h"
#import "api_mqtt_member_dxid.h"
//#import "api_jampush_get_free_push.h"

/**
 MQTT 管理器
 */
//@interface MQTTManager : NSObject<SPMQTTClientDelegate,api_jampush_get_free_pushDelegate>
@interface MQTTManager : NSObject<SPMQTTClientDelegate,api_mqtt_member_dxidDelegate>

///---------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------

/**
 MQTT 訂閱的頻道
 */
@property (nonatomic, strong) NSMutableDictionary *topics;

///---------------------------------------------------------------------
/// @name Class Methods
///---------------------------------------------------------------------

/**
 @return Singleton Instance type MQTTManager
 */
+ (instancetype)sharedInstance;

/**
 移除所有 Topic
 */
- (void)unsubscribeAllTopic;

/**
 開始執行, 會先 Call API 取得 MQTT所需 IP, 取得後再 Connect MQTT, 如果未取得, 會一直重試
 */
- (void)start;

/**
 停止執行, 會停止所有 MQTT 活動, 並 release MQTT Client
 */
- (void)stop;

/**
 MQTT 傳送訊息, MQTT 相關協定請自行閱讀文件 http://mqtt.org
 
 @param payload 訊息內容
 @param topic 訊息頻道
 @param qos Qos 類型
 @param retain retain 類型
 */
- (void)publish:(NSData *)payload onTopic:(NSString *)topic Qos:(NSUInteger)qos retain:(BOOL)retain;

-(void) GetJamPushWithPid:(NSString *)pid;
-(void) GetFreePush:(NSString *)pid;
@end
