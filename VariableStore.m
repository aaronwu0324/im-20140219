#import "VariableStore.h"

@implementation VariableStore

+ (VariableStore *)sharedInstance
{
    // the instance of this class is stored here
    static VariableStore *myInstance = nil;
	
    // check to see if an instance already exists
    if (nil == myInstance) {
        myInstance  = [[[self class] alloc] init];
        // initialize variables here
	}
    // return the instance of this class
    return myInstance;
}
#pragma mark -
#pragma mark 讀取設定與初始變數
-(void) ReadSetting {

}
-(void) initVariables {
    self.arrayHTTPRequests = [[NSMutableArray alloc] init];
    self.AnimationClass = [[MyAnimation alloc] init];
    [self initFMDB:@"data.db"];
}
-(void) initFMDB:(NSString *)filename { //初始化FMDB
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:filename];
    
	success = [fileManager fileExistsAtPath:dbPath];
    if (!success) { //檔案不存在,複製一份到cache目錄
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        if (!success) {
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        }
    }
    
    self.FMDB = [[FMDatabase alloc] initWithPath:dbPath];
}
-(BOOL) isNetworkAvailable {
    BOOL isNetworkAvailable = YES;
    
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    if (netStatus == NotReachable) {
        NSLog(@"網路連線:NO");
        isNetworkAvailable = NO;
    } else {
        NSLog(@"網路連線:YES");
    }
    
    return isNetworkAvailable;
}
#pragma mark -
#pragma mark HUD
-(void) ShowHUDInView:(UIView *)myView Text:(NSString *)text {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    self.hud = [MBProgressHUD showHUDAddedTo:myView animated:YES];
    _hud.labelText = text;
}
-(void) HideHUDInView:(UIView *)myView {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [MBProgressHUD hideHUDForView:myView animated:YES];
}
#pragma mark -
#pragma mark 雜項
- (UIViewController*)viewController:(UIView *)myView { //回傳現在UIView所在的UIViewController
    for (UIView* next = [myView superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}
- (void)dealloc {
}
@end
