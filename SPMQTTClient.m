

#import "SPMQTTClient.h"

@interface SPMQTTClient(/*Private*/)

;
#pragma mark - Private Properties

//mosquitto
@property (nonatomic, assign) struct mosquitto *mosq;

//timer 每 .1 秒觸發一次
@property (nonatomic, strong) NSTimer *timer;

//keepAlive
@property (nonatomic, assign) NSUInteger keepAlive;

@end


@implementation SPMQTTClient

//mosquitto callback
static void on_connect(struct mosquitto *mosq, void *obj, int rc)
{
    SPMQTTClient *client = (__bridge SPMQTTClient *)obj;
    [client.delegate mqttDidConnect:client];
}

static void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
    SPMQTTClient *client = (__bridge SPMQTTClient *)obj;
    NSData *data = [[[NSString alloc]initWithBytes:message->payload
                                            length:message->payloadlen
                                          encoding:NSUTF8StringEncoding]
                    dataUsingEncoding:NSUTF8StringEncoding];
    [client.delegate mqtt:client didReciveMessage:data onTopic:[NSString stringWithUTF8String:message->topic]];
}

;
#pragma mark - LifeCycle

- (void)dealloc {
    [self preRelease];
    //DLog(@"%s", __PRETTY_FUNCTION__);
}
- (id)initWithClientId:(NSString *)clinetId {
    self = [super init];
    if(self)
    {
        _mosq = mosquitto_new([clinetId cStringUsingEncoding:NSUTF8StringEncoding], NO, (__bridge void *)(self));
        mosquitto_connect_callback_set(_mosq, on_connect);
        mosquitto_message_callback_set(_mosq, on_message);
    }
    
    return self;
}
- (id)initWithClientId:(NSString *)clinetId
             keepAlive:(NSUInteger)keepAlive
           willMessage:(NSString *)message
             willTopic:(NSString *)topic
                   Qos:(NSUInteger)qos
          cleanSession:(BOOL)cleanSession
                retain:(BOOL)retain
{
    self = [super init];
    if(self)
    {
        self.keepAlive = keepAlive;
        
        self.mosq = mosquitto_new([clinetId cStringUsingEncoding:NSUTF8StringEncoding], false, (__bridge  void *)(self));
        mosquitto_connect_callback_set(_mosq, on_connect);
        mosquitto_message_callback_set(_mosq, on_message);
        const char* cstrTopic = [topic cStringUsingEncoding:NSUTF8StringEncoding];
        const uint8_t* cstrPayload = (const uint8_t*)[message cStringUsingEncoding:NSUTF8StringEncoding];
        size_t cstrlen = [message lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
        
        //基本上不會用到 retain, 所以最後一個都設成 false
        mosquitto_will_set(_mosq, cstrTopic, cstrlen, cstrPayload, qos, false);
    }
    
    return self;
}

#pragma mark - 連接 MQTT 主機
- (void)connectHost:(NSString *)host port:(NSUInteger)port
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

        const char *cstrHost = [host cStringUsingEncoding:NSASCIIStringEncoding];

        /* User and Passwd 沒用到
         
        const char *cstrUsername = NULL, *cstrPassword = NULL;

        if (username)
            cstrUsername = [username cStringUsingEncoding:NSUTF8StringEncoding];

        if (password)
            cstrPassword = [password cStringUsingEncoding:NSUTF8StringEncoding];

        // FIXME: check for errors
        mosquitto_username_pw_set(mosq, cstrUsername, cstrPassword);
        */

        if(_keepAlive < 20) self.keepAlive = 20;
        
        int ret = mosquitto_connect(_mosq, cstrHost, port, _keepAlive);

        dispatch_async(dispatch_get_main_queue(), ^{
            
            //連線失敗
            if ( ret != 0 )
            {
                [_delegate mqttConnectError:self];
            }
            else
            {
                self.timer = [NSTimer scheduledTimerWithTimeInterval:.1f
                                                              target:self
                                                            selector:@selector(loop:)
                                                            userInfo:nil
                                                             repeats:YES];
            }
        });

    });
}

#pragma mark -  傳送訊息
- (void)publish:(NSString *)payload onTopic:(NSString *)topic Qos:(NSUInteger)qos retain:(BOOL)retain {
    const char* cstrTopic = [topic cStringUsingEncoding:NSUTF8StringEncoding];
    const uint8_t* cstrPayload = (const uint8_t*)[payload cStringUsingEncoding:NSUTF8StringEncoding];
    size_t cstrlen = [payload lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    
    //基本上不會用到 retain, 所以最後一個都設成 false
    mosquitto_publish(_mosq, NULL, cstrTopic, cstrlen, cstrPayload, qos, false);
}

#pragma mark -  訂閱頻道
- (void)subscribe:(NSString *)topic Qos:(NSUInteger)qos {
    const char* cstrTopic = [topic cStringUsingEncoding:NSUTF8StringEncoding];
    mosquitto_subscribe(_mosq, NULL, cstrTopic, qos);
}
#pragma mark - 取消頻道
- (void)unsubscribe:(NSString *)topic {
    const char* cstrTopic = [topic cStringUsingEncoding:NSUTF8StringEncoding];
    mosquitto_unsubscribe(_mosq, NULL, cstrTopic);
}
#pragma mark - Pre Release
- (void)preRelease; {
    [_timer invalidate];
    self.timer = nil;
    mosquitto_destroy(_mosq);
    self.mosq = NULL;
}

- (void)loop:(NSTimer *)timer
{
    if(mosquitto_loop(_mosq, 1, 1) == MOSQ_ERR_NO_CONN)
    {
        [_delegate mqttConnectError:self];
    }
}
@end
