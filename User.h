

#import <CoreLocation/CoreLocation.h>

/**
 目前使用 App 的使用者物件
 */
@interface User : NSObject

///---------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------

/**
 聊天對象
 */
@property(nonatomic, strong) NSMutableDictionary *chatTarget;

/**
 使用者是否登入
 */
@property (nonatomic, assign, readonly) BOOL isLogin;

///---------------------------------------------------------------------
/// @name Class Methods
///---------------------------------------------------------------------

/**
 返回一個 instance User Object
 
 @retrun 返回目前使用這個 App 的使用者
 */
+ (instancetype)sharedUser;

///---------------------------------------------------------------------
/// @name Public Methods
///---------------------------------------------------------------------

/**
 登入
 */
- (void)login;

/**
 登出
 */
- (void)logout;

/**
 會員資料儲存程序, 相關條件請查閱 Gateway 規格文件
 @param newInfo 新的資訊
 */
- (void)updateInfo:(NSDictionary *)newInfo;

/**
 @return 回傳自定義產生的 UUID
 */
- (NSString *)generateUUID;

/**
 @return 回傳目前手機的 UDID
 */
- (NSString *)UDID;

/**
 @return 回傳 APP Bundle ID
 */
- (NSString *)APPID;

/**
 @return 回傳 MQTT 所需要的 Client ID
 */
- (NSString *)CLIENTID;

/**
 @return 回傳登入的使用者 Account
 */
- (NSString *)ACCOUNT;

/**
 @return 回傳登入的使用者 ID
 */
- (NSString *)MEMBER_ID;

/**
 @return 回傳登入的使用者 中文姓名
 */
- (NSString *)MEMBER_CHINESE_NAME;

/**
 @return 回傳登入的使用者 圖片網址
 */
- (NSString *)MEMBER_AVATAR;

/**
 @return 回傳登入的使用者位置
 */
//- (CLLocation *)location;

@end
