//作用：訊息通知

#import <UIKit/UIKit.h>

@interface NotifyBar : UIWindow

@property (nonatomic, assign) BOOL isShow;

@property (nonatomic, strong) NSArray *exceptionControllers;

+ (NotifyBar *)defaultNotify;

- (void)show;

- (void)showInChatRoom;

- (IBAction)buttonDidClicked:(id)sender;
@end
