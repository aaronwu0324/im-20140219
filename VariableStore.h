
#import "MyDefinition.h"
#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "MyAnimation.h"
#import "User.h"

@interface VariableStore : NSObject {
}
@property (nonatomic,strong) FMDatabase *FMDB;
@property (nonatomic,strong) NSMutableArray *arrayHTTPRequests;
@property (nonatomic,strong) MBProgressHUD *hud;
@property (nonatomic,strong) MyAnimation *AnimationClass;
@property (nonatomic,strong) NSString *TokenFromApi;
@property (nonatomic,strong) NSString *notifyMemberID;
@property BOOL isInChatRoom; //是否正在聊天室
@property (nonatomic,strong) NSString *currentChatRoomGroupID; //正在聊天室的聊天室ID
@property NSInteger currentFunction;

// message from which our instance is obtained
+ (VariableStore *)sharedInstance;
-(void) ReadSetting;
-(void) initVariables;
-(BOOL) isNetworkAvailable;
-(void) ShowHUDInView:(UIView *)myView Text:(NSString *)text;
-(void) HideHUDInView:(UIView *)myView;
- (UIViewController*)viewController:(UIView *)myView; //回傳現在UIView所在的UIViewController
@end
