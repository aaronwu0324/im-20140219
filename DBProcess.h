

#import <Foundation/Foundation.h>

@interface DBProcess : NSObject
-(void) SaveMessage:(NSMutableDictionary *)dicMessage; //儲存訊息
-(void) SaveGroupMessageReceive:(NSDictionary *)dicMessage; //儲存群組訊息的已收狀態
-(void) SaveGroupMessageRead:(NSDictionary *)dicMessage; //儲存群組訊息的已讀狀態
-(NSString *)GetLastMessageTimestampWithToID:(NSString *)toID; //最後一則訊息的時間，用來詢問後台有沒有離線訊息
-(NSArray *)GetMessagesWithFromMemberID:(NSString *)from_memeber_id ToMemeberID:(NSString *)to_member_id isGroupMessasge:(BOOL)isGroupMessage; //取得聊天記錄
-(NSArray *)GetChatListWithAccount:(NSString *)account; //取得跟所有人聊天的清單
-(BOOL) didMessageExistWithMessageID:(NSString *)message_id; //判斷該訊息是否存在
-(void) SaveDataMyGroup:(NSArray *)data; //儲存我的群組清單資料
-(NSString *)GetToIDWithMessageID:(NSString *)message_id; //用message取得toID，用來判斷是不是群聊的訊息
-(NSArray *)GetReadListWithMessageID:(NSString *)message_id; //用messageID取得已讀取該訊息的人員清單
-(NSString *)GetGroupIDWithGroupName:(NSString *)group_name; //從群組名稱取得群組ID


-(NSMutableDictionary *) GetMessageWithMessageID:(NSString *)message_id; //取得單一訊息
-(BOOL) didUnreadMessageExist; //判斷是否有任何未讀訊息
-(BOOL) didUnreadMessageExistWithMemberID:(NSString *)memeber_id; //判斷該帳號是否有任何未讀訊息
-(void) SetUnreadMessageWithMemberID:(NSString *)memeber_id; //把該帳號的所有訊息設定為已讀
@end
