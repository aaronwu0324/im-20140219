
#import "NotifyBar.h"
#import "AppDelegate.h"
#import "NameList.h"
#import "GroupChat.h"

@implementation NotifyBar

+ (NotifyBar *)defaultNotify {
    static dispatch_once_t once;
    static NotifyBar *sharedInstance = nil;
    
    dispatch_once(&once, ^{
        sharedInstance = [[NotifyBar alloc]initWithFrame:CGRectMake(0, -64, 320, 64)];
    });
    return sharedInstance;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        self.windowLevel = UIWindowLevelStatusBar + 1.0f;
        self.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.750];
        [self setup];
    }
    
    return self;
}

- (void)setup {
    //例外的VC，一般是聊天室不需要接到
    self.exceptionControllers = @[@"ClassmateChatRoom" ,
                                  ];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = self.bounds;
    button.titleEdgeInsets = UIEdgeInsetsMake(20.0f, 0, 0, 0);
    
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"您有聊天訊息 !" forState:UIControlStateNormal];
    
    [button setTitleColor:[UIColor colorWithRed:0.502 green:0.251 blue:0.000 alpha:1.000] forState:UIControlStateHighlighted];

    [button addTarget:self action:@selector(buttonDidClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
}
- (void)showInChatRoom {
    if(_isShow)
        return;
    
    self.isShow = YES;
    
    [self setFrame:CGRectMake(0, -64, 320, 64)];
    self.hidden = NO;
    
    [UIView animateWithDuration:1.0f animations:^{
        [self setFrame:CGRectMake(0, 0, 320, 64)];
    } completion:^(BOOL finished) {
        [self performSelector:@selector(hide) withObject:nil afterDelay:2.0f];
    }];
}
- (void)show {
    if(_isShow)
        return;
    
    if([self exceptionController])
        return;
    
    self.isShow = YES;
    
    [self setFrame:CGRectMake(0, -64, 320, 64)];
    self.hidden = NO;
    
    [UIView animateWithDuration:1.0f animations:^{
        self.alpha = 1.0f;
        [self setFrame:CGRectMake(0, 0, 320, 64)];
    } completion:^(BOOL finished) {
        [self performSelector:@selector(hide) withObject:nil afterDelay:2.0f];
    }];
}

- (void)buttonDidClicked:(id)sender { //按到通知就到聊天室
    //避免 User 重複 click, hidden 起來
    self.hidden = YES;
    
    //取出TabBar所在的Navigation
    AppDelegate *delegate = [[UIApplication sharedApplication]delegate];
    UIViewController *vc = delegate.window.rootViewController;
    NSLog(@"vc:%@",vc);
    
    UITabBarController *tabController = (UITabBarController *)vc.presentedViewController;
    NSLog(@"tabController:%@",tabController);
    
    ///navigation，如果正在跟其他人聊天，就推回
    UINavigationController *navController = (UINavigationController *)[tabController selectedViewController];
    NSArray *arrayVC = navController.viewControllers;
    UIViewController *currentController = [arrayVC objectAtIndex:[arrayVC count]-1];
    NSLog(@"currentController:%@",currentController);
    if ([currentController isKindOfClass:NSClassFromString(@"MQTTChatRoom")]) {
        [navController popToRootViewControllerAnimated:NO];
    }
    
    //判斷是不是群組訊息（toId是數字）
    NSString *tid = [VariableStore sharedInstance].notifyMemberID;
    BOOL isGroupMessage = [[NSScanner scannerWithString:tid] scanInt:nil];
    if (isGroupMessage) { //群聊，把群組id存起來
        //前往群聊navigation
        tabController.selectedIndex = 2;
        
        //取得名單vc
        UINavigationController *navGroupChat = (UINavigationController *)[tabController selectedViewController];
        NSLog(@"navGroupChat:%@",navGroupChat);
        GroupChat *GroupChatVC = (GroupChat *)[navGroupChat topViewController];
        NSLog(@"GroupChatVC:%@",GroupChatVC);
        
        //在抓一次群組清單，抓完之後會自動跟該群組聊天
        [GroupChatVC GetGroupList];
    }
    else { //個聊，存訊息來源者的account
        //前往名單navigation
        tabController.selectedIndex = 0;
        
        //取得名單vc
        UINavigationController *navNameList = (UINavigationController *)[tabController selectedViewController];
        NSLog(@"navNameList:%@",navNameList);
        NameList *nameListVC = (NameList *)[navNameList topViewController];
        NSLog(@"nameListVC:%@",nameListVC);
        
        //在抓一次聯絡人清單，抓完之後會自動跟該人聊天
        [nameListVC GetContact];
    }
}
- (void)hide {
    [UIView animateWithDuration:1.0f animations:^{
        [self setFrame:CGRectMake(0, -64, 320, 44)];
    } completion:^(BOOL finished) {
        self.isShow = NO;
        self.hidden = YES;
        
        //歸0
        [VariableStore sharedInstance].notifyMemberID = @"";
    }];
}

- (BOOL)exceptionController
{
    BOOL result = NO;
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    
    id rootViewController = delegate.window.rootViewController;
    
    if([rootViewController isKindOfClass:[UINavigationController class]]) {
        rootViewController = [[(UINavigationController *)rootViewController viewControllers]lastObject];
    }
    
    NSString *className = NSStringFromClass([rootViewController class]);
    
    result = [_exceptionControllers containsObject:className];
    
    return result;
}
@end
