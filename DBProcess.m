

#import "DBProcess.h"
#import "FMDatabaseQueue.h"
#import "User.h"

@implementation DBProcess

#pragma mark -
#pragma mark 訊息相關
-(void) SaveMessage:(NSMutableDictionary *)dicMessage { //儲存訊息
    //[[VariableStore sharedInstance].FMDB open];
    
    //null處理
    NSString *fromId = [dicMessage objectForKey:@"fromId"];
    if (!fromId) {
        fromId = @"";
    }
    NSString *isFail = [dicMessage objectForKey:@"isFail"];
    if (!isFail) {
        isFail = @"";
    }
    NSString *isRead = [dicMessage objectForKey:@"isRead"];
    if (!isRead) {
        isRead = @"";
    }
    NSString *isRecv = [dicMessage objectForKey:@"isRecv"];
    if (!isRecv) {
        isRecv = @"";
    }
    NSString *isSend = [dicMessage objectForKey:@"isSend"];
    if (!isSend) {
        isSend = @"";
    }
    NSString *message = [dicMessage objectForKey:@"message"];
    if (!message) {
        message = @"";
    }
    NSString *messageId = [dicMessage objectForKey:@"messageId"];
    if (!messageId) {
        messageId = @"";
    }
    NSString *readDate = [dicMessage objectForKey:@"readDate"];
    if (!readDate) {
        readDate = @"";
    }
    NSString *recvDate = [dicMessage objectForKey:@"recvDate"];
    if (!recvDate) {
        recvDate = @"";
    }
    NSString *sendDate = [dicMessage objectForKey:@"sendDate"];
    if (!sendDate) {
        sendDate = @"";
    }
    NSString *toId = [dicMessage objectForKey:@"toId"];
    if (!toId) {
        toId = @"";
    }
    NSString *isGroupMessage = [dicMessage objectForKey:@"isGroupMessage"];
    if (!isGroupMessage) {
        isGroupMessage = @"";
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:@"data.db"];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSString *sqlString = [NSString stringWithFormat:@"INSERT OR REPLACE INTO message (fromId,isFail,isRead,isRecv,isSend,message,messageId,readDate,recvDate,sendDate,toId,isGroupMessage,MyAccount) VALUES('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                               fromId,
                               isFail,
                               isRead,
                               isRecv,
                               isSend,
                               message,
                               messageId,
                               readDate,
                               recvDate,
                               sendDate,
                               toId,
                               isGroupMessage,
                               MyAccount];
        //NSLog(@"SaveMessage sqlString:%@",sqlString);
        [db executeUpdate:sqlString];
    }];
}
-(void) SaveGroupMessageReceive:(NSDictionary *)dicMessage { //儲存群組訊息的已收狀態
    NSLog(@"dicMessage:%@",dicMessage);
    
    NSString *account = [dicMessage objectForKey:@"fid"];
    NSString *messageId = [dicMessage objectForKey:@"mid"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:@"data.db"];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM GroupMessageReadList WHERE messageId = '%@'",messageId];
    
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    if ([s next]) {
        NSInteger count = [s intForColumnIndex:0];
        if (count > 0) { //有資料，做update
            sqlString = [NSString stringWithFormat:@"UPDATE GroupMessageReadList SET isRecv = '1' WHERE messageId = '%@'",messageId];
        }
        else { //沒資料更新
            sqlString = [NSString stringWithFormat:@"INSERT OR REPLACE INTO GroupMessageReadList (account,messageId,MyAccount,isRecv) VALUES('%@','%@','%@','1')",
                         account,
                         messageId,
                         MyAccount];
        }
        
        NSLog(@"SaveGroupMessageReceive sqlString:%@",sqlString);
        [[VariableStore sharedInstance].FMDB executeUpdate:sqlString];
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    /*FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM GroupMessageReadList WHERE messageId = '%@'",messageId];
        
        FMResultSet *s = [db executeQuery:sqlString];
        if ([s next]) {
            NSInteger count = [s intForColumnIndex:0];
            if (count > 0) { //有資料，做update
                sqlString = [NSString stringWithFormat:@"UPDATE GroupMessageReadList SET isRecv = '1' WHERE messageId = '%@'",messageId];
            }
            else { //沒資料更新
                sqlString = [NSString stringWithFormat:@"INSERT OR REPLACE INTO GroupMessageReadList (account,messageId,MyAccount,isRecv) VALUES('%@','%@','%@','1')",
                             account,
                             messageId,
                             MyAccount];
            }
            
            NSLog(@"SaveGroupMessageReceive sqlString:%@",sqlString);
            [db executeUpdate:sqlString];
        }
    }];*/
}
-(void) SaveGroupMessageRead:(NSDictionary *)dicMessage { //儲存群組訊息的已讀狀態
    NSLog(@"dicMessage:%@",dicMessage);
    
    NSString *account = [dicMessage objectForKey:@"fid"];
    NSString *messageId = [dicMessage objectForKey:@"mid"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:@"data.db"];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM GroupMessageReadList WHERE messageId = '%@'",messageId];
    
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    if ([s next]) {
        NSInteger count = [s intForColumnIndex:0];
        if (count > 0) { //有資料，做update
            sqlString = [NSString stringWithFormat:@"UPDATE GroupMessageReadList SET isRecv = '1',isRead = '1' WHERE messageId = '%@'",messageId];
        }
        else { //沒資料更新
            sqlString = [NSString stringWithFormat:@"INSERT OR REPLACE INTO GroupMessageReadList (account,messageId,MyAccount,isRead,isRecv) VALUES('%@','%@','%@','1','1')",
                         account,
                         messageId,
                         MyAccount];
        }
        
        NSLog(@"SaveGroupMessageRead sqlString:%@",sqlString);
        [[VariableStore sharedInstance].FMDB executeUpdate:sqlString];
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    /*FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM GroupMessageReadList WHERE messageId = '%@'",messageId];
        
        FMResultSet *s = [db executeQuery:sqlString];
        if ([s next]) {
            NSInteger count = [s intForColumnIndex:0];
            if (count > 0) { //有資料，做update
                sqlString = [NSString stringWithFormat:@"UPDATE GroupMessageReadList SET isRecv = '1',isRead = '1' WHERE messageId = '%@'",messageId];
            }
            else { //沒資料更新
                sqlString = [NSString stringWithFormat:@"INSERT OR REPLACE INTO GroupMessageReadList (account,messageId,MyAccount,isRead,isRecv) VALUES('%@','%@','%@','1','1')",
                             account,
                             messageId,
                             MyAccount];
            }
            
            NSLog(@"SaveGroupMessageRead sqlString:%@",sqlString);
            [db executeUpdate:sqlString];
        }
    }];*/
}
-(NSString *)GetLastMessageTimestampWithToID:(NSString *)toID { //最後一則訊息的時間，用來詢問後台有沒有離線訊息
    NSString *timestamp = @"";
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM message WHERE toId = '%@' AND MyAccount = '%@' ORDER BY sendDate DESC LIMIT 1",toID,MyAccount];
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    if ([s next]) {
        timestamp = [s stringForColumn:@"sendDate"];
    }
    
    [[VariableStore sharedInstance].FMDB close];
    return timestamp;
}
-(NSArray *) GetMessagesWithFromMemberID:(NSString *)from_memeber_id ToMemeberID:(NSString *)to_member_id isGroupMessasge:(BOOL)isGroupMessage { //取得聊天記錄
    NSMutableArray *arrayToReturn = [[NSMutableArray alloc] init];
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    NSString *sqlString = @"";
    if (isGroupMessage) {
        sqlString = [NSString stringWithFormat:@"SELECT * FROM message WHERE toId = '%@' AND MyAccount = '%@' ORDER BY sendDate DESC",to_member_id,MyAccount];
    }
    else {
        sqlString = [NSString stringWithFormat:@"SELECT * FROM message WHERE (fromId = '%@' AND toId = '%@' AND MyAccount = '%@') OR (fromId = '%@' AND toId = '%@' AND MyAccount = '%@') ORDER BY sendDate DESC",to_member_id,from_memeber_id,MyAccount,from_memeber_id,to_member_id,MyAccount];
    }
    NSLog(@"GetMessagesWithFromMemberID sqlString:%@",sqlString);
    
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    
    while ([s next]) {
        NSString *fromId = [s stringForColumn:@"fromId"];
        NSString *isFail = [s stringForColumn:@"isFail"];
        NSString *isRead = [s stringForColumn:@"isRead"];
        NSString *isRecv = [s stringForColumn:@"isRecv"];
        NSString *isSend = [s stringForColumn:@"isSend"];
        NSString *message = [s stringForColumn:@"message"];
        NSString *messageId = [s stringForColumn:@"messageId"];
        NSString *readDate = [s stringForColumn:@"readDate"];
        NSString *recvDate = [s stringForColumn:@"recvDate"];
        NSString *sendDate = [s stringForColumn:@"sendDate"];
        NSString *toId = [s stringForColumn:@"toId"];
        
        NSMutableDictionary *_dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     fromId,@"fromId",
                                     isFail,@"isFail",
                                     isRead,@"isRead",
                                     isRecv,@"isRecv",
                                     isSend,@"isSend",
                                     message,@"message",
                                     messageId,@"messageId",
                                     readDate,@"readDate",
                                     recvDate,@"recvDate",
                                     sendDate,@"sendDate",
                                     toId,@"toId",
                                     nil];
        [arrayToReturn addObject:_dic];
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    return arrayToReturn;
}
-(NSArray *)GetChatListWithAccount:(NSString *)account { //取得跟所有人聊天的清單
    NSMutableArray *arrayToReturn = [[NSMutableArray alloc] init];
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    //個聊
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM message WHERE toId = '%@' AND MyAccount = '%@' GROUP BY fromId",account,MyAccount];
    NSLog(@"GetChatListWithAccount sqlString:%@",sqlString);
    
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    
    while ([s next]) {
        NSString *fromId = [s stringForColumn:@"fromId"];
        NSString *isFail = [s stringForColumn:@"isFail"];
        NSString *isRead = [s stringForColumn:@"isRead"];
        NSString *isRecv = [s stringForColumn:@"isRecv"];
        NSString *isSend = [s stringForColumn:@"isSend"];
        NSString *message = [s stringForColumn:@"message"];
        NSString *messageId = [s stringForColumn:@"messageId"];
        NSString *readDate = [s stringForColumn:@"readDate"];
        NSString *recvDate = [s stringForColumn:@"recvDate"];
        NSString *sendDate = [s stringForColumn:@"sendDate"];
        NSString *toId = [s stringForColumn:@"toId"];
        
        NSMutableDictionary *_dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     fromId,@"fromId",
                                     isFail,@"isFail",
                                     isRead,@"isRead",
                                     isRecv,@"isRecv",
                                     isSend,@"isSend",
                                     message,@"message",
                                     messageId,@"messageId",
                                     readDate,@"readDate",
                                     recvDate,@"recvDate",
                                     sendDate,@"sendDate",
                                     toId,@"toId",
                                     nil];
        [arrayToReturn addObject:_dic];
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    return arrayToReturn;
}
-(BOOL) didMessageExistWithMessageID:(NSString *)message_id { //判斷該訊息是否存在
    BOOL didMessageExist = NO;
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM message WHERE messageId = '%@' AND MyAccount = '%@'",message_id,MyAccount];
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    if ([s next]) {
        NSInteger count = [s intForColumnIndex:0];
        if (count > 0) {
            didMessageExist = YES;
        }
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    return didMessageExist;
}
-(NSString *)GetToIDWithMessageID:(NSString *)message_id { //用message取得toID，用來判斷是不是群聊的訊息
    NSString *toID = @"";
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM message WHERE messageId = '%@' AND MyAccount = '%@' GROUP BY messageId",message_id,MyAccount];
    
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    if ([s next]) {
        toID = [s stringForColumn:@"toId"];
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    return toID;
}
-(NSArray *)GetReadListWithMessageID:(NSString *)message_id { //用messageID取得已讀取該訊息的人員清單
    NSMutableArray *arrayToReturn = [[NSMutableArray alloc] init];
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM GroupMessageReadList WHERE messageId = '%@' AND MyAccount = '%@'",message_id,MyAccount];
    NSLog(@"GetReadListWithMessageID sqlString:%@",sqlString);
    
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    
    while ([s next]) {
        NSDictionary *_dic = [NSDictionary dictionaryWithObjectsAndKeys:
                              [s stringForColumn:@"account"],@"account",
                              [s stringForColumn:@"isRead"],@"isRead",
                              [s stringForColumn:@"isRecv"],@"isRecv",
                              nil];
        
        [arrayToReturn addObject:_dic];
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    return arrayToReturn;
}
-(NSString *)GetGroupIDWithGroupName:(NSString *)group_name { //從群組名稱取得群組ID
    NSString *gid = @"";
    
    NSMutableArray *arrayGid = [[NSMutableArray alloc] init];
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT gid FROM my_group WHERE group_name = '%@' AND MyAccount = '%@'",group_name,MyAccount];
    NSLog(@"GetGroupIDWithGroupName sqlString:%@",sqlString);
    
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    
    while ([s next]) {
        NSString *account = [s stringForColumn:@"gid"];
        
        [arrayGid addObject:account];
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    if ([arrayGid count] > 2) { //有同名的群組
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"群組名稱重複" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else if ([arrayGid count] == 0) { //找不到正確的群組
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"找不到正確的群組" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else { //正確
        gid = [arrayGid objectAtIndex:0];
    }
    
    return gid;
}
#pragma mark - 下載的資料儲存起來
-(void) SaveDataMyGroup:(NSArray *)data { //儲存我的群組清單資料
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *MyAccount = [[User sharedUser] ACCOUNT];
    
    for (NSDictionary *_dic in data) {
        NSString *gid = [_dic objectForKey:@"gid"];
        NSString *owner = [_dic objectForKey:@"owner"];
        NSString *group_name = [_dic objectForKey:@"group_name"];
        NSString *group_pic = [_dic objectForKey:@"group_pic"];
        NSArray *members_arr = [_dic objectForKey:@"members_arr"];
        
        if ([members_arr count] > 0) { //有成員的資料就存起來
            for (NSDictionary *_dicMember in members_arr) {
                NSString *avatar = [_dicMember objectForKey:@"avatar"];
                NSString *chinese_name = [_dicMember objectForKey:@"chinese_name"];
                NSString *english_name = [_dicMember objectForKey:@"english_name"];
                NSString *account = [_dicMember objectForKey:@"account"];
                
                NSString *sqlString = [NSString stringWithFormat:@"INSERT OR REPLACE INTO members_arr (gid,avatar,chinese_name,english_name,account,MyAccount) VALUES ('%@','%@','%@','%@','%@','%@')",gid,avatar,chinese_name,english_name,account,MyAccount];
                //NSLog(@"sqlString SaveDataMyGroup member_arr:%@",sqlString);
                [[VariableStore sharedInstance].FMDB executeUpdate:sqlString];
            }
        }
        
        //存該群組
        NSString *sqlString = [NSString stringWithFormat:@"INSERT OR REPLACE INTO my_group (gid,owner,group_name,group_pic,MyAccount) VALUES ('%@','%@','%@','%@','%@')",gid,owner,group_name,group_pic,MyAccount];
        //NSLog(@"sqlString SaveDataMyGroup:%@",sqlString);
        [[VariableStore sharedInstance].FMDB executeUpdate:sqlString];
    }
    
    [[VariableStore sharedInstance].FMDB close];
}




//以下未使用
-(NSMutableDictionary *) GetMessageWithMessageID:(NSString *)message_id { //取得單一訊息
    NSMutableDictionary *dicToReturn = [[NSMutableDictionary alloc] init];
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM message WHERE (messageId = '%@')",message_id];
    NSLog(@"GetMessageWithMessageID sqlString:%@",sqlString);
    
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    
    if ([s next]) {
        NSString *fromId = [s stringForColumn:@"fromId"];
        NSString *isFail = [s stringForColumn:@"isFail"];
        NSString *isRead = [s stringForColumn:@"isRead"];
        NSString *isRecv = [s stringForColumn:@"isRecv"];
        NSString *isSend = [s stringForColumn:@"isSend"];
        NSString *message = [s stringForColumn:@"message"];
        NSString *messageId = [s stringForColumn:@"messageId"];
        NSString *readDate = [s stringForColumn:@"readDate"];
        NSString *recvDate = [s stringForColumn:@"recvDate"];
        NSString *sendDate = [s stringForColumn:@"sendDate"];
        NSString *toId = [s stringForColumn:@"toId"];
        
        [dicToReturn setObject:fromId forKey:@"fromId"];
        [dicToReturn setObject:isFail forKey:@"isFail"];
        [dicToReturn setObject:isRead forKey:@"isRead"];
        [dicToReturn setObject:isRecv forKey:@"isRecv"];
        [dicToReturn setObject:isSend forKey:@"isSend"];
        [dicToReturn setObject:message forKey:@"message"];
        [dicToReturn setObject:messageId forKey:@"messageId"];
        [dicToReturn setObject:readDate forKey:@"readDate"];
        [dicToReturn setObject:recvDate forKey:@"recvDate"];
        [dicToReturn setObject:sendDate forKey:@"sendDate"];
        [dicToReturn setObject:toId forKey:@"toId"];
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    return dicToReturn;
}
-(BOOL) didUnreadMessageExist { //判斷是否有任何未讀訊息
    BOOL didUnreadMessageExist = NO;
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *member_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"member_id"];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM message WHERE isRead <> '1' AND toId = '%@'",member_id];
    NSLog(@"didUnreadMessageExist sqlString:%@",sqlString);
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    if ([s next]) {
        NSInteger count = [s intForColumnIndex:0];
        if (count > 0) {
            didUnreadMessageExist = YES;
        }
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    return didUnreadMessageExist;
}
-(BOOL) didUnreadMessageExistWithMemberID:(NSString *)memeber_id { //判斷該帳號是否有任何未讀訊息
    BOOL didUnreadMessageExist = NO;
    
    [[VariableStore sharedInstance].FMDB open];
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM message WHERE isRead <> '1' AND fromId = '%@'",memeber_id];
    //NSLog(@"didUnreadMessageExistWithMemberID sqlString:%@",sqlString);
    
    FMResultSet *s = [[VariableStore sharedInstance].FMDB executeQuery:sqlString];
    if ([s next]) {
        NSInteger count = [s intForColumnIndex:0];
        if (count > 0) {
            didUnreadMessageExist = YES;
        }
    }
    
    [[VariableStore sharedInstance].FMDB close];
    
    return didUnreadMessageExist;
}
-(void) SetUnreadMessageWithMemberID:(NSString *)memeber_id { //把該帳號的所有訊息設定為已讀
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:@"data.db"];
    
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSString *sqlString = [NSString stringWithFormat:@"UPDATE message SET isRead = '1' WHERE toId = '%@'",memeber_id];
        NSLog(@"SetUnreadMessageWithMemberID sqlString:%@",sqlString);
        [db executeUpdate:sqlString];
    }];
}
#pragma mark - 雜項
-(void) dealloc {
}
@end
