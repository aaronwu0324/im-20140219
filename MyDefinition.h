
#define kPasswd @"com.jamzoo.im"

#define kSYSGateway @"http://202.74.126.64/index.php"
#define kSYSGatewayTemp @"http://mqttnewyearmem.cloudapp.net:8088/index.php"
#define kMQTTGateway @"http://mqttnewyearmem.cloudapp.net:8088" //http://mqttnewyear.jampush.com.tw:8088

#define kURLTimeoutSecond 60
#define kURLRetryOnTimeout 2
#define kMessageNetworkFail @"目前無網路連線，請確認您的網路狀態"
#define kMessageServerNoResponse @"目前無網路連線，請確認您的網路狀態"
#define kMessageServerZeroResult @"無相關資料，重新試一次。"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define kDEFAULTS_KEY_TOKEN_FROM_API @"TOKEN_FROM_API" //後台回傳的Token
#define kDEFAULTS_KEY_IS_LOGIN @"IS_LOGIN"
#define kDEFAULTS_KEY_MEMBER_ACCOUNT @"account"
#define kDEFAULTS_KEY_MEMBER_ID @"member_id"
#define kDEFAULTS_KEY_MEMBER_CHINESE_NAME @"member_chinese_name"
#define kDEFAULTS_KEY_MEMBER_PASSWORD @"member_password"
#define kDEFAULTS_KEY_MEMBER_AVATAR @"avatar"
#define kDEFAULTS_KEY_SETTING_VIBRATE @"setting_vibrate"
#define kDEFAULTS_KEY_SETTING_SOUND @"setting_sound"
#define kENCRYPT_KEY @"CMCCampaign"

enum FunctionTag {
    kFunctionTag_NameList = 1,  //名單
    kFunctionTag_Chat = 2,      //聊天
    kFunctionTag_GroupChat = 3, //群聊
    kFunctionTag_Setup = 4,     //設定
} FunctionTag;

enum ChatRoomUserType {
    ChatRoomUserType_SingleChat = 1,  //名單
    ChatRoomUserType_GroupChat = 2,      //群組聊天
} ChatRoomUserType;

//以下未使用
#define kDEFAULTS_KEY_NICKNAME @"NICKNAME"
#define kDEFAULTS_KEY_MEMBERTYPE @"member_type"
#define kDEFAULTS_KEY_NAME @"NAME"

#define kDatabasePath [NSHomeDirectory() stringByAppendingPathComponent:@"Library/db.sqlite"]
#define kCoreDataPath [NSHomeDirectory() stringByAppendingPathComponent:@"Library/messages.sqlite"]

