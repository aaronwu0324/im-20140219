
#import "MyImageView.h"

@implementation MyImageView

#pragma mark - 初始化
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}
-(void) UpdateUI {
}
#pragma mark - 換頁
-(IBAction) Dismiss {
    [self removeFromSuperview];
}
#pragma mark - 雜項
-(void) dealloc {
    NSLog(@"MyImageView dealloc");
}
@end
