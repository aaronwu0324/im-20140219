
#import "Message.h"

/**
 SingleBank 訊息資料庫管理器
 */
@interface MessageManager : NSObject

///---------------------------------------------------------------------
/// @name Class Methods
///---------------------------------------------------------------------

/**
 @return 返回一個 singleton 訊息管理器
 */
+ (instancetype)sharedManager;

///---------------------------------------------------------------------
/// @name Public Methods
///---------------------------------------------------------------------

/**
 @return 返回一筆新的 message 物件
 */
- (Message *)newMessage;

/**
 返回一筆 message 物件 by messageId
 
 @param messageId messageId
 @return 返回一筆 message 物件 by messageId
 */
- (Message *)messageByMessageId:(NSString *)messageId;

/**
 刪除單一筆 message 物件
 
 @param message 刪除的訊息
 */
- (BOOL)deleteSingleMessage:(Message *)message;

/**
 刪除多筆 message 物件
 
 @param messages 多個 message 的集合
 */
- (BOOL)deleteAllMessages:(NSArray *)messages;

/**
 查詢
 
 @param predicate 查詢條件
 @param limit 返回筆數, 當 limit == 0 返回全部
 @param desc 是否要升序
 */
- (NSArray *)queryByPredicate:(NSPredicate *)predicate limit:(NSInteger)limit ascending:(BOOL)desc;

/**
 儲存
 */
- (BOOL)saveContext;
@end
