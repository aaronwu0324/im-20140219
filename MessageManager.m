

#import "MessageManager.h"
#import <CoreData/CoreData.h>

@interface MessageManager(/*Private*/)

@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end

@implementation MessageManager

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken;
    static MessageManager *_sharedManager;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[MessageManager alloc]init];
    });
    
    return _sharedManager;
}

- (id)init
{
    self = [super init];
    if(self)
    {
        _managedObjectModel = [self setupManagedObjectModel];
        _persistentStoreCoordinator = [self setupPersistentStoreCoordinator];
        _managedObjectContext = [self setupManagedObjectContext];
    }
    
    return self;
}

- (Message *)newMessage
{
    //NSLog(@"_managedObjectContext:%@",_managedObjectContext);
    Message *message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:_managedObjectContext];
    //NSLog(@"Message:%@",message);
    return message;
}

- (Message *)messageByMessageId:(NSString *)messageId
{
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Message"
                                              inManagedObjectContext:_managedObjectContext];
    
    [request setEntity:entity];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"(messageId = %@)", messageId];
    [request setPredicate:filter];
    
    NSArray *filterArray = [_managedObjectContext executeFetchRequest:request error:nil];
    NSLog(@"filterArray:%@",filterArray);
    
    if(filterArray.count > 0)
    {
        return filterArray[0];
    }
    else
    {
        return nil;
    }
}

- (BOOL)deleteSingleMessage:(Message *)message
{
    [_managedObjectContext deleteObject:message];
    
    return [_managedObjectContext hasChanges];
}

- (BOOL)deleteAllMessages:(NSArray *)messages
{
    for(Message *message in messages)
    {
        [_managedObjectContext deleteObject:message];
    }
    
    return [_managedObjectContext hasChanges];
}

- (NSArray *)queryByPredicate:(NSPredicate *)predicate limit:(NSInteger)limit ascending:(BOOL)flag
{
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Message"
                                              inManagedObjectContext:_managedObjectContext];
    
    [request setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]initWithKey:@"sendDate" ascending:flag];
    [request setSortDescriptors:@[sort]];
    
    if(predicate)
        [request setPredicate:predicate];
    
    if(limit > 0)
        [request setFetchLimit:limit];
    
    
    return [_managedObjectContext executeFetchRequest:request error:nil];
}

- (BOOL)saveContext
{
    NSError *error = nil;

    if([_managedObjectContext hasChanges] && ![_managedObjectContext save:&error])
    {
        NSLog(@"message save / update error: %@", [error userInfo]);
        return NO;
    }
    else
    {
        return YES;
    }
}

- (NSManagedObjectModel *)setupManagedObjectModel
{
    NSURL *modelURL = [[NSBundle mainBundle]URLForResource:@"SingleBankMessage"
                                             withExtension:@"momd"];
    //NSLog(@"modelURL:%@",modelURL);
    return [[NSManagedObjectModel alloc]initWithContentsOfURL:modelURL];
}

- (NSPersistentStoreCoordinator *)setupPersistentStoreCoordinator
{
    if(!_managedObjectModel)
    {
        NSLog(@"_managedObjectModel is nil");
        return nil;
    }
    
    NSURL *storeURL = [NSURL fileURLWithPath:kCoreDataPath];
    
    NSError *error = nil;
    
    NSPersistentStoreCoordinator *store =
    [[NSPersistentStoreCoordinator alloc]initWithManagedObjectModel:_managedObjectModel];
    
    if(![store addPersistentStoreWithType:NSSQLiteStoreType
                            configuration:nil
                                      URL:storeURL
                                  options:nil
                                    error:&error])
    {
        NSLog(@"init persistentStoreCoordinator fail:%@", [error userInfo]);
        return nil;
    }
    
    return store;
}

- (NSManagedObjectContext *)setupManagedObjectContext
{
    if(!_persistentStoreCoordinator)
    {
        NSLog(@"_persistentStoreCoordinator is nil");
        return nil;
    }
    
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc]init];
    [context setPersistentStoreCoordinator:_persistentStoreCoordinator];

    //不要 Undo
    [context setUndoManager:nil];
    
    return context;
}
@end
