
#import "User.h"
#import "AESCrypt.h"
#import "AppDelegate.h"
//#import "MQTTManager.h"

@interface User (/*Private*/)

;
#pragma mark - Private Properties

//使用者訊息
@property (readwrite, nonatomic, strong) NSMutableDictionary *info;

//使用者位置
//@property (readwrite, nonatomic, strong) CLLocationManager *localManager;

@end


@implementation User

;
#pragma mark - LifeCycle

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

+ (instancetype)sharedUser {
    static dispatch_once_t onceToken;
    static User *_sharedUser = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedUser = [[User alloc] init];
    });
    return _sharedUser;
}

- (id)init {
    self = [super init];
    if(self) {
        [self setup];
        //self.localManager = [[CLLocationManager alloc]init];
        //[_localManager startUpdatingLocation];
    }
    return self;
}
#pragma mark - 設置初始

///---------------------------------------------------------------------
/// @name Private Methods
///---------------------------------------------------------------------

/**
 設置使用者初始值, 設置 ISOK, LOGINKEY, MEMID, ISAUTH, UDID, CLIENTID
 */
- (void)setup {
    self.info = [NSMutableDictionary dictionary];
    
    _info[@"APPID"] = [[NSBundle mainBundle] bundleIdentifier];
    
    NSUserDefaults *userDafault = [NSUserDefaults standardUserDefaults];
    NSString *ACCOUNT = [userDafault objectForKey:kDEFAULTS_KEY_MEMBER_ACCOUNT];
    NSString *MEMBER_ID = [userDafault objectForKey:kDEFAULTS_KEY_MEMBER_ID];
    NSString *MEMBER_CHINESE_NAME = [userDafault objectForKey:kDEFAULTS_KEY_MEMBER_CHINESE_NAME];
    NSString *MEMBER_AVATAR = [userDafault objectForKey:kDEFAULTS_KEY_MEMBER_AVATAR];
    NSString *UDID = [userDafault objectForKey:@"UDID"];
    NSString *CLIENTID = [userDafault objectForKey:@"CLIENTID"];
    
    if(ACCOUNT.length > 0) {
        _info[kDEFAULTS_KEY_MEMBER_ACCOUNT] = [AESCrypt decrypt:ACCOUNT password:kPasswd];
    }
    
    if(MEMBER_ID.length > 0) {
        _info[kDEFAULTS_KEY_MEMBER_ID] = [AESCrypt decrypt:MEMBER_ID password:kPasswd];
    }
    
    if(MEMBER_CHINESE_NAME.length > 0) {
        _info[kDEFAULTS_KEY_MEMBER_CHINESE_NAME] = [AESCrypt decrypt:MEMBER_CHINESE_NAME password:kPasswd];
    }
    
    if(MEMBER_AVATAR.length > 0) {
        _info[kDEFAULTS_KEY_MEMBER_AVATAR] = [AESCrypt decrypt:MEMBER_AVATAR password:kPasswd];
    }
    
    if(UDID.length > 0 && CLIENTID.length > 0) {
        _info[@"UDID"] = [AESCrypt decrypt:UDID password:kPasswd];
        _info[@"CLIENTID"] = [AESCrypt decrypt:CLIENTID password:kPasswd];
    }
    else {
        UDID = [self generateUUID];
        CLIENTID = [UDID substringToIndex:23];
        
        _info[@"UDID"] = UDID;
        _info[@"CLIENTID"] = CLIENTID;
        
        [userDafault setObject:[AESCrypt encrypt:UDID password:kPasswd] forKey:@"UDID"];
        [userDafault setObject:[AESCrypt encrypt:CLIENTID password:kPasswd] forKey:@"CLIENTID"];
    }
    //NSLog(@"CLIENTID:%@",_info[@"CLIENTID"]);
    
    [userDafault synchronize];
}
#pragma mark - 登入
- (void) login {
    //[[MQTTManager sharedInstance]start];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if(_info[@"MEMID"]) {
        [userDefault setObject:[AESCrypt encrypt:_info[@"MEMID"] password:kPasswd]
                        forKey:@"SYSMEMID"];
    }
    
    [userDefault synchronize];
    
    //將 window.rootViewController 切換到九宮格頁面
    /*UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UINavigationController *nav = [s instantiateViewControllerWithIdentifier:@"MainViewControllerNav"];
    
    AppDelegate *delegate = [[UIApplication sharedApplication]delegate];
    delegate.window.rootViewController = nav;*/
    
    _isLogin = YES;
}

#pragma mark - 登出

- (void)logout
{
    //[[MQTTManager sharedInstance]stop];
    
    /*AppDelegate *delegate = [[UIApplication sharedApplication]delegate];
    id MVC = delegate.window.rootViewController;
    
    //如果已經在登入頁面, 就不做事了
    if([MVC isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *nav = MVC;
        
        if([nav.viewControllers[0]isKindOfClass:NSClassFromString(@"LoginViewController")])
        {
            return;
        }
    }*/
    
    //會員登出清空程序, 相關條件請查閱 Gateway 規格文件
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SYSLOGINKEY"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SYSMEMID"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SYSISAUTH"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //因為登出了, 所以要再初始設置
    [self setup];
    
    /*//將 window.rootViewController 切換到登入頁面
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    id rootViewControllelr = [s instantiateViewControllerWithIdentifier:@"RegisterNav"];
    delegate.window.rootViewController = rootViewControllelr;*/
    
    _isLogin = NO;
}

#pragma mark - 會員資料儲存程序
- (void)updateInfo:(NSDictionary *)newInfo {
    //存到User物件與defaults
    [newInfo enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        [_info setObject:obj forKey:key];
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        
        if(_info[kDEFAULTS_KEY_MEMBER_ACCOUNT]) {
            [userDefault setObject:[AESCrypt encrypt:_info[kDEFAULTS_KEY_MEMBER_ACCOUNT] password:kPasswd] forKey:kDEFAULTS_KEY_MEMBER_ACCOUNT];
        }
        
        if(_info[kDEFAULTS_KEY_MEMBER_ID]) {
            [userDefault setObject:[AESCrypt encrypt:_info[kDEFAULTS_KEY_MEMBER_ID] password:kPasswd] forKey:kDEFAULTS_KEY_MEMBER_ID];
        }
        
        if(_info[kDEFAULTS_KEY_MEMBER_CHINESE_NAME]) {
            [userDefault setObject:[AESCrypt encrypt:_info[kDEFAULTS_KEY_MEMBER_CHINESE_NAME] password:kPasswd] forKey:kDEFAULTS_KEY_MEMBER_CHINESE_NAME];
        }
        
        if(_info[kDEFAULTS_KEY_MEMBER_AVATAR]) {
            [userDefault setObject:[AESCrypt encrypt:_info[kDEFAULTS_KEY_MEMBER_AVATAR] password:kPasswd] forKey:kDEFAULTS_KEY_MEMBER_AVATAR];
        }
        
        [userDefault synchronize];
    }];
}
#pragma mark - 回傳自定義產生的 UUID
- (NSString *)generateUUID {
    CFUUIDRef udid = CFUUIDCreate(NULL);
    
    //ARC bridge
    NSString *UDID = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, udid));
    
    CFRelease(udid);
    return UDID;
}

#pragma mark - Return Methods
- (NSString *)UDID {
    return _info[@"UDID"];
}
- (NSString *)APPID {
    return _info[@"APPID"];
}
- (NSString *)CLIENTID {
    return _info[@"CLIENTID"];
}
- (NSString *)ACCOUNT {
    return _info[kDEFAULTS_KEY_MEMBER_ACCOUNT];
}
- (NSString *)MEMBER_ID {
    return _info[kDEFAULTS_KEY_MEMBER_ID];
}
- (NSString *)MEMBER_CHINESE_NAME {
    return _info[kDEFAULTS_KEY_MEMBER_CHINESE_NAME];
}
- (NSString *)MEMBER_AVATAR {
    return _info[kDEFAULTS_KEY_MEMBER_AVATAR];
}
/*- (CLLocation *)location {
    return _localManager.location;
}*/
@end
