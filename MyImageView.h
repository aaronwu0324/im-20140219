
#import <UIKit/UIKit.h>

@interface MyImageView : UIView {
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,strong) IBOutlet UIImageView *Image;
-(void) UpdateUI;
@end
