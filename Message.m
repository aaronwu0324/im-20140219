
#import "Message.h"


@implementation Message

@dynamic fromId;
@dynamic toId;
@dynamic messageId;
@dynamic isRead;
@dynamic isSend;
@dynamic isRecv;
@dynamic readDate;
@dynamic sendDate;
@dynamic recvDate;
@dynamic isFail;
@dynamic message;

@end
