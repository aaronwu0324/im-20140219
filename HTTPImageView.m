
#import "HTTPImageView.h"

@implementation HTTPImageView
- (void)setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholder {
    [request setDelegate:nil];
    [request cancel];
    
    //setDefaultCache
    [ASIHTTPRequest setDefaultCache:[ASIDownloadCache sharedCache]];
    
    //避免中文抓不到
    //NSString *encodedString = (NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)url,NULL,NULL,kCFStringEncodingUTF8);
    //NSLog(@"encodedString:%@",encodedString);
    //NSURL *_url = [NSURL URLWithString:encodedString];
    
    NSURL *_url = [NSURL URLWithString:url];
    request = [ASIHTTPRequest requestWithURL:_url];
    
    // Always ask the server if there is new content available,
    // If the request fails, use data from the cache even if it should have expired.
    [request setCachePolicy:ASIAskServerIfModifiedCachePolicy|ASIFallbackToCacheIfLoadFailsCachePolicy];
    
    //cached responses are stored permanently
    [request setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    
    [request setDownloadCache:[ASIDownloadCache sharedCache]];
    
    if (placeholder) self.image = placeholder;
    
    [request setDelegate:self];
    [request startAsynchronous];
}
- (void)dealloc {
    [request setDelegate:nil];
    [request cancel];
    //NSLog(@"HTTPImageView dealloc");
}
- (void)requestFinished:(ASIHTTPRequest *)req {
    
    if (request.responseStatusCode != 200) return;
    
    self.image = [UIImage imageWithData:request.responseData];
}
@end
