#import "ASIHTTPRequest.h"
#import <UIKit/UIKit.h>
#import "ASIDownloadCache.h"

@interface HTTPImageView : UIImageView {
    ASIHTTPRequest *request;
}
-(void)setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholder;
@end