
#import "mosquitto.h"

@class SPMQTTClient;

/**
 SPMQTTClientDelegate
 */
@protocol SPMQTTClientDelegate <NSObject>

/**
 MQTT 連線成功
 
 @param mqttClient Self
 */
- (void)mqttDidConnect:(SPMQTTClient *)mqttClient;

/**
 MQTT 連線失敗
 
 @param mqttClient Self
 */
- (void)mqttConnectError:(SPMQTTClient *)mqttClient;

/**
 MQTT 收到訊息
 
 @param mqttClient Self
 @param message 訊息內容
 @param topic 訊息頻道
 */
- (void)mqtt:(SPMQTTClient *)mqttClient didReciveMessage:(NSData *)message onTopic:(NSString *)topic;

@end


/**
 基於 mosquitto 集成的 MQTT client, 有關 mosquitto 請自行閱讀文件 http://mosquitto.org
 */
@interface SPMQTTClient : NSObject

///---------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------

/**
 delegation for SPMQTTClient
 */
@property (nonatomic, retain) id<SPMQTTClientDelegate>delegate;

///---------------------------------------------------------------------
/// @name Public Methods
///---------------------------------------------------------------------

/**
 返回一個設置 clinetId 的 MQTT Client, MQTT 相關協定請自行閱讀文件 http://mqtt.org
 
 @param clientId MQTT 所需的 clinetId
 */
- (id)initWithClientId:(NSString *)clientId;

/**
 返回一個設置 clinetId, keepAlive, will message, will topic, qos, cleanSession, retain 的 MQTT Client, 
 MQTT 相關協定請自行閱讀文件 http://mqtt.org
 
 @param clinetId MQTT 所需的 Client Id
 @param keepAlive MQTT 所需的 Keep Alive 時間
 @param message MQTT 所需的 Will Message
 @param topic MQTT 所需的 Will Topic
 @param qos MQTT 所需的 Qos
 @param cleanSession MQTT 所需的 CleanSession
 @param retain MQTT 所需的 Retain
 @return MQTT Client
 */
- (id)initWithClientId:(NSString *)clinetId
             keepAlive:(NSUInteger)keepAlive
           willMessage:(NSString *)message
             willTopic:(NSString *)topic
                   Qos:(NSUInteger)qos
          cleanSession:(BOOL)cleanSession
                retain:(BOOL)retain;

/**
 連接 MQTT 主機
 
 @param host 主機位址
 @param port 主機 port
 */
- (void)connectHost:(NSString *)host port:(NSUInteger)port;

/**
 MQTT 傳送訊息, MQTT 相關協定請自行閱讀文件 http://mqtt.org
 
 @param payload 訊息內容
 @param topic 訊息頻道
 @param qos Qos 類型
 @param retain retain 類型
 */
- (void)publish:(NSString *)payload onTopic:(NSString *)topic Qos:(NSUInteger)qos retain:(BOOL)retain;

/**
 MQTT 訂閱頻道, MQTT 相關協定請自行閱讀文件 http://mqtt.org
 
 @param topic 頻道
 @param qos Qos 類型
 */
- (void)subscribe:(NSString *)topic Qos:(NSUInteger)qos;

/**
 MQTT 取消頻道, MQTT 相關協定請自行閱讀文件 http://mqtt.org
 
 @param topic 頻道
 */
- (void)unsubscribe:(NSString *)topic;

/**
 SPMQTTClient release 前的設定
 
 @warning 因為 SPMQTTClient 是 Objective-c 與 c 混合寫法, 所以在 release 之前, 必須 Call preRelease
 */
- (void)preRelease;
@end
