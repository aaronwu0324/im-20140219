

#import "User.h"
#import "APIClient.h"

@interface APIClient()

;
#pragma mark - Private Properties


@property (nonatomic, strong) AFHTTPRequestOperation *borkerOperation;
@end


@implementation APIClient

;
#pragma mark - LifeCycle
- (void)dealloc {
    [super dealloc];
}
+ (instancetype)sharedClient {
    static APIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        NSURL *url = [NSURL URLWithString:kSYSGateway];
        _sharedClient = [[APIClient alloc]initWithBaseURL:url];
    });
    
    return _sharedClient;
}
- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if(self)
    {
        [self registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    }
    
    return self;
}
#pragma mark - 初始化 Borker IP
- (void)callAPIInitBorker:(APIClientResponse)callback {
    NSLog(@"init Borker IP");
    
    [self cancelBrokerOperation];
    
    //{dxid} 為該次安裝的 UUID (36 bytes)，重安裝 APP 才會重新取值。
    //{token} 為 MQTT Client ID (23 bytes)，重安裝 APP 才會重新取值。
    //※這是必須要呼叫的gateway，略過會無法收到聊天頻道哦！
    
    NSString *dxid = [[User sharedUser] UDID];
    NSString *token = [[User sharedUser] CLIENTID];
    
    //NSString *apiURLString = [NSString stringWithFormat:@"https://hwu-mq2.jampush.com.tw/v1/login/%@/%@", dxid, token];
    NSString *apiURLString = [NSString stringWithFormat:@"https://mqttnewyear.jampush.com.tw/v1/login/%@/%@", dxid, token];
    
    NSURL *apiURL = [NSURL URLWithString:apiURLString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:apiURL];
    [request setTimeoutInterval:5.0f];
    
    self.borkerOperation = [self HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error = nil;
        id JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        NSLog(@"init Borker IP result = \n%@", JSON);
        
        //回傳非 JSON or NSDictionary
        if(error || ![JSON isKindOfClass:[NSDictionary class]]) {
            //DLog(@"init Borker IP result fail");
            callback(APIClientResponseStatusResultTypeFail, @{@"MSG" : @"發生錯誤 , 請重新啓動 APP"});
        }
        //有錯誤
        else if(![JSON valueForKey:@"broker"]) {
            callback(APIClientResponseStatusSuccessButError, @{@"MSG" : @"找不到 broker IP"});
        }
        else {
            callback(APIClientResponseStatusSuccess, JSON);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(APIClientResponseStatusNetworkFail, @{@"MSG" : @"無法連線!\n請確認網路連線狀態."});
    }];
    
    [_borkerOperation start];
}

- (void)cancelBrokerOperation {
    [_borkerOperation pause];
    self.borkerOperation = nil;
}
@end
