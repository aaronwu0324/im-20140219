
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

#import "MyAnimation.h"
#import <QuartzCore/QuartzCore.h>

@implementation MyAnimation
#pragma mark -
#pragma mark 淡入/淡出
-(void) FadeIn:(UIView *)sender time:(float)second {
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:second];
    sender.alpha = 1.0;
    //[UIView setAnimationDidStopSelector:@selector(animationFinished:)];
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}
-(void) FadeIn:(UIView *)sender time:(float)second alpha:(float)_alpha{
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:second];
    sender.alpha = _alpha;
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}
-(void) FadeOut:(UIView *)sender time:(float)second {
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:second];
    sender.alpha = 0;
    //[UIView setAnimationDidStopSelector:@selector(animationFinished:)];
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}
#pragma mark -
#pragma mark 移動相關
-(void) MoveCenterTo:(UIView *)sender time:(float)second position:(CGPoint)point {
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:second];
    sender.center = point;
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}
-(void) MakeTranslation:(UIView *)sender time:(float)second X:(float)x Y:(float)y {
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:second];
    sender.transform = CGAffineTransformMakeTranslation(x, y);
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}
-(void) MakeTranslationBack:(UIView *)sender time:(float)second {
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:second];
    sender.transform = CGAffineTransformMakeTranslation(0, 0);
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}
#pragma mark -
#pragma mark 旋轉相關
-(void) Rotate:(UIView *)sender degrees:(float)degree time:(float)
    second {
    float radian = DEGREES_TO_RADIANS(degree);
    NSLog(@"radian:%f",radian);
    NSLog(@"%@",sender);
    NSLog(@"time:%f",second);
    /*
    CATransform3DMakeRotation(<#CGFloat angle#>, <#CGFloat x#>, <#CGFloat y#>, <#CGFloat z#>)
    CABasicAnimation *theAnimation; 
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.rotate.x"];
    theAnimation.duration= 1.0;
    theAnimation.repeatCount= 1;
    theAnimation.autoreverses= NO;
    theAnimation.fromValue= [NSNumber numberWithFloat:0];
    theAnimation.toValue= [NSNumber numberWithFloat:2000];
    */
    
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:second];
    sender.transform = CGAffineTransformRotate(CGAffineTransformIdentity, radian);
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}
-(void) MakeRotate:(UIView *)sender degrees:(float)degree time:(float)second {
    float radian = DEGREES_TO_RADIANS(degree);
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:second];
    sender.transform = CGAffineTransformMakeRotation(radian);
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}
#pragma mark -
#pragma mark 放大/縮小
-(void) MakeScale:(UIView *)sender x:(float)x y:(float)y time:(float)second {
    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:second];
    sender.transform = CGAffineTransformMakeScale(x, y);
    [UIView setAnimationDelegate:self];
    [UIView commitAnimations];
}
@end
