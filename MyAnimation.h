

#import <Foundation/Foundation.h>


@interface MyAnimation : NSObject {
    
}

-(void) FadeIn:(UIView *)sender time:(float)second;     //淡入
-(void) FadeIn:(UIView *)sender time:(float)second alpha:(float)_alpha;     //淡入,指定alpha
-(void) FadeOut:(UIView *)sender time:(float)second;    //淡出
-(void) MoveCenterTo:(UIView *)sender time:(float)second position:(CGPoint)point;   //移動center到某位置
-(void) MakeTranslation:(UIView *)sender time:(float)second X:(float)x Y:(float)y;
-(void) MakeTranslationBack:(UIView *)sender time:(float)second;
-(void) Rotate:(UIView *)sender degrees:(float)degree time:(float)second;
-(void) MakeRotate:(UIView *)sender degrees:(float)degree time:(float)second;
-(void) MakeScale:(UIView *)sender x:(float)x y:(float)y time:(float)second;
@end
